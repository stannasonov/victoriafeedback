<?
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
    $APPLICATION->SetTitle("Обратная связь");
?>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=1024" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Обратная связь</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="../css/ui.css">
    <link rel="stylesheet" type="text/css" href="../css/fa.css">
    <!--script type="text/javascript" src="../js/jquery.js"></script-->
    <script type="text/javascript" src="../js/jq.sb.js"></script>
    <script type="text/javascript" src="../js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="../js/init.js"></script>
    <script src="http://code.createjs.com/createjs-2015.11.26.min.js"></script>
    <script src="../js/Icecream.js"></script>
    <script src="../js/canvas.js"></script>
<div class="crumbs main">
    <div class="right">
        <a href="/obratnaya-svyaz/">Выберите раздел</a> <span class="sep fa fa-caret-right"></span>
        <a class="active" href="#">Заполните форму</a> <span class="sep fa fa-caret-right"></span>
        <a href="#">Укажите контактные данные</a>
    </div>
</div>

<div class="main">
<?
    function forms($menu, $path = array(), $data = array()) {
        foreach($menu->item as $sub) {
            $name = $path;
            $name[] = $sub->text;

            if($sub->form)
                $data[] = array(
                    implode(" - ", $name),
                    $sub->form
                );
            elseif($sub->menu) {
              $data = forms($sub->menu, $name, $data);
            }
        }

        return $data;
    }

    $tree = simplexml_load_file('http://victoria-group.ru/obratnaya-svyaz-1/feedback/menu.xml' );
    $fs = forms($tree);

    $f = @$fs[ intval($_GET['id']) - 1];

    echo "<h3 style='text-align: center;'>".$f[0]."</h3>";

    if(isset($f))
    {
        foreach ($f[1] as $arr) {
            echo '<form class="form">';
            echo '<div class="step-1">';

            foreach ($arr as $input) {
                $elementId = '';
                foreach ($input->attributes() as $key => $value) {
                    if($key == 'id') {
                        $elementId = $value->__toString();
                        break;
                    }
                }

                echo '<div class="form-control">';
                if($input == 'number' || $input == 'check'){
                    if($input == 'check') {
                        echo '<label>'.$input.'</label>';
                        echo '<i class="fa fa-info info card" aria-hidden="true"></i>';
                    }else if($input == 'check'){
                        echo '<i class="fa fa-info info check" aria-hidden="true"></i>';
                    }
                    echo '<input ';
                }else if($input == 'files'){
                    echo '<label for="file" class="uploadFile"><i class="fa fa-paperclip fa-flip-horizontal" aria-hidden="true"></i></label>';
                    echo '<input ';
                }else{
                    echo '<input ';
                }

                foreach ($input->attributes() as $attr => $value) {
                    echo $attr.'="'.$value.'"';
                }

                echo '>';
                echo '</div>';

            }

        }
        echo '<img class="card" src="../css/images/card.png">';
        echo '<img class="check" src="../css/images/check.png">';
        echo '<label>Выразите свои эмоции</label>';
    }
    else
    {
        echo '<h1>Форма не найдена.</h1><br>';
    }
    echo "</div>";
?>
<div id="animation_container" style="background-color:rgba(255, 255, 255, 1.00); width:440px; height:395px">
    <canvas id="canvas" width="440" height="395" style="position: absolute; display: block; background-color:rgba(255, 255, 255, 1.00);"></canvas>
    <div id="dom_overlay_container" style="pointer-events:none; overflow:hidden; width:440px; height:395px; position: absolute; left: 0px; top: 0px; display: block;">
    </div>
</div>
<div class="step-2">
    <h3>Если вы хотите, чтобы мы связались с вами <br>по результатам рассмотрения вашего <br>обращения, укажите контактные данные</h3>
    <div class="form-control">
        <input type="text" placeholder="* Представьтесь, пожалуйста" required>
    </div>
    <div class="form-control">
        <input type="text" placeholder="* Телефон" required>
    </div>
    <div class="form-control">
        <input type="text" placeholder="Электронный адрес">
    </div>
    <div class="form-control">
        <input type="checkbox" id="i-agree"><label for="i-agree">Я хочу чтобы со мной связались после обработки обращения</label>
    </div>
</div>
<div class="controls">
    <a href="/feedback.php"><span class="fa fa-caret-left"></span> Назад</a>
    <div class="step-1">
        <span class="on">Далее<i class="fa fa-caret-right"></i></span>
    </div>
    <div class="step-2">
        <button type="submit">Отправить обращение<i class="fa fa-caret-right"></i></button>
    </div>
</div>
<? echo '</form>'; ?>
</div>
<script>init();</script>
<?  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
