(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:



(lib.Icecream_Berry = function() {
	this.initialize(img.Icecream_Berry);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,68,104);


(lib.Icecream_body_01 = function() {
	this.initialize(img.Icecream_body_01);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,154,207);


(lib.Icecream_body_02 = function() {
	this.initialize(img.Icecream_body_02);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,141,158);


(lib.Icecream_body_03 = function() {
	this.initialize(img.Icecream_body_03);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,144,159);


(lib.Icecream_body_04 = function() {
	this.initialize(img.Icecream_body_04);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,145,144);


(lib.Icecream_body_04_1 = function() {
	this.initialize(img.Icecream_body_04_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,152);


(lib.Icecream_body_06 = function() {
	this.initialize(img.Icecream_body_06);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,163,167);


(lib.Icecream_cone = function() {
	this.initialize(img.Icecream_cone);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,122,150);


(lib.Icecream_hat_02 = function() {
	this.initialize(img.Icecream_hat_02);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,60,32);


(lib.Icecream_hat_03 = function() {
	this.initialize(img.Icecream_hat_03);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,77,18);


(lib.Icecream_hat_04 = function() {
	this.initialize(img.Icecream_hat_04);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,78,32);


(lib.Icecream_hat_04_1 = function() {
	this.initialize(img.Icecream_hat_04_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,134,48);


(lib.Icecream_red = function() {
	this.initialize(img.Icecream_red);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,32,22);


(lib.Icecream_Shadow = function() {
	this.initialize(img.Icecream_Shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,186,27);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.brov = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgUAPQgCgFALgIIAPgLQAGgEAGgCQAGgBAAAHQAAAGgJACQgJACgLAJQgIAGgDAAQgBAAAAAAQAAAAgBAAQAAAAAAgBQAAAAAAAAg");
	this.shape.setTransform(1.9,1.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.brov, new cjs.Rectangle(-0.3,-0.1,4.4,3.2), null);


(lib.share3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACgAAQAABCgvAvQgvAvhCAAQhBAAgvgvQgvgvAAhCQAAhBAvgvQAvguBBAAQBCAAAvAuQAvAvAABBg");
	this.shape.setTransform(16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#CCCCCC").s().p("AhwBxQgugvgBhCQABhBAugvQAvguBBgBQBCABAvAuQAuAvABBBQgBBCguAvQgvAuhCABQhBgBgvgug");
	this.shape_1.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.share3, new cjs.Rectangle(-1,-1,33.9,33.9), null);


(lib.share2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACfAAQAABCgvAuQguAvhCAAQhBAAgvgvQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAvAvAABBg");
	this.shape.setTransform(16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#C1DF6E").s().p("AhwBwQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAwAvgBBBQABBCgwAuQguAwhCgBQhBABgvgwg");
	this.shape_1.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.share2, new cjs.Rectangle(-1,-1,33.9,33.9), null);


(lib.share1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACgAAQAABCgvAvQgvAvhCAAQhBAAgvgvQgvgvAAhCQAAhBAvgvQAvguBBAAQBCAAAvAuQAvAvAABBg");
	this.shape.setTransform(16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhwBxQgugvgBhCQABhBAugvQAvguBBgBQBCABAvAuQAuAvABBBQgBBCguAvQgvAuhCABQhBgBgvgug");
	this.shape_1.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.share1, new cjs.Rectangle(-1,-1,33.9,33.9), null);


(lib.rotik = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgZATQghgFgOgNQgNgMgFgJQBVApBggpIgNAPQgMAKgdAKQgTAGgUAAQgLAAgMgCg");
	this.shape.setTransform(9.1,2.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.rotik, new cjs.Rectangle(0,0,18.3,4.2), null);


(lib.r4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AAiANQgEgEAAgLQAEAJAKgBQAKgBACgGQADgIgDgCIAUgJQAGARgJAMQgJAMgLAAQgLAAgIgIgAhFANQgIgIAAgKQAAgFACgFQAAAIAEAFQAEAEAGAAQAFAAAEgEQAEgEABgFIAXALQgCAHgGAGQgIAIgLAAQgLAAgHgIg");
	this.shape.setTransform(7.8,2.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.r4, new cjs.Rectangle(0,0,15.7,4.3), null);


(lib.r3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AAdAHQgkgUggAMQggAKARgOQARgPAaACQAYABAVAMQAVALAFAKQABABAAABQAAAAAAABQgBAAAAAAQgBABAAAAQgGAAgYgNg");
	this.shape.setTransform(6.1,1.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.r3, new cjs.Rectangle(0,0,12.2,3.9), null);


(lib.r2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgoAAQABgGAOAAIAngBQAZAAABAHQACAGgTABIgpABQgWgBAAgHg");
	this.shape.setTransform(4.1,0.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.r2, new cjs.Rectangle(0,0,8.1,1.6), null);


(lib.lightstrike = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.lf(["#E83231","#FFFFFF"],[0,1],580.4,-307.5,580.4,307.5).s().p("ADkjVMgiUgEqMAfcgoDIZZAoI7MepIf4DwMg9PA9Eg");
	this.shape.setTransform(196.9,307.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.lightstrike, new cjs.Rectangle(0,0,393.8,615), null);


(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-65.5,-13.7)).s().p("AgDADIgBAAIAAgCQAAgBAAAAQAAAAAAAAQgBgBAAAAQAAgBAAAAQACgCAEAEIAGADIgKAAg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.6,-0.3,1.3,0.7);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_cone();
	this.instance.parent = this;
	this.instance.setTransform(-61,-75);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-61,-75,122,150);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_Shadow();
	this.instance.parent = this;
	this.instance.setTransform(-93,-13.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-93,-13.5,186,27);


(lib.Symbol68 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_body_03();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol68, new cjs.Rectangle(0,0,144,159), null);


(lib.Symbol67 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("AgBgEQACAEABADIAAACIgDgJg");
	this.shape.setTransform(107.9,23.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.bf(img.Icecream_body_06, null, new cjs.Matrix2D(1,0,0,1,-77.6,-90)).s().p("Ah3MCQgBgxAQggQAFgKAGgDQAGgFAOACIA5AAQAHAAADACQADAEAAAGIAAAVQgBAMAEAIIAIAKQAGAGABAEQAEANgMALgAGBI2IAEACQANAIAZAFIAiAFIAAAHIgBAxIgDAaQgCANgFADQgvg0gShCgAmqKIQgNgEgFgGQgGgGAAgQQABggALgeQAEAAAJgEQAIgDAEABQAEAAADAEIAHAGQANALAZgDIAUgBQALgCAIABQAVACADAOQABAHgFAJIgKARIgEAPQgDAKgEAEQgGAHgQACQgQACgQAAQgZAAgYgFgAB7JFQgNgZgEgNQgBgGABgDQACgDAHgDIAQgMQALgGAIACQAEAAAHAGIAJAFQAIAIABACQAFAKgKARIgfAzQgDAGgEAEQgHgbgGgNgADkHQQgIgOgKgMIgLgMQgGgGgCgHQgBgIgDgDIgFgHQgDgEACgDQAUACAOgMQAMgKADgBQAGgCAFACQAFACADAFQADADABAJIgBAWIgFAmQgCALgCADIgEAGIgCAFIgBAAQgEAAgEgHgALfHBQgVgBgjgTQgOgIgFgFIgNgQQgTgeAFgTQACgHAEgDQADgCAHABIATAEIATAGIAaABQAQABAKAEQAIAEAJAJQAJAIAEAHQAJASgRAfQgEAJgDACQgGAFgJAAIgEAAgAogElQgBgDAEgFIAOgYQAFgHACgGQADgMADgCQAFgEAKAIQANALALAOQgCACAAAFIgCAIQgBAEgGAFIgIAGIgJABgAJUD8IgkgKQgWgFgKgJQgOgLgBgVQgCgUAJgSQASAEAJgEQALgGACgRQAAgRABgJQAVAKAPgFQAIgDAFgJQAFgJgFgIIAfAIQAUAlARAnQgngTgpAFQgNACgDAHQgCAGAEAIIAZA/QACAFgBAEQgCADgFAAIgHgBgArThFQgfgOgRgfIAdgIQANALAbgCQAagBAUgKIAVgKQAMgGAKADQAFAQAAAPQABAKgEAHQgDAGgMAFQgTALgNAEQgNADgNAAQgUAAgTgJgApSltQgMgHgPgSQgzg/gRgiIBEgLQAOgCAHABQAIABAPAJIArAYQATALAGAJQAGAKABAVQACAWgHALQgIAKgUAFQgTAGgPAAQgPAAgKgFgAGSnWIgOgDQgGgCgHgGQgJgHgEgCQgOgGgYAEQgMACgFgBQgFgBgEgFQgHgHgGgKQgHgPgCgcQgBgKAFgEIAJgFIAFgFIAEgHQADgEAIgFIALgJIANgQQAHgJAVgUQAHAMgBARIgEATQgDASABAcQABAKACAEQAFAFAIgBQAHAAAHgFQAGgEAGgIIAIgLQALgLAOABQgCAaAGAbQACAHACADQAFAGAIABQAIAAAGgFQAFgEAIgJQAOgLAQAFQAHANgBANQgBAJgIASQgEAJgEACQgDABgJAAQgdAAg8gEgAiFpwQgUgIgOgSIATh2IAhAnQAHAHAHAKQAQAYADAdQABAVgJAJQgIAJgMAAIgCAAQgLAAgKgEg");
	this.shape_1.setTransform(77.6,90.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol67, new cjs.Rectangle(-35.9,-38,268.6,262.6), null);


(lib.Symbol66 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_body_06, null, new cjs.Matrix2D(1,0,0,1,-81.5,-82.9)).s().p("ACsM8QgUgCgOgMQARgTADggQABgSgGglQgEgTAGgJQAHgIAOABQALAAAQAGIAZAKQAPAEAeAHQAbAIAKASIAHASQAEAMAFAFQAGAEgBADQAAADgGACQgKAFgdAGQgaAFgNAIIgUANQgMAJgIAEQgMAGgOAAIgJgBgAJiMSQgEghAJhCQAEgeAIgLIBIgFQAQgBAIACQAMACAKAKQAGAGALAOQAEAGgBADQABAGgKAFQgZALgLAIQgMANgIAEIgQAIQgKAEgGADQgGAEgHAJIgNAOQgNAOgNAAIgGAAgAgXLgQgDgCgCgIQgJgUgCgKIgFgdQgEgRgIgJQgGgFgHgCQgHgDgHADQgHACgIAKIgMARQgDAEgUANQgMAIgYAUQgWAPgVgCQgCgQAAgVQAAglAHgkIACgSQAFgaAJgNQAMAJAPAEQAGACAAACIAAAIQAAAIAJAEQAFACALAAQAMAAAFgCQAEgCAHgHQALgKAUgWQANgOAJABIAJASQAFAKAEAEQAIAFAQgCIAigEQALgBAEgCQAGgDAIgJQAIgJAFgDIgHAYQgFAPgDAIIgQAfQgEAIgFAbIgHAcQgFAagGAOQgGALgGAEQgEADgGAAQgFAAgEgEgAGxKUIgugIQgYgEgNgIQgIgFgTgPIgwguQgMgKgDgHQgCgFgBgEIAZgHIADACIAUAPQAKAFAYAAQAMgBADgCQALgHgEgXQAcgHAcAAQASAAAGgEQAJgGACgNQACgKgEgNQgHgigQggIAEgGQAaAEAgAhQAKAJAEAIIAIAOQAFAHAJAKIAQAPQAJAJABALQACAMgGAIQgWgBgnANQgHACgDADQgEAEgBAMQgCAegFATIgIAVQgGAKgFACIgGABIgGgBgAqAKJQgDgGAEgMIALgiQAHgVAGgGIADgFIAHgBQAVAAAUAJQANAGAGAKQADAGAAAHQgBAGgEAFQgEAEgHACIgSANIgSAMQgJAFgNAFQgGACgFAAQgJAAgEgHgAmIJnQgPgBgGABQACgZAIgWQAHgQgEgHQgDgIgMAAQgEAAgNAEQgpANgxgDQACgQAHgOQAHgLACgHQACgEgCgFQgBgGgEgCQgDgDgIABIgLADQgRAFgigBQgHgBgCgCQgBgEAEgGIAXgoQAUgjAKgMQAFgGAFgDQAFgCAIABQAOACAEAAQALAAAGgFIAJgIIAIgJQAMgKAQAEQgKAdAIAMQADAFAGAFQAJAGAOABIAZABQAXgBATAEQgEATAAALQABASAOAGQAJAEAPgCIAXgCQAPgCAGAFQgBAGgFAHIgIANIgGAOQgDAIgDAEQgDACgNAFQgkAMgdA3QgFAKgEADQgFADgIAAIgHgBgAJZFVIABg5QAWABALgCQATgFAJgMQAEgHACgLQAHgbgNgKQgFgCgBgCQAAgDAFgFIAHgMQAEgDAKgDQAMgHABgWIABgRQABgJADgHQAGACAIgDQAHgEAFgGQAHgMgCgRIgBgfQABgOAHgMQACgEADgBQADAAAEACQAPAJAJAMIALAQIAdA6IAAA8IgKAMQgcAigIAeIgFATQgEAJgJAJQgFAGgHADQgDABgOABQgGACgPAGQggARgQAHQgaALgVAAIgFAAgAr+EqQgQgDgHgKQADgDALgDQAKgCADgEQAEgDAAgGQAAgGgDgFQgDgHgNgHIgagOIgLgJIAAhCIAKgEIAngPQARgHAAgKQgBgIgQgEQgPgDgDgIQgBgFACgFQADgFAFgEQAPgFAFgEIAOgMQAJgIAWgJIAmgRQALgFAGADQAEABAEAIQAKAPABAHIAEAVQACAMAHAFIAHAEQAEADAAADQgQAPAFAVQACAKAHAIQAGAHAKAEQgJANgCAIQgCAOAKAHQAFACALAAQARABASAFQgDARgMAMIgNAMQgIAIABAHQAAAEAEAJQADAHAAAFIgvABQgeAAgRADIgeAFIgkACQgTAAgKgDgAsuD4IANAIQgDAEgKABgALohPQgEgCgLgIQgPgRgFgJQgNgYANgqQADgLAFgFQALgNAVAFQAMACAWAJQANAEATgBIAABOQgZARgbAMQgKAFgGAAIgDAAgAqXiNQgIgBgQgJQg4gjgXggQgFgGACgDIAFgEQALgEAZAAIBZABQAGAAADACQAFAFgFANQgJATABAWQABASgCAFQgEAGgGACIgJACIgFgBgAKFkGQgHAAgDgEQgDgDgCgIIgGgkQgEgVgGgMQgJgSgRgCIgLgCQgGgBgDgDQgCgDAAgIIABgZQAAgPgEgKQgFgKgLgKIgUgQQgbgVAEgWQACgIAGgDQAEgCAFAAIAxgBQAOAAAJgDIAJgGQAFgEAEgBIAKgCQAFgCADgDIACgJQADgEAKAAQAYgBAXAGQAQAEAIAGQAEAEAIAMQAMAXAFASQAMAtgbBBQgXA5giApQgOASgNAAIAAAAgAm1lYIgpAAQgTAAgKgCQgRgDgJgJIgIgKIgIgLIgSgUQgLgLgDgKQgCgIAAgNQABgOgCgGIgDgRQgBgGABgNIgBgTIgEgPQgBgJAAgQIAAgpQAAgIADgDQADgCAGAAQAOAAARAMIAQANIANALIAKAFQAGADADADIAGAFIAFAFIAMADQAFADAFAKIAKAOIAJAOQADAFAGAPQAEAKALASIAPAcQAHAMAAALQAAAGgCAJIgFATQgMAOgMASIgCAAgAl6rfQAAgFABgCQACgDAFAAQASgDAPALQAKAHANAWIAIAOQAWAkADAUQAEAWgKAnQgFgEgHgBQgJgBgFAGQgDADgCAGIgCADQgyhNgIhdgAEGpWQADAEABAEIABACIgFgKgACIpXQgHgEAFgTQAEgMgBgQQAAgTgJgHQgHgHgXAAQgVAAgHgIIgHgKQgEgHgEgCQgGgDgJADQgKAFgFABQgMAEgQgHQAOgWAFgaIAJgTQAMgfgBgWIAAgBID/AAQgKARgKAHQgLAIgFAFQgEAEgGANQgGANgCAHIgBAXIgDAVQgCAIABAMIABAVIgBAXIAAACQgHgHgHgDQgOgHgNAGQgJAFgJAUQgNAdgPADIgDABIgGgCgAF2peQgGgDgEgJQgHgNgBgRQAAgOAHgHIAIgGIAGgIQACgFAEgCQADgCAJgBQADgCAEgJQAFgKALgEQALgEALAEQAKADABAJQACAFgCAGIgEALQgDAHgDAVIgDAaQgCALgDADQgFAFgMADIgXAFIgGAAQgJAAgEgDgAg+qbIAHgHIAIgHIgOAVIgDAFQgBgJADgDgAgFsgQgGgKgDgDQgEgDgHgDIgMgFIgHgEIA3AAQABAOgIAVIgBACIgDABIgFgKg");
	this.shape.setTransform(81.5,82.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol66, new cjs.Rectangle(-39.9,-50,323.1,274.1), null);


(lib.Symbol65 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3E0903").s().p("Ah5ANIAMgTIDngaIAABBg");
	this.shape.setTransform(9.7,9.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol65, new cjs.Rectangle(-2.5,5.8,24.5,6.7), null);


(lib.Symbol63 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-130.5,-39)).s().p("AADAzQgSgKgKgXQgKgTABgYQAAgUALgGQAEgCAKAAQAQABAIAGQAHAGAGAJIAHANIAAA6IgDADQgEAFgFADQgFADgFAAQgEAAgGgDg");
	this.shape.setTransform(3.5,5.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol63, new cjs.Rectangle(0,0,7,10.8), null);


(lib.Symbol62 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-112.1,-36.2)).s().p("AgFAoQgNgDgJgJQgIgJgCgOQgCgMAGgMQACgGAFgEIAEgEQADgEgCgCQAUgCARAGQAGACAEADQAFAEAEALIADAPQABAKgHAMQgFAIgIAFQgIAFgKAAIgGAAg");
	this.shape.setTransform(3.8,4.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol62, new cjs.Rectangle(0,0,7.7,8.1), null);


(lib.Symbol61 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-117.9,-23.2)).s().p("AAdA9QgOgCgZgOIgSgKQgKgFgHgGIgLgMQgHgKgBgFIgBgMQAAgKACgHQAEgJAJgFIAHgEIAGgHQADgDAIAAQAVABAJADQAPAEALAKIAGAHIAGAFIAJAGQACABACAFIAHAKQAEAFAAAEQABAFgCAGQgFAXgOAUQgCAEgDABQgCABgFAAIgFAAg");
	this.shape.setTransform(6.6,6.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol61, new cjs.Rectangle(0,0,13.3,12.3), null);


(lib.Symbol60 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-100.9,-24.1)).s().p("AAAA1IgMAAQgOAAgFgEIgGgFQgMgMABgJIADgLIAAgLQABgKAFgIQADgDABgEQgCgFABgDQAAgDAEgEQAIgJAGgCQAFgCAIABIAKABQAGABAHAFIAIAGQAHAGADADIAIAUQAFALAAAGQABAIgHAPIgFAIQgDADgJAEQgKAGgIABIgEAAIgEAAg");
	this.shape.setTransform(4.9,5.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol60, new cjs.Rectangle(0,0,9.8,10.6), null);


(lib.Symbol59 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-94.9,-9)).s().p("AgYA4IgRgDIgKgCQgJgDgGgIIgJgQQADgEABgGIABgLQAAgDACgEQADgHAIgLIAHgKQAHgJAJgFQAHgDALgCQAagGATACQAMACAEAEQAEADADAFIAHAKQAHAKAFAJQAMAagPAhIgEAFIgFACQgQACghAAIgZABIgJgBg");
	this.shape.setTransform(7.6,5.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol59, new cjs.Rectangle(0,0,15.3,11.3), null);


(lib.Symbol58 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-89.6,-33.6)).s().p("AgVApQgSgEgEgMQgDgHACgOQACgJACgCIAGgIIAEgNQAGgNAXgCQAOgCAKAEQALAFAHALQAGALgBANQgBAZgWANQgMAHgNAAQgJAAgKgDg");
	this.shape.setTransform(4.6,4.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol58, new cjs.Rectangle(0,0,9.3,8.9), null);


(lib.Symbol57 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-84,-18)).s().p("AgZAvIgFgBIgGgCQgHgFgDgGQgGgIAAgLQAAgFADgKIAEgMIADgGIAGgEQADgEgCgEIAOgNQAEgCAKAAIAPACIAJACIAJAEQAMAHAJAUIAFANQABAHgBAFQgCALgFAHQgJAKgOADQgJACgQAAg");
	this.shape.setTransform(5.3,4.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol57, new cjs.Rectangle(0,0,10.6,9.4), null);


(lib.Symbol56 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-70,-6.8)).s().p("AgRBDQgHgBgLgIQgJgGgGgGIgHgJQgFgHgFgKQgKgTADgOQACgHAFgKQAQgcARgKIBBAAIAGAEIAKAIQARALAFAJQAGALAAAQQABAPgGAOQgGAMgNAKQgIAHgTAKQgMAHgGABIgNABg");
	this.shape.setTransform(7.5,6.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol56, new cjs.Rectangle(0,0,15.1,13.7), null);


(lib.Symbol55 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-65.7,-22.6)).s().p("AgUBGQgRgEgIgHQgFgEgDgFQgOgVgBgYIAAgJIAEgMIAFgUQACgMAFgGQADgFAJgGQAJgFAMgBQAWgEAaAPIAJAGIAJAEIAEAHIAJALQAKANgBATQgCAQgLAPIgMAQQgIALgGADQgDACgHABIgLADIgJAFIgFABQgGAAgJgDg");
	this.shape.setTransform(6.9,7.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol55, new cjs.Rectangle(0,0,13.8,14.6), null);


(lib.Symbol54 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-29.6,-19.2)).s().p("AgiBEQgEgBgDgGQgHgJgIgWQgFgMABgGIACgJIAAgOIACgLIABgNQACgIAHgOQAGgIADgCQAFgDAMABIAWACQAJAAAGACQAKACAJAGQALAHAEAFQAGAHACAFIAAAKIACAJIgBAMIgBAJIgDALQgDAGgGAGQgLAKgNAIQgPAKgMAGIgDABIgHADQgHACgFAAQgEAAgEgCg");
	this.shape.setTransform(6.1,7.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol54, new cjs.Rectangle(0,0,12.2,14.1), null);


(lib.Symbol53 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-46,-14.9)).s().p("AghBYIgMgMQgTgXgEgOQgCgHgFgBQgFABgCgBQgDgBABgEQABgOAGgZIAJgmQAGgaALgIQAJgHAXAAQAjAAAOADQAcAHANASQANAQABAYIAAArIAAAWQgEAVgUAOQgGAFgLAEQgKAFgJACIgfABQgTAAgIgFg");
	this.shape.setTransform(8.5,9.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol53, new cjs.Rectangle(0,0,17.1,18.6), null);


(lib.Symbol52 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-36.2,-32.2)).s().p("AgIBIIgOgCIgFgBQgEgBgCgDQgTgTgGgMQgEgIgCgKQgFgQABgKQAAgHADgOQACgIACgDIAHgGIATgJIAKgEIAJgFQANgHAVABQAXAAAJAHQAMAJAEAZQAEASgBANQgBAPgKAXQgCAIgEAFQgDAEgJAGIgKAFIgNADIgNADIgDABIgJgBg");
	this.shape.setTransform(6.9,7.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol52, new cjs.Rectangle(0,0,13.8,14.5), null);


(lib.Symbol51 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-18.3,-40.8)).s().p("AgQAwQgKgDgNgNIgHgKIgFgLIgBgIIACgGIABgGQAEgHAAgFIAAgDIAJgKQAEgHAKgJIACgBIAVAAIAMABIAOAFIAHADIAKAKQADAFACAHQAEAMAAADQAAAFgEAJQgDALgFAHIgJALQgJAJgHACQgGACgKAAQgMgBgEgCg");
	this.shape.setTransform(5.3,5.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol51, new cjs.Rectangle(0,0,10.5,10.3), null);


(lib.Symbol50 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-13.6,-30.7)).s().p("AgeBLQgHgCgGgHQgIgHgEgIIgLgfIgCgJIACgLQABgYAEgLIAIgOQAMgSAKgEQAIgEAQAAIAPACIAQAGIAUAGQAMADAHAFQAEAEABAEQABAEgCAHQgGAYgPARIgSAUIgMAMIgFACQgJAFgGAHIgFAGIgHAIIgCAHQgEACgEAAIgEgBg");
	this.shape.setTransform(6.9,7.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol50, new cjs.Rectangle(0,0,13.8,15.2), null);


(lib.Symbol49 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04_1, null, new cjs.Matrix2D(1,0,0,1,-4.1,-43.5)).s().p("AgoAtIAAhJQAFgGAFgDQAKgGAUgBQAOgCAIAFQAKAGAEATIAFArQAAAHgDALIAAAAg");
	this.shape.setTransform(4.1,4.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol49, new cjs.Rectangle(0,0,8.3,9.1), null);


(lib.Symbol30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_02, null, new cjs.Matrix2D(0.866,-0.5,0.5,0.866,-55.5,2.2)).s().p("AhGAkQgGgEgEgHQgJgLAAgWQgBgIADgCQACgDAGgDQACgCAAgDQAAAAAAgBQAAgBAAAAQAAAAgBgBQAAAAgBAAIAYgMQAHgFAHgCQAHgCAKABIAqALQAXAGALAGIASALIALAHIAGAGQADAFAAAGQAAAGgFAGQgDAFgNAIIgGADIgIAEIgKACIgdAHIgVAFIgPABQgbAAgXgRg");
	this.shape.setTransform(8.5,6.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol30, new cjs.Rectangle(-0.5,1.4,18.1,10.7), null);


(lib.Symbol29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_02, null, new cjs.Matrix2D(0.707,0.707,-0.707,0.707,-13.7,-37.2)).s().p("AAIAtQgcgBgWgJIgNgFIgLgDIgNgDIgNgEQgOgGgGgLQgEgFADgHQABgGAHgFQAFgFALgIQALgJAGgBQADgBAMAAQAVAAAVABIAXADQALACAeAAQAaABAPACQAIACADACQAEADAEAJQAJARgDALQgEAQgSAIQgIAEgYAEQgeAEgTAAIgEAAg");
	this.shape.setTransform(8.7,9.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol29, new cjs.Rectangle(-2.8,4.8,23.1,9), null);


(lib.Symbol28 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_02, null, new cjs.Matrix2D(0.796,-0.605,0.605,0.796,-45.4,26.4)).s().p("AgRAwQgOgCgYgLIgSgHIgMgIQgDgDgCgGIgFgIIgCgIIAzgnQAMgEADAAIALAAIARAFIAMAFIAPACQAeAGAdARQAJAGADAGQADAFAAAFQAAAKgFAIQgGAJgJADQgGACgIABIgQACIghAEIgTABIgNgBg");
	this.shape.setTransform(7.6,6.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol28, new cjs.Rectangle(-2.2,1.6,19.6,9.9), null);


(lib.Symbol27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_02, null, new cjs.Matrix2D(0.799,0.601,-0.601,0.799,-14.5,-18.1)).s().p("AAWAmQglgDgUABIgiABQgSAAgNgJQgIgGgBgGQgBgIAEgHQACgDAIgHQARgPAKgEQAJgEASgCQA/gIAoATIALAGIAWARIAKALIAEAGQABAAAAABQABAAAAABQAAAAAAABQAAABAAAAIgCAFQgHAGgUACIgCABQghADgTAAIgFAAg");
	this.shape.setTransform(9.4,5.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol27, new cjs.Rectangle(-1.6,1.9,22.1,7.5), null);


(lib.Symbol26 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_02, null, new cjs.Matrix2D(0.931,-0.366,0.366,0.931,-10.6,-14.6)).s().p("Ag3gSQABgPADgYQAFgYAEgJQAHgPALgFQAFgCAFgBQAoAUASAmIABAGQAIAxADA8QAAAXgGAKQgHAKgOAFQgHADgRACIgIABg");
	this.shape.setTransform(4.6,11.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol26, new cjs.Rectangle(-0.9,0.2,11.1,22.8), null);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04, null, new cjs.Matrix2D(1,0,0,1,-8.6,-25.4)).s().p("Ag7gtIAiA1IAVhJQAPAYAXAaQAUAWAdASQhSACg0AnQg5hSAxgdg");
	this.shape.setTransform(8.3,6.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol22, new cjs.Rectangle(0,0,16.6,13.2), null);


(lib.Symbol21 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04, null, new cjs.Matrix2D(1,0,0,1,-61.7,-22.9)).s().p("AhRgTQAhgjAugMQAggJAnADQAZAOgVBxIhCASQgohUgwgIg");
	this.shape.setTransform(8.2,7.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol21, new cjs.Rectangle(0,0,16.5,14.7), null);


(lib.Symbol20 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04, null, new cjs.Matrix2D(1,0,0,1,-52.2,-26.4)).s().p("AhtAgQAPggAKgJIAAgBQAdggAqgLIAFgBQAKgCALACIAAAAQAxAIAoBUIAIASg");
	this.shape.setTransform(11,5.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol20, new cjs.Rectangle(0,0,22.1,11.3), null);


(lib.Symbol19 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04, null, new cjs.Matrix2D(1,0,0,1,-50.7,-12.5)).s().p("AhqACIBKhNQAagRAZARIBYBwQguAMghAiIgBAAQgKgCgLACIgFABQgygsg5gmg");
	this.shape.setTransform(10.7,8.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol19, new cjs.Rectangle(0,0,21.4,16.8), null);


(lib.Symbol18 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_04, null, new cjs.Matrix2D(1,0,0,1,-27.6,-16.5)).s().p("AhqByQgdgSgUgXQgXgagQgYQg6hVAhg6QA9hDB/BDQBTAlBIAvQA5AkAzAsQgrAMgdAgIAAAAIiYBDQhEgOgugbg");
	this.shape.setTransform(23.2,15.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol18, new cjs.Rectangle(0,0,46.4,31), null);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-74.5,-10)).s().p("AACAZIgPgEIgJgDQgCgCAAgGIABgNQACgLgDgDQAMgHAGgBIAJAAQAIAAAEABQAHADACAFIABACIAAAOIgBACIgBAIIgDAFQAAAIgDADIgGAAIgJgBg");
	this.shape.setTransform(2.5,2.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol17, new cjs.Rectangle(0,0,5,5.3), null);


(lib.Symbol16 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-65,-14)).s().p("AgIAjQgPgDgHgKQgGgJABgPQAAgWAMgLQAIgCAMAEIASAHIAMAFQAHAEACAFQABAEgBAIIgFAaQgBAEgCACQgDAEgIAAIgLABQgJAAgFgCg");
	this.shape.setTransform(3.6,3.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol16, new cjs.Rectangle(0,0,7.3,7.4), null);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-58.4,-5.8)).s().p("AgCAnQgJgDgNgHIgPgIQgFgEgEgGQAEgBACgEIACgHIAEgHIADgKIAEgGIADgGQADgEAGgFIAFgCIADAAIAQABIAUAEIAGADQAHAEACAEIADAHIADANIAAAQQABAFgBAEIgFAJIgBAAQAAAAgBAAQgBAAAAABQgBAAAAAAQgBAAAAABIgCADQgCACgFACIgEADIgCABIgGAAQgIAAgLgDg");
	this.shape.setTransform(4.9,4.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol15, new cjs.Rectangle(0,0,9.7,8.5), null);


(lib.Symbol14 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-40.8,-13)).s().p("AgaAoIgHgBQgFgCgEgFIAAgbIADgLIADgFIADgKQACgHAFgEIAFgEIAEgDIAGgCIAAAAQAKADASAAIAFAAQAIAFAEAFIAEAEIACAFIACADIABAGIAAAQIgDALIgDAFQgEALgDAEQgCADgEABIgHAAIgMABQgPAAgQgCg");
	this.shape.setTransform(4.3,4.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol14, new cjs.Rectangle(0,0,8.6,8.4), null);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-41.1,-4)).s().p("AAQAmIgMgBIgIAAIgIAAIgJgDQgPgFgFgLQgCgFgBgIIgBgJIABgHQACgLADgHIAGgKIBNAAQACADAAAIIAAAVIgBAJIgHAVQgEAHgFAFIgFAEIgEABg");
	this.shape.setTransform(4.6,4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol13, new cjs.Rectangle(0,0,9.2,8), null);


(lib.Symbol12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-27.6,-4.8)).s().p("AgWAtIgFgBIgDgDIgBgBIgKgLIgBgGQAAAAABAAQAAAAABAAQAAgBABAAQAAAAABgBQAAAAAAgBQABAAAAgBQAAAAAAgBQAAAAAAgBIgCgFIgBgGIABgMQAFgTAGgIQAFgHALgHIAiAAIAKAFQAIAHACAJIABAJIgBAWIAAAHIgCAHQgCAOgEAGIgDAFQgCADgDABIgDAAIgNAAQgUAAgMgDg");
	this.shape.setTransform(4.3,4.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol12, new cjs.Rectangle(0,0,8.6,9.6), null);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-20.7,-12.5)).s().p("AgHArIgWgLQgFgCgCgDQgDgEAAgIIAAgJQABgEACgFQABgEADgEIAFgFIALgLQADgEgBgDIgCgCIABgEIAHgCIAIAAQANAAAFADIADACIAFAGIAEAEIAEAHIADAJIACAEIABAGIAAANIgBARIgCAHIgDAHIgjABg");
	this.shape.setTransform(4,4.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol11, new cjs.Rectangle(0,0,8.1,8.8), null);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-14,-8.5)).s().p("AgOAbQgMgCgFgDQgJgFAAgKQADAAACgEIACgGQACgOANgHQAIgEASAAQASABAGACQAEACACACQADACAAAEIAAAFIgCABIgDAFQgDAEgBADIgBAIIgBAAIgLAIQgFAGgDACIgHABIgSgBg");
	this.shape.setTransform(4.1,2.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,8.3,5.7), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.Icecream_hat_03, null, new cjs.Matrix2D(1,0,0,1,-4.2,-14.6)).s().p("AgqAiIAAg7IAFgDIAFgEQAFgBAFAAIAQAAIAJABIAJAFIAJAIQAFAEADAIIACAFIAIAQQADAJAAAJIAAACg");
	this.shape.setTransform(4.3,3.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,8.5,6.9), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#410912").s().p("AhTBTQgigigBgxQABgwAigjQAjgiAwgBQAxABAiAiQAjAjAAAwQAAAxgjAiQgiAjgxAAQgwAAgjgjg");
	this.shape.setTransform(11.9,11.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,23.7,23.7), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#FFFFFF","#B8C6C1"],[0.604,0.867],-6.8,-7.5,0,-6.8,-7.5,24.2).s().p("AhTBTQgigigBgxQABgwAigjQAjgiAwgBQAxABAiAiQAjAjAAAwQAAAxgjAiQgiAjgxAAQgwAAgjgjg");
	this.shape.setTransform(11.9,11.9);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,23.7,23.7), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgrAsQgRgTgBgZQABgZARgSQASgRAZgBQAZABATARQASASAAAZQAAAZgSATQgTASgZAAQgZAAgSgSg");
	this.shape.setTransform(6.2,6.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,12.3,12.3), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgeAeQgMgMAAgSQAAgRAMgNQANgMARAAQASAAAMAMQANANAAARQAAASgNAMQgMANgSAAQgRAAgNgNg");
	this.shape.setTransform(4.3,4.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,8.7,8.6), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgSgUQAUgCAVgBQATAIgBAKQABAMgSAGQgeAEgeAGQgPgZAhgSg");
	this.shape.setTransform(4.2,2.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,8.3,4.7), null);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgyABQgCgLAUgHQAhgBAlABQAMAEABANQgCANgQABQghABgfAEQgQgCgDgQg");
	this.shape.setTransform(5.1,1.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol3, new cjs.Rectangle(0,0,10.3,3.8), null);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgdAPQgOgBgBgOQACgNAQgCQAeACAgAGQALAEgDAJQgDAKgMAAQgdgCgdABg");
	this.shape.setTransform(4.5,1.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol2, new cjs.Rectangle(0,0,9,3.2), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgfAJQgRgFAEgJQAFgJAPAAQAfAGAhAJQAOAMgUACQgigEgfgCg");
	this.shape.setTransform(4.6,1.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,9.2,3), null);


(lib.red = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_red();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.red, new cjs.Rectangle(0,0,32,22), null);


(lib.mouth07 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E82A2B").s().p("Ah3AXQAxhQBGARQBFAQAzBEQh0gih7ANg");
	this.shape.setTransform(26.4,16.9,1,1.201);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C070E").s().p("AB3BJQg0hFhEgQQhGgQgxBQQhbADg1AeQAFglARggQATgiAigeQBNhEBnAFQBlAGA5A2QA6A1ASAkQARAkAXAmg");
	this.shape_1.setTransform(26.5,13.4,1,1.201);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mouth07, new cjs.Rectangle(0,0,53.1,26.9), null);


(lib.mouth06 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.shape = new cjs.Shape();
	this.shape.graphics.rf(["#E2E0E0","#FFFFFF","#B39EA1"],[0.635,0.922,1],0,17.9,0,0,17.9,24.9).s().p("AABAcQiGAAhpAnQATgkAhgeQBOhDBsAAQBtAABNBDQAiAeATAkQhogniGAAg");
	this.shape.setTransform(26.6,6.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#E82A2B").s().p("AABgaQBGAAA0A0Qh+gFh3AGQA1g1BGAAg");
	this.shape_1.setTransform(26.6,16.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3C070E").s().p("AjugOQBpgnCGAAQCGAABpAnQAQAdAFAjQhGgGhFgCQg0g0hGAAQhFAAg1A1QhHADhEAIQAFglASgfg");
	this.shape_2.setTransform(26.6,14.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mouth06, new cjs.Rectangle(0,0,53,22), null);


(lib.mouth = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E72829").s().p("AhuACQgJgIgHgJIgEgHQB6g2CLA4IgFAHIgKALQguAyhDABIgDAAQhAAAgugvg");
	this.shape.setTransform(16.9,14.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3D0802").s().p("AiFBAQgbgoAAg0QAAgSADgQQAEAIALAHQALAHASAHQAvAQBCgBQBDAAAvgRQASgHALgHQAMgHAEgJQACAOAAAQQAAA5ghAsQiKg4h7A2g");
	this.shape_1.setTransform(17.2,6.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.mouth, new cjs.Rectangle(0,-0.5,33.3,29.5), null);


(lib.body06 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_body_06();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.body06, new cjs.Rectangle(0,0,163,167), null);


(lib.body05 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_body_04_1();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.body05, new cjs.Rectangle(0,0,147,152), null);


(lib.body02 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_body_02();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.body02, new cjs.Rectangle(0,0,141,158), null);


(lib.body01 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_body_01();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.body01, new cjs.Rectangle(0,0,154,207), null);


(lib.body_04 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_body_04();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.body_04, new cjs.Rectangle(0,0,145,144), null);


(lib.berry = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Icecream_Berry();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.berry, new cjs.Rectangle(0,0,68,104), null);


(lib.heart2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#E92A2B").s().p("AjyNSQkxkjjQkRQkil/g1kmQhBlxE4jRQEWiuFBB2QCgA8BoBeIBEg6QBWhCBigoQE4h9EtC/QEgDKhEFvQg3EmkWGAQjIETkjEmQiSCThqBbQhuhaiZiRg");
	this.shape.setTransform(111,108.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.heart2, new cjs.Rectangle(0,0,222.1,217.1), null);


(lib.glaz = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AglADIAAAAIgCAAIgBgBIgBgCIABAAIAMgIIAAgBQAQgJAOgBIAAAAQAPAAAKAKQAJAJADAIQAEAIgCAEIAAAAQghgggtAPg");
	this.shape.setTransform(3.6,1.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.glaz, new cjs.Rectangle(-0.6,-0.2,8.3,4.1), null);


(lib.f = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgXAJQgZgEgGgKQgGgKAZALQAYAJAlgIQAlgJgIAIQgIAHgXAFQgNADgMAAQgLAAgLgCg");
	this.shape.setTransform(5.6,1.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.f, new cjs.Rectangle(0,0,11.3,2.2), null);


(lib.ey = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F3F3F3").s().p("AhuAwQBLiKCSAwQhWBhhXAAQgYAAgYgHg");
	this.shape.setTransform(11.1,14.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#F0F0F0").s().p("AhsgPQCLhDBOBQQg4Awg0AAQg5AAg0g9g");
	this.shape_1.setTransform(62.8,5.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.ey, new cjs.Rectangle(0,1.3,73.7,19.1), null);


(lib.cloud = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#B5B5B5").s().p("EAPVAluQkVgljpiqQjsisg5jUQjsGakdB8QjvBnj1hmQjMhWiXjDQiLiyAAiFQlrCOkugNQkKgMi7iBQiph0hCi4QhBixAziwQkcgHi8ipQiqiZgrjvQgrjpBgjUQBmjiDihtQjdkaAdktQAakLDPjNQDHjFEJgvQEagyDcCXQgZmtD+i/QD/i/EIgMQEIgME1CGQE0CHAqDgQCNlsETicQEUicGDA3QGEA2C3DeQC2DfgwESQFpiWEyAXQEZAVCzCfQCtCZAbDpQAbDxiLD8QGNAtCsDvQCrDugxESQgxETiGCjQiGCkh/AtQB+GBhiEQQhUDojqB7QjKBqj7gBQjrgBiPhaQhCF8j4CmQimBwjbAAQg/AAhEgKg");
	this.shape.setTransform(338.7,240.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.cloud, new cjs.Rectangle(0,-1.7,677.5,484.7), null);


(lib.brov2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgSAEQASgNATgBIAAAKQgQAAgRAJIgEACg");
	this.shape.setTransform(1.9,1.1);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.brov2, new cjs.Rectangle(0,0,3.8,2.2), null);


(lib.smile1anim = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_8 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(8).call(this.frame_8).wait(1));

	// Слой 1
	this.instance = new lib.share1();
	this.instance.parent = this;
	this.instance.setTransform(16,16.1,0.969,0.966,0,0,0,16,16);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleY:1.08},2).to({regX:15.8,scaleX:1.3,scaleY:1.16,x:15.8},3).to({regX:16,scaleX:1.11,scaleY:1.11,x:16,y:16},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.7,-0.6,33.4,33.3);


(lib.smile1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.instance = new lib.rotik();
	this.instance.parent = this;
	this.instance.setTransform(16,21,1,1,0,0,0,9.1,1.9);

	this.instance_1 = new lib.glaz();
	this.instance_1.parent = this;
	this.instance_1.setTransform(22,12.1,1,1,0,-7.5,172.5,3.5,1.7);

	this.instance_2 = new lib.brov();
	this.instance_2.parent = this;
	this.instance_2.setTransform(21.6,6.6,1,1,0,0,180,1.8,1.3);

	this.instance_3 = new lib.glaz();
	this.instance_3.parent = this;
	this.instance_3.setTransform(10.6,12.4,1,1,0,0,0,3.6,1.6);

	this.instance_4 = new lib.brov();
	this.instance_4.parent = this;
	this.instance_4.setTransform(8.5,7,1,1,0,0,0,1.8,1.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4,p:{x:8.5,y:7}},{t:this.instance_3,p:{x:10.6,y:12.4}},{t:this.instance_2,p:{x:21.6,y:6.6}},{t:this.instance_1,p:{x:22,y:12.1}},{t:this.instance,p:{scaleX:1,scaleY:1,x:16,y:21}}]}).to({state:[{t:this.instance_4,p:{x:8.3,y:7.7}},{t:this.instance_3,p:{x:10.1,y:13.6}},{t:this.instance_2,p:{x:21.9,y:7.4}},{t:this.instance_1,p:{x:22.6,y:13.3}},{t:this.instance,p:{scaleX:1.066,scaleY:0.872,x:16.1,y:22.6}}]},1).wait(3));

	// Слой 1
	this.instance_5 = new lib.share1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_6 = new lib.smile1anim();
	this.instance_6.parent = this;
	this.instance_6.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_7 = new lib.share2();
	this.instance_7.parent = this;
	this.instance_7.setTransform(16,16,1,1,0,0,0,16,16);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACfAAQAABCgvAuQguAvhCAAQhBAAgvgvQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAvAvAABBg");
	this.shape.setTransform(16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#999C28").s().p("AhwBwQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAwAvgBBBQABBCgwAuQguAwhCgBQhBABgvgwg");
	this.shape_1.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-1.3,34.5,34.5);


(lib.mol = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// cloud (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_19 = new cjs.Graphics().p("Almj2IOOA/Ik8QoIw7EzgAHpn9QgZgWgogDQgsgEgzAWQAHgngaggQgaggg3gHQg3gIgnAWQgoAWgUA0QgFgggsgTQgrgTgmACIgNABIArpGINpAVIlPK0QgHgUgRgPg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(19).to({graphics:mask_graphics_19,x:36.1,y:86.4}).wait(5).to({graphics:null,x:0,y:0}).wait(76));

	// lightstrike
	this.instance = new lib.lightstrike();
	this.instance.parent = this;
	this.instance.setTransform(57.8,26.3,0.129,0.147,-15,0,0,198.7,308.2);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({_off:false},0).to({regX:198.1,scaleX:0.09,rotation:0,x:38.8,y:109.3},2).to({_off:true},1).wait(2).to({_off:false,regX:196.8,regY:0,scaleY:0.18,skewX:2,x:33.7,y:71.5},0).to({_off:true},1).wait(1).to({_off:false,scaleY:0.21},0).to({_off:true},1).wait(1).to({_off:false,regX:198.1,regY:308.2,scaleY:0.15,skewX:0,x:38.8,y:109.3},0).to({_off:true},2).wait(70));

	// cloud
	this.instance_1 = new lib.cloud();
	this.instance_1.parent = this;
	this.instance_1.setTransform(48,58.1,0.142,0.142,0,0,0,337.7,241.7);
	this.instance_1.alpha = 0.43;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15).to({regY:242,x:48.1,y:57},4).to({regX:337.5,regY:242.2,scaleX:0.15,scaleY:0.13},2).to({regX:337.7,regY:242,scaleX:0.14,scaleY:0.14,alpha:0.121},1).wait(2).to({scaleX:0.16,alpha:0.68},0).to({_off:true},1).wait(1).to({_off:false,regX:337.6,regY:242.4,scaleX:0.15,y:55.9,alpha:0.578},0).to({regX:337.2,scaleX:0.15,y:55.3,alpha:0.527},1).to({regX:337.7,regY:241.7,scaleX:0.14,x:48,y:54.1,alpha:0.43},2).wait(71));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,23.5,96.3,68.9);


(lib.Symbol48 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 63
	this.instance = new lib.Symbol63();
	this.instance.parent = this;
	this.instance.setTransform(130.5,39,0.386,0.386,0,0,0,3.5,5.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:5.4,scaleX:1.21,scaleY:1.21,x:130.6,y:39.1},2).to({scaleX:1,scaleY:1,y:39},2).wait(11).to({scaleX:1.21,scaleY:1.21,y:39.1},2).to({regY:5.3,scaleX:0.39,scaleY:0.39,x:130.5,y:39},2).wait(1));

	// Symbol 62
	this.instance_1 = new lib.Symbol62();
	this.instance_1.parent = this;
	this.instance_1.setTransform(112.2,36.2,0.312,0.312,0,0,0,3.9,4);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).to({scaleX:1.36,scaleY:1.36,x:112.3,y:36.1},2).to({regX:3.8,scaleX:1,scaleY:1,x:112.1},2).wait(9).to({regX:3.9,scaleX:1.36,scaleY:1.36,x:112.3},2).to({scaleX:0.31,scaleY:0.31,x:112.2,y:36.2},2).to({_off:true},1).wait(1));

	// Symbol 61
	this.instance_2 = new lib.Symbol61();
	this.instance_2.parent = this;
	this.instance_2.setTransform(115.9,24.3,0.382,0.382,0,0,0,6.5,6.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).to({regX:6.7,scaleX:1.23,scaleY:1.23,x:118,y:23.4},2).to({regX:6.6,scaleX:1,scaleY:1,x:117.9,y:23.3},2).wait(7).to({regX:6.7,scaleX:1.23,scaleY:1.23,x:118,y:23.4},2).to({regX:6.5,scaleX:0.38,scaleY:0.38,x:115.9,y:24.3},2).to({_off:true},1).wait(2));

	// Symbol 60
	this.instance_3 = new lib.Symbol60();
	this.instance_3.parent = this;
	this.instance_3.setTransform(101,24.2,0.274,0.274,0,0,0,5,5.3);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off:false},0).to({scaleX:1.22,scaleY:1.22,x:101.1},2).to({regX:4.9,regY:5.2,scaleX:1,scaleY:1,x:100.9,y:24.1},2).wait(5).to({regX:5,regY:5.3,scaleX:1.22,scaleY:1.22,x:101.1,y:24.2},2).to({scaleX:0.27,scaleY:0.27,x:101},2).to({_off:true},1).wait(3));

	// Symbol 59
	this.instance_4 = new lib.Symbol59();
	this.instance_4.parent = this;
	this.instance_4.setTransform(94,12.1,0.301,0.301,0,0,0,7.7,5.7);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(4).to({_off:false},0).to({scaleX:1.18,scaleY:1.18,x:95.1,y:9.2},2).to({regX:7.6,scaleX:1,scaleY:1,x:94.9,y:9.1},2).wait(3).to({regX:7.7,scaleX:1.18,scaleY:1.18,x:95.1,y:9.2},2).to({scaleX:0.3,scaleY:0.3,x:94,y:12.1},2).to({_off:true},1).wait(4));

	// Symbol 58
	this.instance_5 = new lib.Symbol58();
	this.instance_5.parent = this;
	this.instance_5.setTransform(89.7,33.6,0.441,0.441,0,0,0,4.7,4.4);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off:false},0).to({regX:4.6,scaleX:1.31,scaleY:1.31,x:89.6},2).to({scaleX:1,scaleY:1},2).wait(3).to({scaleX:1.31,scaleY:1.31},2).to({regX:4.7,scaleX:0.44,scaleY:0.44,x:89.7},2).to({_off:true},1).wait(4));

	// Symbol 57
	this.instance_6 = new lib.Symbol57();
	this.instance_6.parent = this;
	this.instance_6.setTransform(84.1,18.1,0.387,0.387,0,0,0,5.3,4.7);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(5).to({_off:false},0).to({scaleX:1.26,scaleY:1.26},2).to({regX:5.2,scaleX:1,scaleY:1,x:84},2).wait(1).to({regX:5.3,scaleX:1.26,scaleY:1.26,x:84.1},2).to({scaleX:0.39,scaleY:0.39},2).to({_off:true},1).wait(5));

	// Symbol 56
	this.instance_7 = new lib.Symbol56();
	this.instance_7.parent = this;
	this.instance_7.setTransform(70.1,6.9,0.27,0.27,0,0,0,7.6,6.9);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(5).to({_off:false},0).to({regX:7.5,regY:6.8,scaleX:1.24,scaleY:1.24,x:70},2).to({regY:6.9,scaleX:1,scaleY:1},2).wait(1).to({regY:6.8,scaleX:1.24,scaleY:1.24},2).to({regX:7.6,regY:6.9,scaleX:0.27,scaleY:0.27,x:70.1},2).to({_off:true},1).wait(5));

	// Symbol 55
	this.instance_8 = new lib.Symbol55();
	this.instance_8.parent = this;
	this.instance_8.setTransform(65.7,22.7,0.301,0.301,0,0,0,7,7.3);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(4).to({_off:false},0).to({scaleX:1.17,scaleY:1.17,x:65.8},2).to({regX:6.9,regY:7.2,scaleX:1,scaleY:1,x:65.7,y:22.6},2).wait(3).to({regX:7,regY:7.3,scaleX:1.17,scaleY:1.17,x:65.8,y:22.7},2).to({scaleX:0.3,scaleY:0.3,x:65.7},2).to({_off:true},1).wait(4));

	// Symbol 54
	this.instance_9 = new lib.Symbol54();
	this.instance_9.parent = this;
	this.instance_9.setTransform(30.7,20.2,0.383,0.383,0,0,0,6.2,7);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(4).to({_off:false},0).to({scaleX:1.16,scaleY:1.16,x:29.8,y:19.2},2).to({regX:6.1,scaleX:1,scaleY:1,x:29.7,y:19.1},2).wait(3).to({regX:6.2,scaleX:1.16,scaleY:1.16,x:29.8,y:19.2},2).to({scaleX:0.38,scaleY:0.38,x:30.7,y:20.2},2).to({_off:true},1).wait(4));

	// Symbol 53
	this.instance_10 = new lib.Symbol53();
	this.instance_10.parent = this;
	this.instance_10.setTransform(46,14.9,0.345,0.345,0,0,0,8.6,9.2);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(3).to({_off:false},0).to({regX:8.5,regY:9.3,scaleX:1.11,scaleY:1.11},2).to({scaleX:1,scaleY:1},2).wait(5).to({scaleX:1.11,scaleY:1.11},2).to({regX:8.6,regY:9.2,scaleX:0.35,scaleY:0.35},2).to({_off:true},1).wait(3));

	// Symbol 52
	this.instance_11 = new lib.Symbol52();
	this.instance_11.parent = this;
	this.instance_11.setTransform(36.2,32.2,0.324,0.324,0,0,0,7,7.2);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(3).to({_off:false},0).to({scaleX:1.17,scaleY:1.17,x:36.3},2).to({regX:6.9,scaleX:1,scaleY:1,x:36.2,y:32.1},2).wait(5).to({regX:7,scaleX:1.17,scaleY:1.17,x:36.3,y:32.2},2).to({scaleX:0.32,scaleY:0.32,x:36.2},2).to({_off:true},1).wait(3));

	// Symbol 51
	this.instance_12 = new lib.Symbol51();
	this.instance_12.parent = this;
	this.instance_12.setTransform(18.4,40.8,0.32,0.32,0,0,0,5.3,5.2);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(2).to({_off:false},0).to({scaleX:1.27,scaleY:1.27,y:40.9},2).to({regX:5.2,scaleX:1,scaleY:1,x:18.3},2).wait(7).to({regX:5.3,scaleX:1.27,scaleY:1.27,x:18.4},2).to({scaleX:0.32,scaleY:0.32,y:40.8},2).to({_off:true},1).wait(2));

	// Symbol 50
	this.instance_13 = new lib.Symbol50();
	this.instance_13.parent = this;
	this.instance_13.setTransform(15,32.1,0.388,0.388,0,0,0,6.9,7.6);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(1).to({_off:false},0).to({scaleX:1.15,scaleY:1.15,x:13.7,y:30.8},2).to({regY:7.5,scaleX:1,scaleY:1,y:30.6},2).wait(9).to({regY:7.6,scaleX:1.15,scaleY:1.15,y:30.8},2).to({scaleX:0.39,scaleY:0.39,x:15,y:32.1},2).to({_off:true},1).wait(1));

	// Symbol 49
	this.instance_14 = new lib.Symbol49();
	this.instance_14.parent = this;
	this.instance_14.setTransform(6.1,43.5,0.319,0.319,0,0,0,4.1,4.5);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(1).to({_off:false},0).to({scaleX:1.21,scaleY:1.21,x:4.1},2).to({scaleX:1,scaleY:1},2).wait(9).to({scaleX:1.21,scaleY:1.21},2).to({scaleX:0.32,scaleY:0.32,x:6.1},2).to({_off:true},1).wait(1));

	// Layer 18
	this.instance_15 = new lib.Tween5("synched",0);
	this.instance_15.parent = this;
	this.instance_15.setTransform(65.5,13.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).to({startPosition:0},2).to({startPosition:0},2).to({startPosition:0},5).to({startPosition:0},1).to({startPosition:0},5).to({startPosition:0},2).to({startPosition:0},2).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,134,48);


(lib.Symbol25 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 20 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_14 = new cjs.Graphics().p("ADzDAIighuIA4gkICXBAIAAhbIidh8IAIBYIibB9Igeg+ICDhxIgeg0IirBoIhoAOIggBvIg8AAIgElNIJ1AAIAAG/g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(14).to({graphics:mask_graphics_14,x:30.7,y:14.8}).wait(10));

	// Symbol 26
	this.instance = new lib.Symbol26();
	this.instance.parent = this;
	this.instance.setTransform(5.4,17.4,1,1,15,0,0,5.4,11.2);
	this.instance._off = true;

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({_off:false},0).to({scaleX:0.54,scaleY:0.54,rotation:-15,x:9.8,y:28},6).to({_off:true},1).wait(3));

	// Symbol 27
	this.instance_1 = new lib.Symbol27();
	this.instance_1.parent = this;
	this.instance_1.setTransform(23.4,7,1,1,-30,0,0,9.3,7);
	this.instance_1._off = true;

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({_off:false},0).wait(2).to({regY:7.2,scaleX:0.49,scaleY:0.49,x:23.5,y:15},6).to({_off:true},1).wait(1));

	// Symbol 29
	this.instance_2 = new lib.Symbol29();
	this.instance_2.parent = this;
	this.instance_2.setTransform(36.2,16.1,1,1,-45,0,0,9.1,9);
	this.instance_2._off = true;

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({_off:false},0).wait(1).to({regY:9.3,scaleX:0.59,scaleY:0.59,rotation:-60,x:35.6,y:25.3},6).to({_off:true},1).wait(2));

	// Symbol 30
	this.instance_3 = new lib.Symbol30();
	this.instance_3.parent = this;
	this.instance_3.setTransform(49,25.5,1,1,30,0,0,8.1,6.5);
	this.instance_3._off = true;

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(14).to({_off:false},0).to({regX:8.2,regY:6.4,scaleX:0.69,scaleY:0.69,rotation:60,x:49.7,y:34},6).to({_off:true},1).wait(3));

	// Symbol 28
	this.instance_4 = new lib.Symbol28();
	this.instance_4.parent = this;
	this.instance_4.setTransform(52.1,7.3,1,1,36,0,0,8,7.2);
	this.instance_4._off = true;

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(14).to({_off:false},0).wait(3).to({regX:8.1,scaleX:0.64,scaleY:0.64,rotation:51,y:15.3},6).wait(1));

	// Symbol 26
	this.instance_5 = new lib.Symbol26();
	this.instance_5.parent = this;
	this.instance_5.setTransform(11,-4,0.824,0.824,125.9,0,0,5.4,11.2);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({_off:false},0).to({scaleX:1,scaleY:1,rotation:15,x:5.4,y:17.4},5).wait(4).to({_off:true},1).wait(10));

	// Symbol 27
	this.instance_6 = new lib.Symbol27();
	this.instance_6.parent = this;
	this.instance_6.setTransform(21,-17.8,0.823,0.823,-74.9,0,0,9.3,7.1);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(5).to({_off:false},0).to({regY:7,scaleX:1,scaleY:1,rotation:-30,x:23.4,y:7},5).wait(3).to({_off:true},1).wait(10));

	// Symbol 29
	this.instance_7 = new lib.Symbol29();
	this.instance_7.parent = this;
	this.instance_7.setTransform(33.2,-20.6,0.823,0.823,52.9,0,0,9.2,9);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(6).to({_off:false},0).to({regX:9.1,scaleX:1,scaleY:1,rotation:-45,x:36.2,y:16.1},5).to({rotation:-45},2).to({_off:true},1).wait(10));

	// Symbol 30
	this.instance_8 = new lib.Symbol30();
	this.instance_8.parent = this;
	this.instance_8.setTransform(40.6,-18.9,0.823,0.823,-135.1,0,0,8.2,6.6);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(7).to({_off:false},0).to({regX:8.1,regY:6.5,scaleX:1,scaleY:1,rotation:30,x:49,y:25.5},5).wait(1).to({_off:true},1).wait(10));

	// Symbol 28
	this.instance_9 = new lib.Symbol28();
	this.instance_9.parent = this;
	this.instance_9.setTransform(48.4,-18.9,0.823,0.823,118.1,0,0,7.8,7.2);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(8).to({_off:false},0).to({regX:8,scaleX:1,scaleY:1,rotation:36,x:52.1,y:7.3},5).to({_off:true},1).wait(10));

	// Layer 9 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_0 = new cjs.Graphics().p("Ao1nDIRrAAIAALdQkkj8llBQQkxAwixEmg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:mask_1_graphics_0,x:39.4,y:-7.2}).wait(9).to({graphics:null,x:0,y:0}).wait(15));

	// Symbol 26
	this.instance_10 = new lib.Symbol26();
	this.instance_10.parent = this;
	this.instance_10.setTransform(21.5,17,0.577,0.577,-129.1,0,0,5.4,11.3);

	var maskedShapeInstanceList = [this.instance_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({regY:11.2,scaleX:0.82,scaleY:0.82,rotation:-234.1,x:11,y:-4},4).to({_off:true},1).wait(19));

	// Symbol 27
	this.instance_11 = new lib.Symbol27();
	this.instance_11.parent = this;
	this.instance_11.setTransform(28.5,9.6,0.576,0.576,-164.9,0,0,9.2,7.1);
	this.instance_11._off = true;

	var maskedShapeInstanceList = [this.instance_11];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(1).to({_off:false},0).to({regX:9.3,scaleX:0.82,scaleY:0.82,rotation:-74.9,x:21,y:-17.8},4).to({_off:true},1).wait(18));

	// Symbol 29
	this.instance_12 = new lib.Symbol29();
	this.instance_12.parent = this;
	this.instance_12.setTransform(37,7.6,0.576,0.576,157.9,0,0,9.1,9.1);
	this.instance_12._off = true;

	var maskedShapeInstanceList = [this.instance_12];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(2).to({_off:false},0).to({regX:9.2,regY:9,scaleX:0.82,scaleY:0.82,rotation:52.9,x:33.2,y:-20.6},4).to({_off:true},1).wait(17));

	// Symbol 30
	this.instance_13 = new lib.Symbol30();
	this.instance_13.parent = this;
	this.instance_13.setTransform(42.1,8.8,0.576,0.576,89.9,0,0,8.1,6.5);
	this.instance_13._off = true;

	var maskedShapeInstanceList = [this.instance_13];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(3).to({_off:false},0).to({regX:8.2,regY:6.6,scaleX:0.82,scaleY:0.82,rotation:224.9,x:40.6,y:-18.9},4).to({_off:true},1).wait(16));

	// Symbol 28
	this.instance_14 = new lib.Symbol28();
	this.instance_14.parent = this;
	this.instance_14.setTransform(47.3,13.2,0.576,0.576,-106.9,0,0,7.8,7.2);
	this.instance_14._off = true;

	var maskedShapeInstanceList = [this.instance_14];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(4).to({_off:false},0).to({scaleX:0.82,scaleY:0.82,rotation:-241.9,x:48.4,y:-18.9},4).to({_off:true},1).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,60,32);


(lib.hat04 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_16 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(16).call(this.frame_16).wait(17));

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3gzhJANIhCASQkXhAiuApIgBAAQhSACg1AnQhxBiiRA6g");
	var mask_graphics_1 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSACg1AnQhxBiiRA6g");
	var mask_graphics_2 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_3 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXhAiuApIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_4 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXhAiuApIgBAAQhSACg1AnQhxBiiRA6g");
	var mask_graphics_5 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_6 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3gzhJANIhCASQkXhAiuApIgBAAQhSACg1AnQhxBiiRA6g");
	var mask_graphics_7 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3gzhJANIhCASQkXhAiuApIgBAAQhSACg1AnQhxBiiRA6g");
	var mask_graphics_8 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_9 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_10 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_11 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_12 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_13 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_14 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_15 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_16 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_17 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_18 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_19 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_20 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_21 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_22 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_23 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_24 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_25 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBiiRA6g");
	var mask_graphics_26 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXg/iuAoIgBAAQhSADg1AmQhxBiiRA6g");
	var mask_graphics_27 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXhAiuApIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_28 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3gzhJANIhCASQkXhAiuApIgBAAQhSACg1AnQhxBiiRA6g");
	var mask_graphics_29 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXhAiuApIgBAAQhSACg1AnQhxBiiRA6g");
	var mask_graphics_30 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3gzhJANIhCASQkXhAiuApIgBAAQhSACg1AnQhxBiiRA6g");
	var mask_graphics_31 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3g0hJAOIhCASQkXhAiuApIgBAAQhSADg1AmQhxBhiRA7g");
	var mask_graphics_32 = new cjs.Graphics().p("AoIlwIQRAAIAAJHQg3gzhJANIhCASQkXhAiuApIgBAAQhSACg1AnQhxBiiRA6g");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:29.7,y:10.7}).wait(1).to({graphics:mask_graphics_1,x:29.7,y:10.9}).wait(1).to({graphics:mask_graphics_2,x:29.7,y:11.2}).wait(1).to({graphics:mask_graphics_3,x:29.7,y:11.7}).wait(1).to({graphics:mask_graphics_4,x:29.7,y:12.4}).wait(1).to({graphics:mask_graphics_5,x:29.7,y:13.4}).wait(1).to({graphics:mask_graphics_6,x:29.7,y:14.7}).wait(1).to({graphics:mask_graphics_7,x:29.7,y:16.5}).wait(1).to({graphics:mask_graphics_8,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_9,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_10,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_11,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_12,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_13,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_14,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_15,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_16,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_17,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_18,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_19,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_20,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_21,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_22,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_23,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_24,x:29.7,y:18.7}).wait(1).to({graphics:mask_graphics_25,x:29.7,y:15.6}).wait(1).to({graphics:mask_graphics_26,x:29.7,y:13.8}).wait(1).to({graphics:mask_graphics_27,x:29.7,y:12.6}).wait(1).to({graphics:mask_graphics_28,x:29.7,y:11.8}).wait(1).to({graphics:mask_graphics_29,x:29.7,y:11.3}).wait(1).to({graphics:mask_graphics_30,x:29.7,y:11}).wait(1).to({graphics:mask_graphics_31,x:29.7,y:10.8}).wait(1).to({graphics:mask_graphics_32,x:29.7,y:10.7}).wait(1));

	// Symbol 22
	this.instance = new lib.Symbol22();
	this.instance.parent = this;
	this.instance.setTransform(38.3,69.5,1,1,0,0,0,8.3,10.6);

	var maskedShapeInstanceList = [this.instance];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regY:6.6,x:37.8,y:64.5},0).wait(1).to({x:37.1,y:63.1},0).wait(1).to({x:36.1,y:60.9},0).wait(1).to({x:34.7,y:57.7},0).wait(1).to({x:32.6,y:53.3},0).wait(1).to({x:29.9,y:47.3},0).wait(1).to({x:26.3,y:39.5},0).wait(1).to({regY:10.6,x:21.7,y:33.5},0).wait(1).to({regY:6.6,x:16.9,y:28},0).wait(1).to({x:13.9,y:27.1},0).wait(1).to({x:11.9,y:26.5},0).wait(1).to({x:10.6,y:26.1},0).wait(1).to({x:9.7,y:25.8},0).wait(1).to({x:9.2,y:25.6},0).wait(1).to({x:8.9,y:25.5},0).wait(1).to({regY:10.6,x:8.7,y:29.5},0).wait(1).to({regY:6.6,x:9.1,y:25.6},0).wait(1).to({x:9.5,y:25.7},0).wait(1).to({x:10.1,y:25.9},0).wait(1).to({x:10.8,y:26.1},0).wait(1).to({x:11.9,y:26.5},0).wait(1).to({x:13.7,y:27},0).wait(1).to({x:16.6,y:27.9},0).wait(1).to({regY:10.6,x:21.7,y:33.5},0).wait(1).to({regY:6.6,x:28,y:43.2},0).wait(1).to({x:31.8,y:51.5},0).wait(1).to({x:34.3,y:56.8},0).wait(1).to({x:35.9,y:60.3},0).wait(1).to({x:36.9,y:62.6},0).wait(1).to({x:37.6,y:64},0).wait(1).to({x:38,y:64.9},0).wait(1).to({regY:10.6,x:38.3,y:69.5},0).wait(1));

	// Symbol 21
	this.instance_1 = new lib.Symbol21();
	this.instance_1.parent = this;
	this.instance_1.setTransform(42.4,66.1,1,1,-22.2,0,0,8.2,12.8);

	var maskedShapeInstanceList = [this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regY:7.4,x:40.8,y:60.2},0).wait(1).to({x:41.4,y:58.9},0).wait(1).to({x:42.4,y:56.8},0).wait(1).to({x:43.8,y:53.8},0).wait(1).to({x:45.7,y:49.7},0).wait(1).to({x:48.3,y:44.1},0).wait(1).to({x:51.8,y:36.8},0).wait(1).to({regY:12.8,x:58.3,y:32.3},0).wait(1).to({regY:7.4,rotation:-14.1,x:58.2,y:25.6},0).wait(1).to({rotation:-9,x:59.5,y:24.5},0).wait(1).to({rotation:-5.6,x:60.3,y:23.9},0).wait(1).to({rotation:-3.3,x:60.9,y:23.5},0).wait(1).to({rotation:-1.9,x:61.2,y:23.2},0).wait(1).to({rotation:-0.9,x:61.4,y:23},0).wait(1).to({rotation:-0.4,x:61.6,y:22.9},0).wait(1).to({regY:12.8,rotation:0,x:61.7,y:28.3},0).wait(1).to({regY:7.4,rotation:-0.7,x:61.5,y:23},0).wait(1).to({rotation:-1.5,x:61.3,y:23.2},0).wait(1).to({rotation:-2.5,x:61.1,y:23.4},0).wait(1).to({rotation:-3.7,x:60.8,y:23.6},0).wait(1).to({rotation:-5.6,x:60.3,y:23.9},0).wait(1).to({rotation:-8.6,x:59.6,y:24.5},0).wait(1).to({rotation:-13.6,x:58.4,y:25.5},0).wait(1).to({regY:12.8,rotation:-22.2,x:58.3,y:32.3},0).wait(1).to({regY:7.4,x:50.2,y:40.2},0).wait(1).to({x:46.5,y:48},0).wait(1).to({x:44.1,y:53},0).wait(1).to({x:42.6,y:56.3},0).wait(1).to({x:41.6,y:58.4},0).wait(1).to({x:41,y:59.8},0).wait(1).to({x:40.6,y:60.6},0).wait(1).to({regY:12.8,x:42.4,y:66.1},0).wait(1));

	// Symbol 20
	this.instance_2 = new lib.Symbol20();
	this.instance_2.parent = this;
	this.instance_2.setTransform(40.4,70.6,1,1,-15,0,0,11,9.2);

	var maskedShapeInstanceList = [this.instance_2];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regY:5.6,x:39.7,y:66.2},0).wait(1).to({x:40.1,y:64.7},0).wait(1).to({x:40.7,y:62.5},0).wait(1).to({x:41.5,y:59.3},0).wait(1).to({x:42.7,y:54.9},0).wait(1).to({x:44.3,y:49},0).wait(1).to({x:46.4,y:41.2},0).wait(1).to({regY:9.2,x:50.1,y:34.6},0).wait(1).to({regY:5.6,rotation:-9.5,x:50.2,y:29.3},0).wait(1).to({rotation:-6.1,x:50.9,y:28.2},0).wait(1).to({rotation:-3.8,x:51.4,y:27.5},0).wait(1).to({rotation:-2.3,x:51.7,y:27},0).wait(1).to({rotation:-1.3,x:51.9,y:26.7},0).wait(1).to({rotation:-0.6,x:52,y:26.5},0).wait(1).to({rotation:-0.2,x:52.1,y:26.4},0).wait(1).to({regY:9.2,rotation:0,x:52.2,y:30},0).wait(1).to({regY:5.6,rotation:-0.5,x:52,y:26.5},0).wait(1).to({rotation:-1,x:51.9,y:26.7},0).wait(1).to({rotation:-1.7,x:51.8,y:26.9},0).wait(1).to({rotation:-2.5,x:51.6,y:27.1},0).wait(1).to({rotation:-3.8,x:51.4,y:27.6},0).wait(1).to({rotation:-5.8,x:51,y:28.2},0).wait(1).to({rotation:-9.2,x:50.3,y:29.3},0).wait(1).to({regY:9.2,rotation:-15,x:50.1,y:34.6},0).wait(1).to({regY:5.6,x:45.4,y:44.9},0).wait(1).to({x:43.2,y:53.2},0).wait(1).to({x:41.8,y:58.5},0).wait(1).to({x:40.8,y:62},0).wait(1).to({x:40.2,y:64.2},0).wait(1).to({x:39.8,y:65.7},0).wait(1).to({x:39.6,y:66.6},0).wait(1).to({regY:9.2,x:40.4,y:70.6},0).wait(1));

	// Symbol 19
	this.instance_3 = new lib.Symbol19();
	this.instance_3.parent = this;
	this.instance_3.setTransform(39.7,61.1,1,1,15,0,0,8.8,16.8);

	var maskedShapeInstanceList = [this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({regX:10.7,regY:8.4,x:43.9,y:52.4},0).wait(1).to({x:44.2,y:51},0).wait(1).to({x:44.6,y:48.8},0).wait(1).to({x:45.3,y:45.6},0).wait(1).to({x:46.1,y:41.2},0).wait(1).to({x:47.3,y:35.2},0).wait(1).to({x:48.8,y:27.4},0).wait(1).to({regX:8.8,regY:16.8,x:46.7,y:25.1},0).wait(1).to({regX:10.7,regY:8.4,rotation:9.5,x:50.7,y:15.6},0).wait(1).to({rotation:6.1,y:14.5},0).wait(1).to({rotation:3.8,y:13.7},0).wait(1).to({rotation:2.3,y:13.2},0).wait(1).to({rotation:1.3,y:13},0).wait(1).to({rotation:0.6,x:50.6,y:12.7},0).wait(1).to({rotation:0.2,y:12.6},0).wait(1).to({regX:8.7,regY:16.8,rotation:0,x:48.7,y:21},0).wait(1).to({regX:10.7,regY:8.4,rotation:0.5,x:50.7,y:12.7},0).wait(1).to({rotation:1,y:12.9},0).wait(1).to({rotation:1.7,y:13.1},0).wait(1).to({rotation:2.5,x:50.8,y:13.3},0).wait(1).to({rotation:3.8,y:13.7},0).wait(1).to({rotation:5.8,x:50.7,y:14.4},0).wait(1).to({rotation:9.2,y:15.4},0).wait(1).to({regX:8.8,regY:16.8,rotation:15,x:46.7,y:25.1},0).wait(1).to({regX:10.7,regY:8.4,x:48.1,y:31.1},0).wait(1).to({x:46.5,y:39.4},0).wait(1).to({x:45.4,y:44.7},0).wait(1).to({x:44.7,y:48.2},0).wait(1).to({x:44.3,y:50.5},0).wait(1).to({x:44,y:51.9},0).wait(1).to({x:43.8,y:52.8},0).wait(1).to({regX:8.8,regY:16.8,x:39.7,y:61.1},0).wait(1));

	// Symbol 18
	this.instance_4 = new lib.Symbol18();
	this.instance_4.parent = this;
	this.instance_4.setTransform(38,65.3,1,1,60,0,0,33.6,24.2);

	var maskedShapeInstanceList = [this.instance_4];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({regX:23.2,regY:15.5,rotation:59.2,x:40.1,y:51},0).wait(1).to({rotation:58,x:39.9,y:49.4},0).wait(1).to({rotation:56.2,x:39.4,y:47.2},0).wait(1).to({rotation:53.5,x:38.8,y:44},0).wait(1).to({rotation:49.9,x:37.9,y:39.6},0).wait(1).to({rotation:44.9,x:36.8,y:33.7},0).wait(1).to({rotation:38.4,x:35.2,y:26.1},0).wait(1).to({regX:33.6,regY:24.2,rotation:30,x:38,y:29.3},0).wait(1).to({regX:23.2,regY:15.5,rotation:19,x:31,y:16.3},0).wait(1).to({rotation:12.1,x:29.7},0).wait(1).to({rotation:7.5,x:28.8,y:16.4},0).wait(1).to({rotation:4.5,x:28.4},0).wait(1).to({rotation:2.5,x:28,y:16.5},0).wait(1).to({rotation:1.3,x:27.8},0).wait(1).to({rotation:0.5,x:27.7,y:16.6},0).wait(1).to({regX:33.6,regY:24.2,rotation:0,x:38,y:25.3},0).wait(1).to({regX:23.2,regY:15.5,rotation:1,x:27.8,y:16.5},0).wait(1).to({rotation:2.1,x:28},0).wait(1).to({rotation:3.3,x:28.1,y:16.4},0).wait(1).to({rotation:5,x:28.4},0).wait(1).to({rotation:7.5,x:28.8,y:16.3},0).wait(1).to({rotation:11.6,x:29.6},0).wait(1).to({rotation:18.3,x:30.9,y:16.2},0).wait(1).to({regX:33.6,regY:24.2,rotation:30,x:38,y:29.3},0).wait(1).to({regX:23.2,regY:15.5,rotation:41.5,x:36,y:29.7},0).wait(1).to({rotation:48.4,x:37.6,y:37.8},0).wait(1).to({rotation:52.8,x:38.6,y:43.2},0).wait(1).to({rotation:55.7,x:39.3,y:46.7},0).wait(1).to({rotation:57.6,x:39.7,y:49},0).wait(1).to({rotation:58.8,x:40.1,y:50.5},0).wait(1).to({rotation:59.6,x:40.2,y:51.4},0).wait(1).to({regX:33.6,regY:24.2,rotation:60,x:38,y:65.3},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,78,47.6);


(lib.Candy03 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Symbol 17
	this.instance = new lib.Symbol17();
	this.instance.parent = this;
	this.instance.setTransform(74.6,10,0.26,0.26,0,0,0,2.5,2.7);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({_off:false},0).to({regY:2.6,scaleX:1.25,scaleY:1.25},5).to({scaleX:1,scaleY:1},2).wait(8).to({scaleX:1.25,scaleY:1.25},2).to({regY:2.7,scaleX:0.26,scaleY:0.26},5).to({_off:true},1).wait(8));

	// Symbol 16
	this.instance_1 = new lib.Symbol16();
	this.instance_1.parent = this;
	this.instance_1.setTransform(65.1,14,0.176,0.176,0,0,0,3.7,3.7);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(3).to({_off:false},0).to({scaleX:1.22,scaleY:1.22,x:65.2,y:14.1},5).to({regX:3.6,regY:3.6,scaleX:1,scaleY:1,x:65,y:13.9},2).wait(8).to({regX:3.7,regY:3.7,scaleX:1.22,scaleY:1.22,x:65.2,y:14.1},2).to({scaleX:0.18,scaleY:0.18,x:65.1,y:14},5).to({_off:true},1).wait(6));

	// Symbol 15
	this.instance_2 = new lib.Symbol15();
	this.instance_2.parent = this;
	this.instance_2.setTransform(58.5,5.8,0.129,0.129,0,0,0,5,4.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5).to({_off:false},0).to({regX:4.9,scaleX:1.17,scaleY:1.17},5).to({scaleX:1,scaleY:1},2).wait(8).to({scaleX:1.17,scaleY:1.17},2).to({regX:5,scaleX:0.13,scaleY:0.13},5).to({_off:true},1).wait(4));

	// Symbol 14
	this.instance_3 = new lib.Symbol14();
	this.instance_3.parent = this;
	this.instance_3.setTransform(40.8,13.1,0.119,0.119,0,0,0,4.2,4.2);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(3).to({_off:false},0).to({regX:4.4,scaleX:1.18,scaleY:1.18,x:41},5).to({regX:4.3,scaleX:1,scaleY:1,x:40.8},2).wait(8).to({regX:4.4,scaleX:1.18,scaleY:1.18,x:41},2).to({regX:4.2,scaleX:0.12,scaleY:0.12,x:40.8},5).to({_off:true},1).wait(6));

	// Symbol 13
	this.instance_4 = new lib.Symbol13();
	this.instance_4.parent = this;
	this.instance_4.setTransform(41.1,4,0.152,0.152,0,0,0,4.6,4);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(8).to({_off:false},0).to({regX:4.7,scaleX:1.2,scaleY:1.2,x:41.3,y:4.1},5).to({regX:4.6,scaleX:1,scaleY:1,x:41.1,y:4},2).wait(8).to({regX:4.7,scaleX:1.2,scaleY:1.2,x:41.3,y:4.1},2).to({regX:4.6,scaleX:0.15,scaleY:0.15,x:41.1,y:4},5).to({_off:true},1).wait(1));

	// Symbol 12
	this.instance_5 = new lib.Symbol12();
	this.instance_5.parent = this;
	this.instance_5.setTransform(27.6,4.8,0.163,0.163,0,0,0,4.3,4.6);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1).to({_off:false},0).to({regY:4.7,scaleX:1.17,scaleY:1.17,y:4.7},5).to({regY:4.8,scaleX:1,scaleY:1,y:4.8},2).wait(8).to({regY:4.7,scaleX:1.17,scaleY:1.17,y:4.7},2).to({regY:4.6,scaleX:0.16,scaleY:0.16,y:4.8},5).to({_off:true},1).wait(8));

	// Symbol 11
	this.instance_6 = new lib.Symbol11();
	this.instance_6.parent = this;
	this.instance_6.setTransform(20.7,12.5,0.08,0.08,0,0,0,3.8,4.4);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(3).to({_off:false},0).to({regX:4,scaleX:1.17,scaleY:1.17,y:12.6},5).to({scaleX:1,scaleY:1,y:12.5},2).wait(8).to({scaleX:1.17,scaleY:1.17,y:12.6},2).to({regX:3.8,scaleX:0.08,scaleY:0.08,y:12.5},5).to({_off:true},1).wait(6));

	// Symbol 10
	this.instance_7 = new lib.Symbol10();
	this.instance_7.parent = this;
	this.instance_7.setTransform(14.1,8.6,0.158,0.158,0,0,0,4.1,2.9);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(5).to({_off:false},0).to({regX:4.2,scaleX:1.18,scaleY:1.18,x:14.2},5).to({regX:4.1,scaleX:1,scaleY:1,x:14},2).wait(8).to({regX:4.2,scaleX:1.18,scaleY:1.18,x:14.2},2).to({regX:4.1,scaleX:0.16,scaleY:0.16,x:14.1},5).to({_off:true},1).wait(4));

	// Symbol 9
	this.instance_8 = new lib.Symbol9();
	this.instance_8.parent = this;
	this.instance_8.setTransform(2.8,15.7,0.156,0.156,0,0,0,2.9,4.5);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1).to({_off:false},0).to({regX:2.7,scaleX:1.19,scaleY:1.19,x:2.7},5).to({regX:2.8,scaleX:1,scaleY:1,x:2.8},2).wait(8).to({regX:2.7,scaleX:1.19,scaleY:1.19,x:2.7},2).to({regX:2.9,scaleX:0.16,scaleY:0.16,x:2.8},5).to({_off:true},1).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,77,18);


(lib.brow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 14
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("ACmAlQhngriDAeIAAAAQhKAMgkhIQAxAkAzgOQCfgfBZBDQASAPgVAAIgBAAg");
	this.shape.setTransform(6.2,8.2);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(35).to({_off:false},0).to({_off:true},1).wait(4));

	// Layer 13
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#3C070E").s().p("AgSgUQAUgCAVAAQATAGgBAMQABAMgSAFQgeAEgeAHQgPgaAhgSg");
	this.shape_1.setTransform(1.9,8.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#3C070E").s().p("AgHgUQAUgCAVAAQATAGgBAMQABAMgSAFQgyAKgWABQgngZBFgTg");
	this.shape_2.setTransform(0.8,8.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#3C070E").s().p("AgnAWQg+gXBngTQAVgCAVgBQATAHAAALQABAMgTAFQgyAMgWAAQgIAAgEgCg");
	this.shape_3.setTransform(-0.2,8.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#3C070E").s().p("AADgUQAVgCAVAAQATAGAAAMQABAMgTAFQgfAEgcAHQhrgEB7gog");
	this.shape_4.setTransform(-0.3,8.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#3C070E").s().p("AADgUQAVgCAVAAQATAGgBAMQABAMgSAFQgyAKgWABQhVgPBygdg");
	this.shape_5.setTransform(-0.3,8.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_1}]},31).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_3}]},1).to({state:[]},1).to({state:[{t:this.shape_1}]},1).wait(1));

	// Layer 12
	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#3C070E").s().p("AgNgSQAcgJAdAQIAHATIg/AKQgXAAgOAEQAHggAdgIg");
	this.shape_6.setTransform(3.4,8.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#3C070E").s().p("Ag5ALQALgUAigHQAfgJAeANIAJAVIhCALIgHABQgcAAgOgKg");
	this.shape_7.setTransform(2,8.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#3C070E").s().p("AhBgBQAPgIAngGQAjgKAeAKIAMAXIhFANIgIAAQglAAgRgWg");
	this.shape_8.setTransform(0.6,8.3);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#3C070E").s().p("AhYgeQAlAdAwgEQArgMAeAEIATAcIhNAQIgJAAQg6AAghg9g");
	this.shape_9.setTransform(-2.8,7.6);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#3C070E").s().p("AhMgVQAZALAsgFQAmgLAfAHIAQAZIhKAPIgSABQgyAAgMgrg");
	this.shape_10.setTransform(-1.1,8.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape_6}]},31).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_8}]},1).to({state:[]},1).to({state:[{t:this.shape_6}]},1).wait(1));

	// Layer 8
	this.instance = new lib.Symbol4();
	this.instance.parent = this;
	this.instance.setTransform(5.4,6,1.014,1,0,-179.4,0.6,7.2,1.8);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(5).to({_off:false},0).to({_off:true},2).wait(2).to({_off:false,regX:7.1,regY:1.7,skewX:-147.8,skewY:32.2,x:6,y:3.5},0).wait(1).to({_off:true},1).wait(1).to({_off:false,scaleX:1.01,scaleY:1,skewX:-163.7,skewY:16.3,x:5.8,y:4.7},0).to({regX:7.2,regY:1.8,scaleX:1.01,scaleY:1,skewX:-179.4,skewY:0.6,x:5.4,y:6},2).to({_off:true},1).wait(25));

	// Layer 7
	this.instance_1 = new lib.Symbol3();
	this.instance_1.parent = this;
	this.instance_1.setTransform(11.7,7,1,1,0,-171.7,8.3,9.2,1.9);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(5).to({_off:false},0).to({_off:true},2).wait(2).to({_off:false,skewX:-155.1,skewY:24.9,x:12.1,y:6.1},0).wait(1).to({_off:true},1).wait(1).to({_off:false,regX:9.3,scaleX:1,scaleY:1,skewX:-163.5,skewY:16.5,y:6.6},0).to({regX:9.2,scaleX:1,scaleY:1,skewX:-171.7,skewY:8.3,x:11.7,y:7},2).to({_off:true},1).wait(25));

	// Layer 6
	this.instance_2 = new lib.Symbol2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(17.4,8,1,1,0,-167,13,8.1,2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5).to({_off:false},0).to({_off:true},2).wait(2).to({_off:false,regX:8.2,skewX:-156.1,skewY:23.9,x:17.5,y:8.2},0).wait(1).to({_off:true},1).wait(1).to({_off:false,scaleX:1,scaleY:1,skewX:-161.7,skewY:18.3},0).to({regX:8.1,scaleX:1,scaleY:1,skewX:-167,skewY:13,x:17.4,y:8},2).to({_off:true},1).wait(25));

	// Layer 5
	this.instance_3 = new lib.Symbol1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(23.4,9,1,1,0,-162.1,17.9,8.1,2.2);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off:false},0).to({_off:true},2).wait(2).to({_off:false},0).wait(1).to({_off:true},1).wait(1).to({_off:false,regX:8.2,scaleX:1,scaleY:1,skewX:-162.2,skewY:17.8,x:23.5,y:9.1},0).to({regX:8.1,scaleX:1,scaleY:1,skewX:-162.1,skewY:17.9,x:23.4,y:9},2).to({_off:true},1).wait(25));

	// Layer 3
	this.instance_4 = new lib.Symbol4();
	this.instance_4.parent = this;
	this.instance_4.setTransform(5,7.9,1,1,0,0,0,7.2,1.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({scaleX:1.01,rotation:17.4,x:5.2,y:6.5},4).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},3).wait(1).to({_off:false,scaleX:1,rotation:0,x:5,y:7.9},0).wait(1).to({regX:6.9,rotation:-31.6,x:5.1,y:10.4},5).to({_off:true},1).wait(5).to({_off:false,regX:7.2,rotation:0,x:5,y:7.9},0).wait(2).to({_off:true},1).wait(1).to({_off:false},0).to({_off:true},3).wait(1).to({_off:false},0).wait(1));

	// Layer 1
	this.instance_5 = new lib.Symbol3();
	this.instance_5.parent = this;
	this.instance_5.setTransform(11.5,7.9,1,1,0,0,0,9.1,1.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({rotation:9.7,x:11.6,y:7.5},4).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},3).wait(1).to({_off:false,rotation:0,x:11.5,y:7.9},0).wait(1).to({regX:9.2,rotation:-16.9,y:8.4},5).to({_off:true},1).wait(5).to({_off:false,regX:9.1,rotation:0,y:7.9},0).to({_off:true},3).wait(1).to({_off:false},0).to({_off:true},3).wait(1).to({_off:false},0).wait(1));

	// Layer 2
	this.instance_6 = new lib.Symbol2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(17.2,8.2,1,1,0,0,0,7.9,2.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({rotation:4.9,x:17.1,y:8.3},4).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},3).wait(1).to({_off:false,rotation:0,x:17.2,y:8.2},0).wait(1).to({rotation:-7.2},5).to({_off:true},1).wait(5).to({_off:false,rotation:0},0).to({_off:true},3).wait(1).to({_off:false,rotation:5.4,x:17.1,y:8.7},0).to({_off:true},3).wait(1).to({_off:false,rotation:0,x:17.2,y:8.2},0).wait(1));

	// Layer 4
	this.instance_7 = new lib.Symbol1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(23.3,9.1,1,1,0,0,0,8,2.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(4).to({_off:true},1).wait(10).to({_off:false},0).to({_off:true},3).wait(1).to({_off:false},0).wait(6).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},3).wait(1).to({_off:false,regX:8.1,rotation:14.4,x:23.1,y:11.2},0).to({_off:true},3).wait(1).to({_off:false,regX:8,rotation:0,x:23.3,y:9.1},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-2.2,5.9,26.7,4.8);


(lib.All = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// red_R
	this.instance = new lib.red();
	this.instance.parent = this;
	this.instance.setTransform(-159,-107,1,1,0,0,0,16,11);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({regY:11.1,rotation:-9.5,x:-159.1,y:-115},6).to({regX:16.1,regY:11.2,scaleX:1,scaleY:1,rotation:-12.8,x:-152.4,y:-115.5},4).to({regX:16,regY:11.1,scaleX:1,scaleY:1,rotation:-9.5,x:-159.1,y:-118},3).to({regX:16.1,regY:11.2,scaleX:1,scaleY:1,rotation:-12.8,x:-152.4,y:-118.8},3).to({regX:16,regY:11.1,scaleX:1,scaleY:1,rotation:-9.5,x:-159.1,y:-115},6).wait(1).to({regY:11,rotation:0,x:-159,y:-107},9).wait(1).to({y:-99},3).to({x:-161.4,y:-109.4},3).to({x:-159,y:-107},2).wait(9).to({regX:16.1,rotation:-11.5,x:-153.3,y:-100.6},4,cjs.Ease.get(0.46)).to({rotation:11.4,x:-161.1,y:-113.4},9).to({regX:16,rotation:0,x:-159,y:-107},6,cjs.Ease.get(-0.39)).wait(12).to({y:-99},3).to({y:-107},3).wait(9).to({x:-163},5).wait(14).to({x:-159},4).wait(15).to({alpha:0.41},9).wait(14).to({alpha:1},15).to({alpha:0.398},1).to({alpha:0},6).wait(29).to({alpha:0.398},8).wait(1).to({alpha:0},4).to({_off:true},1).wait(16).to({_off:false},0).wait(1).to({alpha:0.398},5).wait(1));

	// red_L
	this.instance_1 = new lib.red();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-99,-107,1,1,0,0,0,16,11);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({regY:11.1,rotation:-18,x:-94.7,y:-131.7},6).to({scaleX:1,scaleY:1,rotation:-21.5,x:-89.2,y:-136.1},4).to({scaleX:1,scaleY:1,rotation:-18,x:-94.7,y:-134.7},3).to({scaleX:1,scaleY:1,rotation:-21.5,x:-89.2,y:-139.3},3).to({scaleX:1,scaleY:1,rotation:-18,x:-94.7,y:-131.7},6).wait(1).to({regY:11,rotation:0,x:-99,y:-107},9).wait(1).to({y:-99},3).to({x:-96.6,y:-109.4},3).to({x:-99,y:-107},2).wait(9).to({regY:11.1,rotation:-11.5,x:-94.6,y:-112.4},4,cjs.Ease.get(0.46)).to({regX:16.1,regY:11,rotation:11.4,x:-102.3,y:-101.5},9).to({regX:16,rotation:0,x:-99,y:-107},6,cjs.Ease.get(-0.39)).wait(12).to({y:-99},3).to({y:-107},3).wait(9).to({x:-103},5).wait(14).to({x:-99},4).wait(15).to({alpha:0.41},9).wait(14).to({alpha:1},15).to({alpha:0.398},1).to({alpha:0},6).wait(29).to({alpha:0.398},8).wait(1).to({alpha:0},4).to({_off:true},1).wait(16).to({_off:false},0).wait(1).to({alpha:0.199},1).to({alpha:0.398},4).wait(1));

	// brow_R
	this.instance_2 = new lib.brow("single",20);
	this.instance_2.parent = this;
	this.instance_2.setTransform(-163.7,-147.5,1,1,0,-36.8,143.2,10.6,9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({startPosition:20},0).to({regY:9.1,scaleX:1,scaleY:1,x:-161,y:-144.9},6).to({scaleX:1,scaleY:1,skewX:-40.3,skewY:139.7,x:-156.3,y:-145.3},4).to({scaleX:1,scaleY:1,skewX:-36.8,skewY:143.2,x:-161,y:-147.9},3).to({scaleX:1,scaleY:1,skewX:-40.3,skewY:139.7,x:-156.3,y:-148.5},3).to({scaleX:1,scaleY:1,skewX:-36.8,skewY:143.2,x:-161,y:-144.9},6).to({startPosition:20},1).to({regY:9,scaleX:1,scaleY:1,x:-163.7,y:-147.5},9).to({startPosition:20},1).to({regY:9.1,skewX:-21.8,skewY:158.2,x:-159.6,y:-131.4,mode:"synched",startPosition:23,loop:false},3).to({regY:9.2,scaleX:0.86,scaleY:0.86,skewX:-44.5,skewY:135.5,x:-164.9,y:-153.6,mode:"single",startPosition:25},3).to({regY:9.1,skewX:-36.8,skewY:143.2,x:-163.8,y:-150.9},2).to({startPosition:25},4).to({regY:9.2,scaleX:0.86,scaleY:0.86,x:-163.7,y:-150.8},5).to({scaleX:0.86,scaleY:0.86,skewX:-48.3,skewY:131.7,x:-166.7,y:-138.5},4,cjs.Ease.get(0.46)).to({scaleX:0.86,scaleY:0.86,skewX:-35.6,skewY:144.4,x:-161.4,y:-146.6},5).to({skewX:-30.8,skewY:149.2,x:-158.7,y:-151.8},3).to({regY:9.1,skewX:-26.6,skewY:153.4,x:-158.3,y:-154.2},1).to({regX:10.5,regY:9.2,scaleX:0.86,scaleY:0.86,skewX:-32.1,skewY:147.9,x:-160.9,y:-150.1},4).to({regX:10.6,scaleX:0.86,scaleY:0.86,skewX:-36.8,skewY:143.2,x:-163.7,y:-150.8},2).to({startPosition:25},1,cjs.Ease.get(0.46)).to({scaleX:0.86,scaleY:0.86},10,cjs.Ease.get(0.46)).to({regY:9,scaleX:1,scaleY:1,y:-147.5,startPosition:0},1).to({regY:9.1,skewX:-21.8,skewY:158.2,x:-159.6,y:-131.4,mode:"synched",startPosition:15,loop:false},3).to({scaleX:0.86,scaleY:0.86,skewX:-36.8,skewY:143.2,x:-163.5,y:-144.2,mode:"single",startPosition:0},3).to({regY:9.2,scaleX:0.86,scaleY:0.86,x:-163.4,mode:"synched",startPosition:2,loop:false},9).to({scaleX:0.86,scaleY:0.86,skewX:-29.3,skewY:150.7,x:-165.7,y:-142.9,mode:"single",startPosition:9},5).wait(14).to({mode:"synched",startPosition:12,loop:false},0).to({scaleX:0.86,scaleY:0.86,skewX:-36.8,skewY:143.2,x:-163.4,y:-144.2,mode:"single",startPosition:2},4).to({startPosition:2},1).to({startPosition:17},9).to({regY:9,scaleX:1,scaleY:1,x:-163.7,y:-147.5,startPosition:0},4).to({startPosition:0},1).to({regY:9.1,scaleX:1,scaleY:1,x:-161.5,y:-145.2},3).to({regY:9,scaleX:1,scaleY:1,x:-163.7,y:-147.5},3).to({startPosition:0},10).to({regY:9.1,skewX:-26.3,skewY:153.7,x:-162,y:-146.2},3).to({regY:9,skewX:-36.8,skewY:143.2,x:-163.7,y:-147.5},3).to({startPosition:0},1).to({startPosition:0},15).to({startPosition:0},1).to({regY:9.1,scaleX:1.12,scaleY:1.28,skewX:-11.5,skewY:168.5,x:-159.3,y:-141.4,mode:"synched",startPosition:31,loop:false},3).to({regX:10.2,regY:9.2,scaleX:1.06,scaleY:1.31,skewX:18.6,skewY:198.6,x:-158.2,y:-136.4,mode:"single",startPosition:35},3).to({startPosition:35},3).to({startPosition:35},2).to({x:-155.2},5).to({startPosition:35},4).to({x:-160.2},5).to({startPosition:35},4).to({x:-158.2},5).to({startPosition:35},1).to({regX:10.6,regY:9,scaleX:1,scaleY:1,skewX:-36.8,skewY:143.2,x:-163.7,y:-147.5,startPosition:0},8).to({startPosition:0},1).to({regX:10.2,regY:9.2,scaleX:1.06,scaleY:1.31,skewX:29.6,skewY:209.6,x:-155.2,y:-136.7,startPosition:35},4).to({skewX:29.6,x:-153.2},3).to({x:-155.2},2).to({x:-153.2},2).to({x:-155.2},2).to({x:-153.2},2).to({x:-155.2},2).to({x:-153.2},2).to({x:-155.2},2).to({startPosition:35},1).to({scaleX:1.03,scaleY:1.18,skewX:3,skewY:183,x:-158.6,y:-140.9,mode:"synched",loop:false},2).to({regX:10.6,regY:9,scaleX:1,scaleY:1,skewX:-36.8,skewY:143.2,x:-163.7,y:-147.5,mode:"single",startPosition:39},3).wait(1));

	// brow_L
	this.instance_3 = new lib.Symbol4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(-99.5,-151.7,1,1,36.8,0,0,7.2,1.9);

	this.instance_4 = new lib.Symbol3();
	this.instance_4.parent = this;
	this.instance_4.setTransform(-94.3,-147.9,1,1,36.8,0,0,9.1,1.9);

	this.instance_5 = new lib.Symbol2();
	this.instance_5.parent = this;
	this.instance_5.setTransform(-90,-144.2,1,1,36.8,0,0,7.8,2.1);

	this.instance_6 = new lib.Symbol1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(-85.6,-139.8,1,1,36.8,0,0,8,2.2);

	this.instance_7 = new lib.brow("synched",19,false);
	this.instance_7.parent = this;
	this.instance_7.setTransform(-95.7,-147.5,1,1,36.8,0,0,10.6,9);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6},{t:this.instance_5},{t:this.instance_4},{t:this.instance_3}]}).to({state:[{t:this.instance_7}]},14).to({state:[{t:this.instance_7}]},6).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},6).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},9).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},5).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},5).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},10).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},9).to({state:[{t:this.instance_7}]},5).to({state:[{t:this.instance_7}]},14).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},9).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},10).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},15).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},5).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},5).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},5).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},8).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},4).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_7}]},2).to({state:[{t:this.instance_7}]},3).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(14).to({_off:false},0).to({regX:10.7,scaleX:1,scaleY:1,rotation:21.8,x:-107.2,y:-164.2,mode:"single",startPosition:25},6).to({regX:10.8,scaleX:1,scaleY:1,rotation:18.3,x:-103.6,y:-167.8},4).to({regX:10.7,scaleX:1,scaleY:1,rotation:21.8,x:-107.2,y:-167.2},3).to({regX:10.8,scaleX:1,scaleY:1,rotation:18.3,x:-103.6,y:-171.1},3).to({regX:10.7,scaleX:1,scaleY:1,rotation:21.8,x:-107.2,y:-164.2},6).to({startPosition:25},1).to({regX:10.6,scaleX:1,scaleY:1,rotation:36.8,x:-95.7,y:-147.5,mode:"synched",startPosition:19,loop:false},9).to({startPosition:20},1).to({rotation:21.8,x:-99.7,y:-131.5,startPosition:23},3).to({regX:10.8,regY:9.2,scaleX:0.88,scaleY:0.88,rotation:45.1,x:-95.3,y:-154.2,mode:"single",startPosition:25},3).to({regY:9.1,rotation:35.3,x:-96.9,y:-151.9},2).to({startPosition:25},4).to({regY:9.2,scaleX:0.88,scaleY:0.88,x:-96.8,y:-151.8},5).to({scaleX:0.87,scaleY:0.87,rotation:23.9,x:-101.4,y:-152.8},4,cjs.Ease.get(0.46)).to({scaleX:0.87,scaleY:0.87,rotation:36.6,x:-95.9,y:-146.2},5).to({regY:9.1,rotation:41.3,x:-93.4,y:-145.9},3).to({regY:9.2,rotation:45.5,x:-93.7,y:-143.5},1).to({rotation:40.1,x:-94.6,y:-145.5},4).to({scaleX:0.88,scaleY:0.88,rotation:35.3,x:-96.8,y:-151.8},2).to({startPosition:25},1,cjs.Ease.get(0.46)).to({scaleX:0.87,scaleY:0.87,x:-96.9},10,cjs.Ease.get(0.46)).to({regX:10.6,regY:9,scaleX:1,scaleY:1,rotation:36.8,x:-95.7,y:-147.5,mode:"synched",startPosition:0},1).to({rotation:21.8,x:-99.7,y:-131.5,startPosition:3},3).to({regX:10.8,scaleX:0.88,scaleY:0.88,rotation:10.6,x:-97.9,y:-141.4,mode:"single",startPosition:10},3).to({regY:9.1,rotation:10.5,y:-141.3},9).to({rotation:25.5,x:-101.3,y:-144.1},5).wait(14).to({startPosition:10},0).to({rotation:10.5,x:-97.9,y:-141.3},4).to({startPosition:10},1).to({startPosition:15},9).to({regX:10.6,regY:9,scaleX:1,scaleY:1,rotation:36.8,x:-95.7,y:-147.5,mode:"synched",startPosition:0},4).to({mode:"single"},1).to({scaleX:1,scaleY:1,x:-97.6,y:-145.5},3).to({scaleX:1,scaleY:1,x:-95.7,y:-147.5},3).to({startPosition:0},10).to({rotation:29.3,x:-97.7,y:-146.6},3).to({rotation:36.8,x:-95.7,y:-147.5},3).to({startPosition:0},1).to({startPosition:0},15).to({startPosition:0},1).to({regX:10.7,scaleX:1.1,scaleY:1.2,rotation:11.5,x:-97.7,y:-141.4,mode:"synched",startPosition:31,loop:false},3).to({regX:10.8,regY:9.3,scaleX:1.04,scaleY:1.27,rotation:-19.4,x:-99.3,y:-137.1,mode:"single",startPosition:35},3).to({startPosition:35},3).to({startPosition:35},2).to({x:-96.3},5).to({startPosition:35},4).to({x:-101.3},5).to({startPosition:35},4).to({x:-99.3},5).to({startPosition:35},1).to({regX:10.6,regY:9,scaleX:1,scaleY:1,rotation:36.8,x:-95.7,y:-147.5,startPosition:0},8).to({startPosition:0},1).to({regX:10.8,regY:9.3,scaleX:1.04,scaleY:1.27,rotation:-26.1,x:-100.1,y:-137.1,startPosition:35},4).to({x:-98.1},3).to({x:-100.1},2).to({x:-98.1},2).to({x:-100.1},2).to({x:-98.1},2).to({x:-100.1},2).to({x:-98.1},2).to({x:-100.1},2).to({startPosition:35},1).to({regY:9.4,scaleX:1.02,scaleY:1.16,rotation:-0.8,x:-98.3,y:-141.2,mode:"synched",loop:false},2).to({regX:10.6,regY:9,scaleX:1,scaleY:1,rotation:36.8,x:-95.7,y:-147.5,mode:"single",startPosition:39},3).wait(1));

	// Mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AtYrhIOEAAIAQHBIhYAAQgDgsggggQgigigxAAQgxAAgjAiQgiAjgBAwIkdAIIAAgHQAAgxgjgjQgigigxAAQgxAAgjAiQgjAjAAAxQAAAMACALIhHABg");
	var mask_graphics_14 = new cjs.Graphics().p("AtYrhIOEAAIAQHBIhYAAQgDgsggggQgigigxAAQgxAAgjAiQgiAjgBAwIkdAIIAAgHQAAgxgjgjQgigigxAAQgxAAgjAiQgjAjAAAxQAAAMACALIhHABg");
	var mask_graphics_15 = new cjs.Graphics().p("AtdraIN3ghIAgG6IhWADQgFgrghgeQgjghgwACQgwABgiAkQggAjABAwIkYASIAAgHQgCgwgkgiQgjgggwABQgxACghAjQghAkACAwQAAAMADALIhHADg");
	var mask_graphics_16 = new cjs.Graphics().p("AthrVINpg/IAvGxIhVAHQgGgqgigdQgjgfgwADQgvAEggAkQgeAjADAvIkUAcIgBgGQgDgwgkgfQgkgfgwADQgvADggAkQgfAlAEAvQABAMACAKIhFAGg");
	var mask_graphics_17 = new cjs.Graphics().p("AtlrQINbhdIA8GpIhTAJQgIgpghgbQglgdgvAFQguAFgeAkQgdAlAFAuIkPAlIAAgHQgFguglgeQglgdguAFQgvAFgeAlQgdAkAFAvQABALADALIhDAIg");
	var mask_graphics_18 = new cjs.Graphics().p("AtorLINKh7IBLGgIhSAMQgJgogigZQglgcguAHQguAHgcAlQgbAkAHAuIkJAuIgBgHQgHgtglgcQglgcguAHQguAHgcAlQgbAlAGAuQACALADAKIhCALg");
	var mask_graphics_19 = new cjs.Graphics().p("AtrrIIM4iXIBZGYIhQAPQgKgogjgYQglgZgtAIQgtAIgaAlQgZAmAHAsIkDA3IgBgGQgIgtgmgaQglgagtAIQgtAJgaAlQgaAmAIAsQACALAEAKIhBANg");
	var mask_graphics_20 = new cjs.Graphics().p("AttrFIMliyIBmGNIhOASQgMgmgjgWQglgYgsAJQgsAKgYAmQgYAlAJAsIj9A/IgBgGQgKgsgmgYQgmgYgrAJQgsAKgZAmQgYAmAKAsQACAKAEAKIg/APg");
	var mask_graphics_21 = new cjs.Graphics().p("Atoq+IMii/IBsGMIhOATQgMgmgjgWQgmgXgsAKQgsALgXAmQgXAmAJArIj8BDIgBgGQgKgsgngXQgmgYgsAKQgrALgYAmQgXAmAKAsQACALAFAJIhAAQg");
	var mask_graphics_22 = new cjs.Graphics().p("Atkq4IMgjKIByGKIhOAUQgMgmgkgVQgmgXgsALQgrALgYAnQgWAmAKArIj7BHIgBgGQgLgsgngXQgmgXgsALQgrALgYAnQgWAnALArQACALAFAJIhAARg");
	var mask_graphics_23 = new cjs.Graphics().p("AtfqxIMdjWIB4GIIhOAVQgNgmgkgUQgngXgrAMQgrAMgXAnQgWAmALArIj6BLIgBgGQgMgrgngXQgngWgrALQgrAMgXAnQgWAnALArQADALAFAJIg/ASg");
	var mask_graphics_24 = new cjs.Graphics().p("AtaqqIMZjjIB+GHIhNAWQgOglgkgVQgngVgrAMQgrAMgXAoQgVAmAMArIj5BPIgBgGQgNgrgngWQgngWgsAMQgrANgWAnQgVAnAMArQADALAFAJIg/ATg");
	var mask_graphics_25 = new cjs.Graphics().p("Athq4IMejTIB2GJIhOAVQgNgmgjgUQgngXgrAMQgsALgXAnQgWAmALArIj6BKIgCgGQgLgsgngWQgngXgrALQgsAMgWAnQgXAnAMArQACALAFAJIg/ARg");
	var mask_graphics_26 = new cjs.Graphics().p("AtnrGIMijCIBtGLIhOATQgMgmgjgVQgmgYgsALQgrALgYAmQgXAmAKArIj8BFIgBgHQgLgrgmgYQgmgXgsAKQgsALgXAmQgXAnAKArQADALAEAJIg/ARg");
	var mask_graphics_27 = new cjs.Graphics().p("AttrUIMliyIBmGNIhOASQgMgmgjgWQglgYgsAJQgsAKgYAmQgYAlAJAsIj9A/IgBgGQgKgsgmgYQgmgYgrAJQgsAKgZAmQgYAmAKAsQACAKAEAKIg/APg");
	var mask_graphics_28 = new cjs.Graphics().p("AtnrLIMijDIBuGMIhOATQgMgmgkgWQgmgXgsAKQgrALgYAmQgXAmAKAsIj7BEIgCgGQgKgsgngXQgmgYgsALQgrALgYAmQgXAmAKAsQADALAEAJIg/AQg");
	var mask_graphics_29 = new cjs.Graphics().p("AtgrDIMdjSIB2GJIhOAUQgMglgkgVQgngXgrAMQgsALgWAnQgWAmAKArIj6BKIgBgGQgMgsgngWQgngXgrAMQgrALgXAnQgXAnAMArQADALAEAJIg/ASg");
	var mask_graphics_30 = new cjs.Graphics().p("Ataq6IMZjjIB+GGIhNAXQgOgmgkgUQgngVgrAMQgrAMgXAnQgVAnAMArIj5BPIgBgGQgNgsgngWQgngVgsAMQgrAMgWAoQgVAnAMArQADALAFAJIg/ATg");
	var mask_graphics_31 = new cjs.Graphics().p("Atdq8IMbjbIB6GIIhOAWQgNgmgkgUQgngXgrAMQgrAMgXAnQgVAnALArIj5BMIgCgGQgMgsgngWQgngWgrAMQgsAMgWAnQgWAnAMArQADALAEAJIg/ASg");
	var mask_graphics_32 = new cjs.Graphics().p("Athq+IMejSIB2GJIhOAUQgNglgjgVQgngXgrAMQgsALgXAnQgWAmALArIj6BKIgCgGQgLgsgngWQgngXgrAMQgsALgWAnQgXAnAMArQACALAFAJIg/ASg");
	var mask_graphics_33 = new cjs.Graphics().p("AtkrAIMgjKIByGKIhOAUQgNgmgjgVQgngXgrALQgsALgXAnQgWAmAKArIj7BHIgBgGQgLgsgngXQgngXgrALQgsALgXAnQgXAnALArQADALAEAJIg/ARg");
	var mask_graphics_34 = new cjs.Graphics().p("AtnrBIMijDIBtGMIhOATQgMgmgjgWQgmgXgsALQgrAKgYAmQgXAmAKAsIj8BEIgBgGQgLgsgmgXQgmgXgsAKQgsALgXAmQgXAmAKAsQADALAEAJIg/AQg");
	var mask_graphics_35 = new cjs.Graphics().p("AtqrDIMji6IBqGMIhOASQgMgmgjgWQgmgXgsAKQgrAKgYAmQgXAmAJArIj8BCIgCgGQgKgsgmgYQgmgXgsAKQgrAKgZAmQgXAmAKAsQADALAEAJIhAAQg");
	var mask_graphics_36 = new cjs.Graphics().p("AttrFIMliyIBmGNIhOASQgMgmgjgWQglgYgsAJQgsAKgYAmQgYAlAJAsIj9A/IgBgGQgKgsgmgYQgmgYgrAJQgsAKgZAmQgYAmAKAsQACAKAEAKIg/APg");
	var mask_graphics_37 = new cjs.Graphics().p("AttrFIMliyIBmGNIhOASQgMgmgjgWQglgYgsAJQgsAKgYAmQgYAlAJAsIj9A/IgBgGQgKgsgmgYQgmgYgrAJQgsAKgZAmQgYAmAKAsQACAKAEAKIg/APg");
	var mask_graphics_38 = new cjs.Graphics().p("AtsrHIMyigIBdGUIhPAQQgLgngigYQgmgZgsAJQgtAJgZAlQgZAmAIAsIkBA6IgBgHQgJgsgmgaQglgZgtAJQgsAIgaAmQgZAmAJAsQACALAEAKIhBANg");
	var mask_graphics_39 = new cjs.Graphics().p("AtqrJIM+iOIBUGaIhRAOQgJgngjgZQglgagtAIQgtAHgbAmQgaAlAHAtIkFA0IgBgHQgHgtgmgbQglgaguAIQgtAIgaAlQgbAlAIAuQACALAEAJIhCANg");
	var mask_graphics_40 = new cjs.Graphics().p("AtorMINKh7IBLGhIhSAMQgJgogigaQglgbguAGQguAHgcAlQgbAlAGAtIkIAvIgBgHQgHgugmgcQgkgbguAHQguAGgcAlQgbAmAGAtQACAMADAKIhCAKg");
	var mask_graphics_41 = new cjs.Graphics().p("AtmrPINVhnIBCGmIhUAKQgHgogigbQglgdguAGQgvAGgdAkQgcAlAFAuIkMAoIgBgHQgGguglgdQglgdguAGQguAGgdAkQgdAlAGAvQABALADAKIhDAJg");
	var mask_graphics_42 = new cjs.Graphics().p("AtjrSINfhUIA4GsIhUAJQgHgqgigbQgkgegvAEQgvAFgeAkQgeAkAEAvIkQAiIAAgHQgFgvglgeQgkgegvAFQgvAEgeAlQgeAkAFAvQABAMADAKIhFAIg");
	var mask_graphics_43 = new cjs.Graphics().p("AthrVINphAIAvGyIhVAGQgGgqgigdQgjgfgwAEQgvADggAkQgeAkACAvIkTAcIgBgHQgDgvgkggQgkgfgwAEQgvADggAkQgfAkAEAwQABALACALIhFAGg");
	var mask_graphics_44 = new cjs.Graphics().p("AterZINygrIAlG3IhWAEQgFgqghgeQgjgggwACQgwACghAkQggAjACAwIkXAVIAAgHQgCgwgkggQgkghgwADQgwACghAkQggAkADAwQAAALADALIhGAEg");
	var mask_graphics_45 = new cjs.Graphics().p("AtbrdIN7gWIAbG8IhXACQgEgrghgfQgjghgwABQgxABghAjQghAjAAAwIkaAPIAAgHQgBgwgkgiQgjghgwABQgxABghAjQgiAjABAxQABAMACALIhHACg");
	var mask_graphics_46 = new cjs.Graphics().p("AtYrhIOEAAIAQHBIhYAAQgDgsggggQgigigxAAQgxAAgjAiQgiAjgBAwIkdAIIAAgHQAAgxgjgjQgigigxAAQgxAAgjAiQgjAjAAAxQAAAMACALIhHABg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:-85.7,y:-73.8}).wait(14).to({graphics:mask_graphics_14,x:-85.7,y:-73.8}).wait(1).to({graphics:mask_graphics_15,x:-86.2,y:-76.4}).wait(1).to({graphics:mask_graphics_16,x:-86.6,y:-78.9}).wait(1).to({graphics:mask_graphics_17,x:-87,y:-81.4}).wait(1).to({graphics:mask_graphics_18,x:-87.3,y:-83.9}).wait(1).to({graphics:mask_graphics_19,x:-87.6,y:-86.4}).wait(1).to({graphics:mask_graphics_20,x:-87.8,y:-88.8}).wait(1).to({graphics:mask_graphics_21,x:-87.3,y:-89.4}).wait(1).to({graphics:mask_graphics_22,x:-86.9,y:-89.9}).wait(1).to({graphics:mask_graphics_23,x:-86.4,y:-90.4}).wait(1).to({graphics:mask_graphics_24,x:-85.9,y:-91}).wait(1).to({graphics:mask_graphics_25,x:-86.6,y:-90.8}).wait(1).to({graphics:mask_graphics_26,x:-87.2,y:-90.5}).wait(1).to({graphics:mask_graphics_27,x:-87.8,y:-90.3}).wait(1).to({graphics:mask_graphics_28,x:-87.2,y:-91.1}).wait(1).to({graphics:mask_graphics_29,x:-86.5,y:-91.8}).wait(1).to({graphics:mask_graphics_30,x:-85.9,y:-92.6}).wait(1).to({graphics:mask_graphics_31,x:-86.2,y:-92}).wait(1).to({graphics:mask_graphics_32,x:-86.6,y:-91.3}).wait(1).to({graphics:mask_graphics_33,x:-86.9,y:-90.7}).wait(1).to({graphics:mask_graphics_34,x:-87.2,y:-90.1}).wait(1).to({graphics:mask_graphics_35,x:-87.5,y:-89.4}).wait(1).to({graphics:mask_graphics_36,x:-87.8,y:-88.8}).wait(1).to({graphics:mask_graphics_37,x:-87.8,y:-88.8}).wait(1).to({graphics:mask_graphics_38,x:-87.7,y:-87.2}).wait(1).to({graphics:mask_graphics_39,x:-87.5,y:-85.6}).wait(1).to({graphics:mask_graphics_40,x:-87.3,y:-84}).wait(1).to({graphics:mask_graphics_41,x:-87.1,y:-82.3}).wait(1).to({graphics:mask_graphics_42,x:-86.8,y:-80.7}).wait(1).to({graphics:mask_graphics_43,x:-86.6,y:-79}).wait(1).to({graphics:mask_graphics_44,x:-86.3,y:-77.3}).wait(1).to({graphics:mask_graphics_45,x:-86,y:-75.6}).wait(1).to({graphics:mask_graphics_46,x:-85.7,y:-73.8}).wait(1).to({graphics:null,x:0,y:0}).wait(212));

	// blink_L
	this.instance_8 = new lib.Symbol5();
	this.instance_8.parent = this;
	this.instance_8.setTransform(-104.9,-128.1,1,1,0,0,0,4.3,4.3);

	this.instance_9 = new lib.ey();
	this.instance_9.parent = this;
	this.instance_9.setTransform(-126.9,-125.8,1,1,0,0,0,38.9,10);
	this.instance_9._off = true;

	var maskedShapeInstanceList = [this.instance_8,this.instance_9];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(14).to({regX:4.4,regY:4.4,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-105.6,y:-133.8},6).to({regY:4.5,scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-100.3,y:-137.6},4).to({regY:4.4,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-105.6,y:-136.8},3).to({regY:4.5,scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-100.3,y:-140.8},3).to({regY:4.4,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-105.6,y:-133.8},6).wait(1).to({regX:4.3,regY:4.3,scaleX:1,scaleY:1,rotation:0,x:-104.9,y:-128.1},9).wait(1).to({regY:4.4,scaleY:0.74,y:-119.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:4.3,scaleX:1.1,scaleY:1.1,x:-104.7,y:-131.7},2).to({scaleX:1,scaleY:1,x:-104.9,y:-127.5},2).to({y:-127.7},9).to({regX:4.4,rotation:-11.5,x:-104.5,y:-131.8},4,cjs.Ease.get(0.46)).to({_off:true,regX:38.9,regY:10,rotation:0,x:-126.9,y:-125.8},1).wait(4).to({_off:false,regX:4.4,regY:4.4,rotation:2.3,x:-104.2,y:-126.3},0).to({scaleX:1,scaleY:1,rotation:6.8,x:-104.1,y:-124.5},3).to({_off:true,regX:38.8,regY:10,scaleX:1,scaleY:1,rotation:18,x:-127,y:-128.3},1).wait(4).to({_off:false,regX:4.4,regY:4.3,rotation:6.8,x:-104.4,y:-124.9},0).to({regX:4.3,rotation:0,x:-104.9,y:-127.7},2).wait(1).to({y:-128},10,cjs.Ease.get(0.46)).to({y:-128.1},1).to({regY:4.4,scaleY:0.74,y:-119.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:4.3,scaleY:1,x:-102.5,y:-131.9},2).wait(9).to({x:-111.1,y:-130.5},5).wait(14).to({x:-102.5,y:-131.9},4).wait(10).to({x:-104.9,y:-128.1},4).wait(1).to({regY:4.4,scaleY:0.74,y:-127.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:4.3,scaleY:1,y:-128.1},2).wait(10).to({scaleY:0.87,y:-128},3).to({scaleY:1,y:-128.1},3).wait(17).to({x:-105.7,y:-129.6},6).wait(5).to({x:-97.7,y:-128.6},5).wait(4).to({x:-111.7,y:-129.4},5).wait(4).to({x:-105.7,y:-129.6},5).wait(1).to({x:-104.9,y:-128.1},8).wait(1).to({x:-105.7,y:-129.6},4).to({x:-103.7},3).to({x:-105.7},2).to({x:-103.7},2).to({x:-105.7},2).to({x:-103.7},2).to({x:-105.7},2).to({x:-103.7},2).to({x:-105.7},2).wait(1).to({x:-104.9,y:-128.1},5).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(68).to({_off:false},1).to({regX:38.8,rotation:8.7,x:-127,y:-125.7},3).to({_off:true},1).wait(3).to({_off:false,rotation:18,y:-128.3},1).to({regY:9.9,rotation:10.5,x:-126.9,y:-128.4},3).to({_off:true},1).wait(178));

	// blink_R
	this.instance_10 = new lib.Symbol5();
	this.instance_10.parent = this;
	this.instance_10.setTransform(-157.1,-128.1,1,1,0,0,0,4.3,4.3);

	var maskedShapeInstanceList = [this.instance_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(14).to({regX:4.4,regY:4.4,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-151.5,y:-123.3},6).to({regY:4.5,scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-145.4,y:-124.3},4).to({regY:4.4,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-151.5,y:-126.3},3).to({regY:4.5,scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-145.4,y:-127.6},3).to({regY:4.4,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-151.5,y:-123.3},6).wait(1).to({regX:4.3,regY:4.3,scaleX:1,scaleY:1,rotation:0,x:-157.1,y:-128.1},9).wait(1).to({regY:4.4,scaleY:0.74,y:-119.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:4.3,scaleX:1.1,scaleY:1.1,x:-156.9,y:-131.7},2).to({scaleX:1,scaleY:1,x:-157.1,y:-127.5},2).to({y:-127.7},9).to({regX:4.4,rotation:-11.5,x:-155.7,y:-121.4},4,cjs.Ease.get(0.46)).to({_off:true},1).wait(4).to({_off:false,regY:4.4,rotation:2.3,x:-155.3,y:-128.5},0).to({regX:4.5,scaleX:1,scaleY:1,rotation:6.8,x:-155.1,y:-130.8},3).to({_off:true},1).wait(4).to({_off:false,regX:4.4,regY:4.3,scaleX:1,scaleY:1,x:-155.9,y:-131.1},0).to({regX:4.3,rotation:0,x:-157.1,y:-127.7},2).wait(1).to({y:-128},10,cjs.Ease.get(0.46)).to({y:-128.1},1).to({regY:4.4,scaleY:0.74,y:-119.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:4.3,scaleY:1,x:-154.7,y:-131.9},2).wait(9).to({x:-163.3,y:-130.5},5).wait(14).to({x:-154.7,y:-131.9},4).wait(10).to({x:-157.1,y:-128.1},4).wait(1).to({regY:4.4,scaleY:0.74,y:-127.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:4.3,scaleY:1,y:-128.1},2).wait(10).to({scaleY:0.87,y:-128},3).to({scaleY:1,y:-128.1},3).wait(17).to({x:-157.9,y:-129.6},6).wait(5).to({x:-149.9,y:-128.6},5).wait(4).to({x:-163.9,y:-129.4},5).wait(4).to({x:-157.9,y:-129.6},5).wait(1).to({x:-157.1,y:-128.1},8).wait(1).to({x:-157.9,y:-129.6},4).to({x:-155.9},3).to({x:-157.9},2).to({x:-155.9},2).to({x:-157.9},2).to({x:-155.9},2).to({x:-157.9},2).to({x:-155.9},2).to({x:-157.9},2).wait(1).to({x:-157.1,y:-128.1},5).wait(1));

	// eye_L
	this.instance_11 = new lib.Symbol6();
	this.instance_11.parent = this;
	this.instance_11.setTransform(-102.9,-127,1,1,0,0,0,6.2,6.2);

	var maskedShapeInstanceList = [this.instance_11];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(14).to({scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-103.7,y:-133.2},6).to({regY:6.3,scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-98.3,y:-137.1},4).to({regY:6.2,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-103.7,y:-136.2},3).to({regY:6.3,scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-98.3,y:-140.3},3).to({regY:6.2,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-103.7,y:-133.2},6).wait(1).to({scaleX:1,scaleY:1,rotation:0,x:-102.9,y:-127},9).wait(1).to({scaleY:0.74,y:-119},2).to({_off:true},1).wait(1).to({_off:false},0).to({scaleX:1.1,scaleY:1.1,x:-102.5,y:-130.5},2).to({scaleX:1,scaleY:1,y:-126.8},2).to({x:-102.6},9).to({rotation:-11.5,x:-102.2,y:-131.3},4,cjs.Ease.get(0.46)).to({_off:true},1).wait(4).to({_off:false,rotation:2.3,x:-102,y:-125.5},0).to({regX:6.3,scaleX:1,scaleY:1,rotation:6.8,x:-101.9,y:-123.6},3).to({_off:true},1).wait(4).to({_off:false,regX:6.2,scaleX:1,scaleY:1,x:-102.3,y:-123.8},0).to({rotation:0,x:-102.6,y:-126.8},2).wait(1).to({x:-102.8,y:-126.9},10,cjs.Ease.get(0.46)).to({x:-102.9,y:-127},1).to({scaleY:0.74,y:-119},2).to({_off:true},1).wait(1).to({_off:false},0).to({scaleY:1,x:-100.5,y:-132.8},2).wait(9).to({x:-109.5},5).wait(14).to({x:-100.5},4).wait(10).to({x:-102.9,y:-127},4).wait(1).to({scaleY:0.74},2).to({_off:true},1).wait(1).to({_off:false},0).to({scaleY:1},2).wait(10).to({scaleY:0.87},3).to({scaleY:1},3).wait(17).to({y:-129.5},6).wait(5).to({x:-94.9,y:-128.5},5).wait(4).to({x:-108.9,y:-129.2},5).wait(4).to({x:-102.9,y:-129.5},5).wait(1).to({y:-127},8).wait(1).to({y:-129.5},4).to({x:-100.9},3).to({x:-102.9},2).to({x:-100.9},2).to({x:-102.9},2).to({x:-100.9},2).to({x:-102.9},2).to({x:-100.9},2).to({x:-102.9},2).wait(1).to({y:-127},5).wait(1));

	// eye_R
	this.instance_12 = new lib.Symbol6();
	this.instance_12.parent = this;
	this.instance_12.setTransform(-155.1,-127,1,1,0,0,0,6.2,6.2);

	var maskedShapeInstanceList = [this.instance_12];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(14).to({regX:6.3,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-149.5,y:-122.8},6).to({regX:6.4,regY:6.3,scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-143.4,y:-123.8},4).to({regX:6.3,regY:6.2,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-149.5,y:-125.8},3).to({regX:6.4,regY:6.3,scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-143.4,y:-127.1},3).to({regX:6.3,regY:6.2,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-149.5,y:-122.8},6).wait(1).to({regX:6.2,scaleX:1,scaleY:1,rotation:0,x:-155.1,y:-127},9).wait(1).to({scaleY:0.74,y:-119},2).to({_off:true},1).wait(1).to({_off:false},0).to({scaleX:1.1,scaleY:1.1,x:-154.7,y:-130.5},2).to({scaleX:1,scaleY:1,y:-126.8},2).to({x:-154.8},9).to({rotation:-11.5,x:-153.3,y:-120.9},4,cjs.Ease.get(0.46)).to({_off:true},1).wait(4).to({_off:false,rotation:2.3,x:-153.1,y:-127.5},0).to({scaleX:1,scaleY:1,rotation:6.8,y:-129.7},3).to({_off:true},1).wait(4).to({_off:false,scaleX:1,scaleY:1,x:-153.8,y:-130},0).to({rotation:0,x:-154.8,y:-126.8},2).wait(1).to({x:-155,y:-126.9},10,cjs.Ease.get(0.46)).to({x:-155.1,y:-127},1).to({scaleY:0.74,y:-119},2).to({_off:true},1).wait(1).to({_off:false},0).to({scaleY:1,x:-152.7,y:-132.8},2).wait(9).to({x:-161.7},5).wait(14).to({x:-152.7},4).wait(10).to({x:-155.1,y:-127},4).wait(1).to({scaleY:0.74},2).to({_off:true},1).wait(1).to({_off:false},0).to({scaleY:1},2).wait(10).to({scaleY:0.87},3).to({scaleY:1},3).wait(17).to({y:-129.5},6).wait(5).to({x:-147.1,y:-128.5},5).wait(4).to({x:-161.1,y:-129.2},5).wait(4).to({x:-155.1,y:-129.5},5).wait(1).to({y:-127},8).wait(1).to({y:-129.5},4).to({x:-153.1},3).to({x:-155.1},2).to({x:-153.1},2).to({x:-155.1},2).to({x:-153.1},2).to({x:-155.1},2).to({x:-153.1},2).to({x:-155.1},2).wait(1).to({y:-127},5).wait(1));

	// eyeball_L
	this.instance_13 = new lib.Symbol7();
	this.instance_13.parent = this;
	this.instance_13.setTransform(-103,-126.9,1,1,0,0,0,11.8,11.8);

	var maskedShapeInstanceList = [this.instance_13];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(14).to({regX:12,regY:12,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-103.6,y:-133},6).to({scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-98.1,y:-136.9},4).to({scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-103.6,y:-136},3).to({scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-98.1,y:-140.1},3).to({scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-103.6,y:-133},6).wait(1).to({regX:11.8,regY:11.8,scaleX:1,scaleY:1,rotation:0,x:-103,y:-126.9},9).wait(1).to({regY:11.9,scaleY:0.37,y:-119.3},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleX:1.1,scaleY:1.1,y:-129.2},2).to({scaleX:1,scaleY:1,y:-126.9},2).wait(9).to({regX:11.9,rotation:-11.5,x:-102.4,y:-131.2},4,cjs.Ease.get(0.46)).to({_off:true},1).wait(4).to({_off:false,regY:11.9,rotation:2.3,x:-102.2,y:-125.4},0).to({scaleX:1,scaleY:1,rotation:6.8,y:-123.5},3).to({_off:true},1).wait(4).to({_off:false,scaleX:1,scaleY:1,x:-102.5,y:-123.8},0).to({regX:11.8,regY:11.8,rotation:0,x:-103,y:-126.9},2).wait(12).to({regY:11.9,scaleY:0.37,y:-119.3},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleY:1,y:-126.9},2).wait(9).to({x:-107},5).wait(14).to({x:-103},4).wait(15).to({regY:11.9,scaleY:0.37,y:-127.3},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleY:1,y:-126.9},2).wait(10).to({regY:12,scaleY:0.68,y:-128.1},3).to({regY:11.8,scaleY:1,y:-126.9},3).wait(17).to({regY:11.9,scaleY:0.92,y:-126.6},6).wait(5).to({x:-100},5).wait(4).to({x:-105},5).wait(4).to({x:-103},5).wait(1).to({regY:11.8,scaleY:1,y:-126.9},8).wait(1).to({regY:11.9,scaleY:0.92,y:-126.6},4).to({x:-101},3).to({x:-103},2).to({x:-101},2).to({x:-103},2).to({x:-101},2).to({x:-103},2).to({x:-101},2).to({x:-103},2).wait(1).to({regY:11.8,scaleY:1,y:-126.9},5).wait(1));

	// eyeball_R
	this.instance_14 = new lib.Symbol7();
	this.instance_14.parent = this;
	this.instance_14.setTransform(-155.2,-126.9,1,1,0,0,0,11.8,11.8);

	var maskedShapeInstanceList = [this.instance_14];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(14).to({regX:12,regY:12,scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-149.4,y:-122.6},6).to({scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-143.3,y:-123.7},4).to({scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-149.4,y:-125.6},3).to({scaleX:0.91,scaleY:0.91,rotation:-15.8,x:-143.3,y:-127},3).to({scaleX:0.92,scaleY:0.92,rotation:-12.5,x:-149.4,y:-122.6},6).wait(1).to({regX:11.8,regY:11.8,scaleX:1,scaleY:1,rotation:0,x:-155.2,y:-126.9},9).wait(1).to({regY:11.9,scaleY:0.37,y:-119.3},2).to({_off:true},1).wait(1).to({_off:false},0).to({regX:11.9,regY:11.8,scaleX:1.1,scaleY:1.1,x:-155.1,y:-129.2},2).to({regX:11.8,scaleX:1,scaleY:1,x:-155.2,y:-126.9},2).wait(9).to({regY:11.9,rotation:-11.5,x:-153.7,y:-120.8},4,cjs.Ease.get(0.46)).to({_off:true},1).wait(4).to({_off:false,regX:12,regY:12,rotation:2.3,x:-153.3,y:-127.4},0).to({regX:12.1,scaleX:1,scaleY:1,rotation:6.8,x:-153.2,y:-129.6},3).to({_off:true},1).wait(4).to({_off:false,regX:11.9,regY:11.9,scaleX:1,scaleY:1,x:-154.1,y:-129.9},0).to({regX:11.8,regY:11.8,rotation:0,x:-155.2,y:-126.9},2).wait(12).to({regY:11.9,scaleY:0.37,y:-119.3},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleY:1,y:-126.9},2).wait(9).to({x:-159.2},5).wait(14).to({x:-155.2},4).wait(15).to({regY:11.9,scaleY:0.37,y:-127.3},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleY:1,y:-126.9},2).wait(10).to({regY:12,scaleY:0.68,y:-128.1},3).to({regY:11.8,scaleY:1,y:-126.9},3).wait(17).to({regY:11.9,scaleY:0.92,y:-126.6},6).wait(5).to({x:-152.2},5).wait(4).to({x:-157.2},5).wait(4).to({x:-155.2},5).wait(1).to({regY:11.8,scaleY:1,y:-126.9},8).wait(1).to({regY:11.9,scaleY:0.92,y:-126.6},4).to({x:-153.2},3).to({x:-155.2},2).to({x:-153.2},2).to({x:-155.2},2).to({x:-153.2},2).to({x:-155.2},2).to({x:-153.2},2).to({x:-155.2},2).wait(1).to({regY:11.8,scaleY:1,y:-126.9},5).wait(1));

	// eyelid_L
	this.instance_15 = new lib.Symbol8();
	this.instance_15.parent = this;
	this.instance_15.setTransform(-103,-128.4,1,1,0,0,0,11.8,11.8);

	var maskedShapeInstanceList = [this.instance_15];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(14).to({regX:11.9,regY:12.1,scaleX:0.84,scaleY:0.92,rotation:-12.5,x:-104.7,y:-135},6).to({regX:12,regY:12.2,scaleX:0.84,scaleY:0.91,rotation:-15.8,x:-99.3,y:-138.8},4).to({regX:11.9,regY:12.1,scaleX:0.84,scaleY:0.92,rotation:-12.5,x:-104.7,y:-138},3).to({regX:12,regY:12.2,scaleX:0.84,scaleY:0.91,rotation:-15.8,x:-99.3,y:-142},3).to({regX:11.9,regY:12.1,scaleX:0.84,scaleY:0.92,rotation:-12.5,x:-104.7,y:-135},6).wait(1).to({regX:11.8,regY:11.8,scaleX:1,scaleY:1,rotation:0,x:-103,y:-128.4},9).wait(1).to({regY:11.9,scaleY:0.37,y:-119.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleX:1.1,scaleY:1.1,y:-130.9},2).to({scaleX:1,scaleY:1,y:-128.4},2).wait(9).to({regX:11.9,rotation:-11.5,x:-102.7,y:-132.7},4,cjs.Ease.get(0.46)).to({_off:true},1).wait(4).to({_off:false,regX:12,regY:11.9,rotation:2.3,x:-102.1,y:-126.9},0).to({scaleX:1,scaleY:1,rotation:6.8,x:-102,y:-125},3).to({_off:true},1).wait(4).to({_off:false,regX:11.9,scaleX:1,scaleY:1,x:-102.4,y:-125.3},0).to({regX:11.8,regY:11.8,rotation:0,x:-103,y:-128.4},2).wait(12).to({regY:11.9,scaleY:0.37,y:-119.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleY:1,y:-128.4},2).wait(9).to({x:-107},5).wait(14).to({x:-103},4).wait(15).to({regY:11.9,scaleY:0.37,y:-127.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleY:1,y:-128.4},2).wait(10).to({scaleY:0.68,y:-130.1},3).to({scaleY:1,y:-128.4},3).wait(17).to({scaleY:0.92,x:-101.2,y:-127.3},6).wait(5).to({x:-98.2},5).wait(4).to({x:-103.2},5).wait(4).to({x:-101.2},5).wait(1).to({scaleY:1,x:-103,y:-128.4},8).wait(1).to({scaleY:0.92,x:-101.2,y:-127.3},4).to({x:-99.2},3).to({x:-101.2},2).to({x:-99.2},2).to({x:-101.2},2).to({x:-99.2},2).to({x:-101.2},2).to({x:-99.2},2).to({x:-101.2},2).wait(1).to({scaleY:1,x:-103,y:-128.4},5).wait(1));

	// eyelid_R
	this.instance_16 = new lib.Symbol8();
	this.instance_16.parent = this;
	this.instance_16.setTransform(-155.2,-128.4,1,1,0,0,0,11.8,11.8);

	var maskedShapeInstanceList = [this.instance_16];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(14).to({regX:12.1,regY:12.2,scaleX:0.82,scaleY:0.93,rotation:-27.5,x:-149.4,y:-124.7},6).to({scaleX:0.82,scaleY:0.93,rotation:-30.8,x:-143.4,y:-125.8},4).to({scaleX:0.82,scaleY:0.93,rotation:-27.5,x:-149.4,y:-127.7},3).to({scaleX:0.82,scaleY:0.93,rotation:-30.8,x:-143.4,y:-129.1},3).to({scaleX:0.82,scaleY:0.93,rotation:-27.5,x:-149.4,y:-124.7},6).wait(1).to({regX:11.8,regY:11.8,scaleX:1,scaleY:1,rotation:0,x:-155.2,y:-128.4},9).wait(1).to({regY:11.9,scaleY:0.37,y:-119.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regX:11.9,regY:11.8,scaleX:1.1,scaleY:1.1,x:-155.1,y:-130.9},2).to({regX:11.8,scaleX:1,scaleY:1,x:-155.2,y:-128.4},2).wait(9).to({regX:11.9,rotation:-11.5,x:-153.9,y:-122.3},4,cjs.Ease.get(0.46)).to({_off:true},1).wait(4).to({_off:false,regX:12,regY:11.9,rotation:2.3,x:-153.2,y:-129},0).to({scaleX:1,scaleY:1,rotation:6.8,x:-153,y:-131.2},3).to({_off:true},1).wait(4).to({_off:false,regX:11.9,regY:11.8,scaleX:1,scaleY:1,x:-153.9,y:-131.5},0).to({regX:11.8,rotation:0,x:-155.2,y:-128.4},2).wait(12).to({regY:11.9,scaleY:0.37,y:-119.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleY:1,y:-128.4},2).wait(9).to({x:-159.2},5).wait(14).to({x:-155.2},4).wait(15).to({regY:11.9,scaleY:0.37,y:-127.8},2).to({_off:true},1).wait(1).to({_off:false},0).to({regY:11.8,scaleY:1,y:-128.4},2).wait(10).to({scaleY:0.68,y:-130.1},3).to({scaleY:1,y:-128.4},3).wait(17).to({scaleY:0.92,x:-156.9,y:-127.3},6).wait(5).to({x:-153.9},5).wait(4).to({x:-158.9},5).wait(4).to({x:-156.9},5).wait(1).to({scaleY:1,x:-155.2,y:-128.4},8).wait(1).to({scaleY:0.92,x:-156.9,y:-127.3},4).to({x:-154.9},3).to({x:-156.9},2).to({x:-154.9},2).to({x:-156.9},2).to({x:-154.9},2).to({x:-156.9},2).to({x:-154.9},2).to({x:-156.9},2).wait(1).to({scaleY:1,x:-155.2,y:-128.4},5).wait(1));

	// Layer 16
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FF0000").s().p("Ag/AhQhLgMgZg6QAYAYBAANQBJALBigQQAngGAdgaQgcA1gqAMQgxAKgtAAQggAAgfgFg");
	this.shape.setTransform(-128.7,-107.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FF0000").s().p("Ag9AqQhQgJgpg1QArAUA9AKQBNAIBggSQAwgLAoggQggA6gxAPQg8AOg5AAQgXAAgXgCg");
	this.shape_1.setTransform(-128,-109.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FF0000").s().p("Ag7AyQhVgFg4gxQA9ASA6AGQBRAFBfgWQA5gOAygmQglA+g4ATQhHAShHAAIgaAAg");
	this.shape_2.setTransform(-127.2,-111.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0000").s().p("Ag5A5QhbgBhHgtQBPARA4ACQBUABBegYQBCgSA8gtQgoBDhAAWQhTAYhXAAIgDAAg");
	this.shape_3.setTransform(-126.4,-113.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FF0000").s().p("AjuAbQBiAOA1gBQBXgDBdgaQBLgWBHg0QgsBHhHAaQhWAbhdADIgPAAQhXAAhRglg");
	this.shape_4.setTransform(-125.6,-115.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FF0000").s().p("AkBAqQB1AMAygGQBagGBdgcQBUgbBRg6QgwBMhPAdQhWAehjAHIggABQhVAAhWgeg");
	this.shape_5.setTransform(-124.8,-116.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FF0000").s().p("AkUA5QCIAJAvgJQBdgKBcgfQBdgeBchBQg1BRhWAgQhWAghoALQgbADgcAAQhQAAhZgXg");
	this.shape_6.setTransform(-124,-118.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FF0000").s().p("AkSA+QCHAHAvgJQBdgMBbggQBUgcBjhGQg4BWhVAfQhWAghkANQgeADgfAAQhMAAhVgVg");
	this.shape_7.setTransform(-122.5,-119.4);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FF0000").s().p("AkRBCQCIAGAugKQBdgNBZghQBNgaBqhMQg8BchVAeQhUAehiAPQghAFgkAAQhHABhQgVg");
	this.shape_8.setTransform(-120.9,-120.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FF0000").s().p("AkQBIQCIADAvgLQBbgOBZgiQBFgYBxhRQhBBhhTAeQhUAdhfARQgjAGgmAAQhDAAhOgSg");
	this.shape_9.setTransform(-119.4,-120.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FF0000").s().p("AkPBNQCIABAvgMQBbgPBXgjQA9gWB5hWQhFBlhTAdQhTAdhcATQglAHgqAAQg/AAhKgQg");
	this.shape_10.setTransform(-117.8,-121.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FF0000").s().p("AkQBFQCHAEAvgLQBcgNBXgiQA2gQCDhYQhBBfhiAfQhVAchcARQgkAIgmAAQg/AAhFgVg");
	this.shape_11.setTransform(-119.9,-121.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FF0000").s().p("AkSA+QCIAHAvgKQBcgMBYgfQAvgOCLhXQg7BYhyAiQhYAbhcAPQgiAHgiAAQg/AAhBgYg");
	this.shape_12.setTransform(-122,-121.5);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FF0000").s().p("AkUA2QCIAKAvgJQBdgKBYgeQApgICUhYQg2BRiDAjQhZAbhcANQghAHghAAQg9AAg8gcg");
	this.shape_13.setTransform(-124,-121.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FF0000").s().p("AkSA9QCIAHAvgKQBcgMBYgfQAogKCShbQg0BSiCAnQhYAchcAPQgkAJgjAAQg7AAg5gag");
	this.shape_14.setTransform(-122,-122.5);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FF0000").s().p("AkQBDQCHAFAvgLQBcgOBXghQAogKCRheQg0BTh/AoQhZAfhbAQQgmALgmAAQg4AAg3gYg");
	this.shape_15.setTransform(-119.9,-123.5);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FF0000").s().p("AkPBKQCIACAvgMQBbgQBXgjQAogLCOhhQgxBUiAAsQhXAghbASQgpANgpAAQg1AAg1gWg");
	this.shape_16.setTransform(-117.8,-124.6);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FF0000").s().p("AkPBHQCHADAvgLQBcgPBXgiQAqgMCMheQgzBTh4ApQhXAghdASQgnALgoAAQg5AAg4gWg");
	this.shape_17.setTransform(-118.8,-123.6);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FF0000").s().p("AkQBEQCHAFAvgLQBcgOBXghQAtgNCMhbQg2BRhxAoQhWAgheARQgmAJgnAAQg8AAg+gWg");
	this.shape_18.setTransform(-119.9,-122.6);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FF0000").s().p("AkRBBQCIAGAugLQBcgMBYghQAvgOCKhYQg4BQhpAmQhVAghgARQgkAIgmAAQhAAAhDgXg");
	this.shape_19.setTransform(-120.9,-121.6);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FF0000").s().p("AkSA/QCIAGAvgKQBcgLBYggQAxgOCJhWQg6BPhiAjQhVAhhhAQQgjAGglAAQhCAAhJgWg");
	this.shape_20.setTransform(-122,-120.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FF0000").s().p("AkTA7QCIAIAvgJQBcgLBZgfQAzgPCIhTQg8BOhbAhQhUAhhiAPQghAGgjAAQhHAAhPgYg");
	this.shape_21.setTransform(-123,-119.6);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FF0000").s().p("AkUA4QCIAKAvgJQBdgKBYgeQA2gQCHhQQg/BMhTAgQhUAghkAPQgeAEgfAAQhMAAhWgYg");
	this.shape_22.setTransform(-124,-118.6);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FF0000").s().p("AkHAuQB7AMAvgIQBdgHBZgcQA0gPB7hKQg7BKhOAdQhWAfhhAMQgXADgYAAQhMAAhUgdg");
	this.shape_23.setTransform(-124.5,-117.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FF0000").s().p("Aj6AlQBvAMAugGQBggEBXgaQAzgNBuhEQg3BGhJAcQhYAdheAIQgSACgSAAQhKAAhRggg");
	this.shape_24.setTransform(-125.1,-116.3);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FF0000").s().p("AjuAbQBiAPAwgGQBggBBWgXQAygMBjg/QgzBFhGAZQhZAchcAEIgZABQhKAAhMglg");
	this.shape_25.setTransform(-125.6,-115.2);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FF0000").s().p("AjhASQBVAQAwgFQBhABBWgUQAwgLBXg5QgvBDhAAWQhcAbhaAAIgPAAQhIABhHgpg");
	this.shape_26.setTransform(-126.1,-114);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FF0000").s().p("AhGA1QhMAChDgwQBJASAwgDQBiAEBVgTQAvgJBMgyQgsA/g7AVQhUAVhOAAIgTAAg");
	this.shape_27.setTransform(-126.6,-112.8);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FF0000").s().p("AhJAwQhGgBg5gxQA9ASAvgCQBkAGBUgQQAugIBAgsQgoA9g4ASQhKAShDAAIgmgBg");
	this.shape_28.setTransform(-127.2,-111.6);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FF0000").s().p("AhNAqQhBgDgug1QAwAVAwgBQBlAJBUgPQArgGA1gmQgkA7gzAQQhBAOg5AAQgeAAgbgDg");
	this.shape_29.setTransform(-127.7,-110.3);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#FF0000").s().p("AhRAjQg7gFgjg3QAkAWAvABQBnALBTgLQAqgGAoggQgfA4gvAOQg6ALgyAAQglAAgigGg");
	this.shape_30.setTransform(-128.2,-109);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#FF0000").s().p("AhUAdQg2gIgZg6QAYAYAwACQBnANBSgIQApgFAdgaQgcA2gqALQg0AKgsAAQgsAAgmgJg");
	this.shape_31.setTransform(-128.7,-107.6);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f("#FF0000").s().p("AhMAeQg/gIgYg6QAYAYAxADQBZALBRgHQA1gFAfgaQgcA1gsAMQgwAIgsAAQgnAAglgHg");
	this.shape_32.setTransform(-128.7,-103.4);

	this.shape_33 = new cjs.Shape();
	this.shape_33.graphics.f("#FF0000").s().p("AhFAgQhFgKgZg6QAYAYAyAFQBLAJBOgGQBDgFAhgbQgcA2guAKQguAJgsAAQgjAAgigFg");
	this.shape_33.setTransform(-128.7,-99.2);

	this.shape_34 = new cjs.Shape();
	this.shape_34.graphics.f("#FF0000").s().p("Ag9AhQhOgLgYg6QAYAYAzAHQA7AHBPgFQBPgFAjgcQgcA2gwAKQgqAJgsAAQgfAAgggEg");
	this.shape_34.setTransform(-128.7,-94.9);

	this.shape_35 = new cjs.Shape();
	this.shape_35.graphics.f("#FF0000").s().p("Ag3AjQhUgOgYg8QAYAYAzAKQBQANBHgKQBIgLAdgaQgcA1gqAMQgqAOgwAAQgcAAgfgFg");
	this.shape_35.setTransform(-128.7,-107.4);
	this.shape_35._off = true;

	this.shape_36 = new cjs.Shape();
	this.shape_36.graphics.f("#FF0000").s().p("Ag3AjQhUgOgYg8QAYAYAzAKQBQAOBHgLQBIgLAdgaQgcA1gqAMQgqAOgwAAQgcAAgfgFg");
	this.shape_36.setTransform(-128.7,-102.3);
	this.shape_36._off = true;

	this.shape_37 = new cjs.Shape();
	this.shape_37.graphics.f("#FF0000").s().p("AgjAmIgUgCQg9gLgeglQgKgMgHgPQAKAKAOAHQAVAMAeAFIACAAQA8ALA2gFIAjgDQAtgHAegNQAPgIALgJIAAABQgRAggXARQgIAGgKAEIgMAFQgqAOgwAAQgSAAgVgCg");
	this.shape_37.setTransform(-129.6,-107.4);

	this.shape_38 = new cjs.Shape();
	this.shape_38.graphics.f("#FF0000").s().p("AgjAmIgUgDQg7gKgggmQgLgMgGgPQAJAKAPAIQAVALAeAGIACAAQA6AKA4gEQARgBAQgCQAtgHAggOQAPgHALgJIAAAAQgQAggZARQgIAHgKAEIgLAEQgqAQgwAAQgSAAgVgDg");
	this.shape_38.setTransform(-130.5,-107.3);

	this.shape_39 = new cjs.Shape();
	this.shape_39.graphics.f("#FF0000").s().p("AgiAmIgVgCQg5gLgigmQgKgMgHgPQAJALAOAHQAWAMAeAFIACAAQA5ALA5gFIAhgDQArgGAigOQAPgIALgJIAAAAQgQAfgZASQgIAHgKAEIgLAEQgsAQgwAAQgRAAgTgDg");
	this.shape_39.setTransform(-131.4,-107.3);

	this.shape_40 = new cjs.Shape();
	this.shape_40.graphics.f("#FF0000").s().p("AgjAmQgKAAgJgCQg4gKgjgoQgKgLgIgPQAJALANAHQAXAMAcAFIACAAQA6ALA5gFQARgBARgBQAogHAkgOQAPgHALgKIABgBQgPAfgaATQgJAHgJAEIgLAEQgsAQgwAAQgSAAgTgDg");
	this.shape_40.setTransform(-132.2,-107.3);

	this.shape_41 = new cjs.Shape();
	this.shape_41.graphics.f("#FF0000").s().p("AgjAmQhDgEgqgwQgLgMgIgOQAJALANAHQAZANAcAEQA5ALA5gFQA6gCA0gVQAQgHAMgLQgOAegbAUQgJAHgKAEQgyAUg0AAQgSAAgTgDg");
	this.shape_41.setTransform(-133.1,-107.3);

	this.shape_42 = new cjs.Shape();
	this.shape_42.graphics.f("#FF0000").s().p("Ag4AjQhTgOgYg8QAYAYA0AKQBPAOBHgLQBIgLAdgaQgcA1gqAMQgqAOgwAAQgcAAgggFg");
	this.shape_42.setTransform(-131.2,-107.4);

	this.shape_43 = new cjs.Shape();
	this.shape_43.graphics.f("#FF0000").s().p("Ag3AjQhUgOgYg8QAYAYAzAKQBPAOBIgLQBIgLAdgaQgcA1gqAMQgqAOgwAAQgcAAgfgFg");
	this.shape_43.setTransform(-130,-107.4);

	this.shape_44 = new cjs.Shape();
	this.shape_44.graphics.f("#FF0000").s().p("Ag9AfQhKgMgUg0QATAPAxAJQBRALBLgIQA/gKAYgSQgXAugmALQgtAMgyAAQgdAAgggEg");
	this.shape_44.setTransform(-128.8,-107.1);

	this.shape_45 = new cjs.Shape();
	this.shape_45.graphics.f("#FF0000").s().p("AhCAbQhBgJgRguQAPAHAuAIQBUAKBOgIQA2gHATgLQgSAogiAJQgvAKgzAAQgfAAghgDg");
	this.shape_45.setTransform(-128.9,-106.8);

	this.shape_46 = new cjs.Shape();
	this.shape_46.graphics.f("#FF0000").s().p("AhHAXQg4gHgNgmQAKgBArAGQBXAHBRgFQAtgGAPgDQgNAigfAIQgzAHg1AAQgfAAghgCg");
	this.shape_46.setTransform(-129,-106.6);

	this.shape_47 = new cjs.Shape();
	this.shape_47.graphics.f("#FF0000").s().p("AhMAUQgvgEgJgfQAGgJAoAFQBYAEBVgDQAkgEAKAEQgIAcgbAHQg0AFg1AAQgiAAgjgCg");
	this.shape_47.setTransform(-129.1,-106.5);

	this.shape_48 = new cjs.Shape();
	this.shape_48.graphics.f("#FF0000").s().p("AhRAUQgmgBgGgZQACgRAlAEQBaACBagCQAagBAFAMQgCAVgYAFQg2ADg3AAIhHgBg");
	this.shape_48.setTransform(-129.2,-106.6);

	this.shape_49 = new cjs.Shape();
	this.shape_49.graphics.f("#FF0000").s().p("Ah0ADQgEgYAjACIC5AAQARABABASQACAPgUAFIi5AAIgGAAQgYAAgBgRg");
	this.shape_49.setTransform(-129.4,-106.8);
	this.shape_49._off = true;

	this.shape_50 = new cjs.Shape();
	this.shape_50.graphics.f("#FF0000").s().p("AhXAUQghAAgGgZQACgQAlACIC6AAQAWABAGAKQgEAWgYAGg");
	this.shape_50.setTransform(-129.2,-106.8);

	this.shape_51 = new cjs.Shape();
	this.shape_51.graphics.f("#FF0000").s().p("AhYAUQglgBgKgiQAIgGAnACIC6AAIAmACQgKAegcAHg");
	this.shape_51.setTransform(-129.1,-106.8);

	this.shape_52 = new cjs.Shape();
	this.shape_52.graphics.f("#FF0000").s().p("AhZAYQgogEgQgpQAOAEAqACIC6AAQAfAAARgIQgQAmggAJg");
	this.shape_52.setTransform(-129,-107.2);

	this.shape_53 = new cjs.Shape();
	this.shape_53.graphics.f("#FF0000").s().p("AhaAdQgsgGgUgyQATAOAtACIC5AAQAkAAAYgRQgWAvgmAKg");
	this.shape_53.setTransform(-128.9,-107.6);

	this.shape_54 = new cjs.Shape();
	this.shape_54.graphics.f("#FF0000").s().p("AhbAhQgwgHgYg6QAYAYAwACIC5AAQApAAAdgaQgcA1gqAMg");
	this.shape_54.setTransform(-128.7,-108.1);

	this.shape_55 = new cjs.Shape();
	this.shape_55.graphics.f("#FF0000").s().p("Ah0APQgCgcAkgEQBYgIBZAIQAUAFACAXQgBAIgVABQhZgHhYAHQgKACgHAAQgNAAgEgHg");
	this.shape_55.setTransform(-129.3,-107.1);

	this.shape_56 = new cjs.Shape();
	this.shape_56.graphics.f("#FF0000").s().p("AhzAYQAAgfAkgJQBWgRBUARQAYAJABAbQgEABgVgCQhVgPhVAPQgSAHgKAAQgFAAgDgCg");
	this.shape_56.setTransform(-129.3,-107.2);

	this.shape_57 = new cjs.Shape();
	this.shape_57.graphics.f("#FF0000").s().p("AhLgSQBRgZBRAZQAaANACAfQgGgGgWgEQhSgWhQAWQgbANgMACQABgiAmgPg");
	this.shape_57.setTransform(-129.3,-107);

	this.shape_58 = new cjs.Shape();
	this.shape_58.graphics.f("#FF0000").s().p("AhIgUQBNgiBNAiQAdARAEAkQgKgNgXgIQhOgdhMAdIgqAaQADgmAngUg");
	this.shape_58.setTransform(-129.3,-106.9);

	this.shape_59 = new cjs.Shape();
	this.shape_59.graphics.f("#FF0000").s().p("AhEgWQBJgqBKAqQAfAUAEApQgMgVgXgKQhLglhIAlIgtAkQAFgpAogZg");
	this.shape_59.setTransform(-129.3,-106.7);

	this.shape_60 = new cjs.Shape();
	this.shape_60.graphics.f("#FF0000").s().p("AhBgZQBFgyBGAyQAjAZAEAtQgPgcgYgOQhHgshEAsIgvAwQAGgtApgfg");
	this.shape_60.setTransform(-129.3,-106.5);
	this.shape_60._off = true;

	this.shape_61 = new cjs.Shape();
	this.shape_61.graphics.f("#FF0000").s().p("AhBgZQBFgyBGAyQAiAZAFAtQgPgcgYgOQhHgshEAsIgvAwQAGgtApgfg");
	this.shape_61.setTransform(-128.7,-106.5);
	this.shape_61._off = true;

	this.shape_62 = new cjs.Shape();
	this.shape_62.graphics.f("#FF0000").s().p("AhBgZQBGgyBFAyQAiAZAFAtQgPgcgYgOQhHgshEAsIgvAwQAGgtApgfg");
	this.shape_62.setTransform(-128.1,-106.5);
	this.shape_62._off = true;

	this.shape_63 = new cjs.Shape();
	this.shape_63.graphics.f("#FF0000").s().p("AhBgZQBGgyBFAyQAjAZAEAtQgPgcgYgOQhHgshEAsIgvAwQAGgtApgfg");
	this.shape_63.setTransform(-129.7,-106.5);

	this.shape_64 = new cjs.Shape();
	this.shape_64.graphics.f("#FF0000").s().p("AhEgXQBJgsBIAsQAgAWAEApQgNgWgXgMQhJgmhIAmIgtAoQAFgrAogag");
	this.shape_64.setTransform(-129.3,-106.6);

	this.shape_65 = new cjs.Shape();
	this.shape_65.graphics.f("#FF0000").s().p("AhGgVQBLgmBMAmQAeATADAmQgLgRgWgJQhNghhKAhIgrAfQAEgoAngWg");
	this.shape_65.setTransform(-129.3,-106.8);

	this.shape_66 = new cjs.Shape();
	this.shape_66.graphics.f("#FF0000").s().p("AhJgTQBPggBOAgQAcAPADAjQgJgLgWgHQhPgbhOAbQgZAQgQAHQADgkAmgTg");
	this.shape_66.setTransform(-129.3,-106.9);

	this.shape_67 = new cjs.Shape();
	this.shape_67.graphics.f("#FF0000").s().p("AhzAaQAAggAlgKQBUgTBUATQAYAKACAcIgagDQhVgRhTARQgWAIgKAAIgFgBg");
	this.shape_67.setTransform(-129.3,-107.2);

	this.shape_68 = new cjs.Shape();
	this.shape_68.graphics.f("#FF0000").s().p("AhzATQgCgdAlgGQBWgNBXANQAWAHABAYQgCAFgVAAQhXgLhWALQgOADgIAAQgKAAgDgEg");
	this.shape_68.setTransform(-129.3,-107.1);

	this.shape_69 = new cjs.Shape();
	this.shape_69.graphics.f("#FF0000").s().p("Ah0AMQgCgbAjgDQBagGBaAGQATAEABAWQAAAKgUACQhagGhaAGIgNACQgRAAgDgKg");
	this.shape_69.setTransform(-129.4,-107);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape}]},14).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_14}]},1).to({state:[{t:this.shape_15}]},1).to({state:[{t:this.shape_16}]},1).to({state:[{t:this.shape_17}]},1).to({state:[{t:this.shape_18}]},1).to({state:[{t:this.shape_19}]},1).to({state:[{t:this.shape_20}]},1).to({state:[{t:this.shape_21}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_22}]},1).to({state:[{t:this.shape_23}]},1).to({state:[{t:this.shape_24}]},1).to({state:[{t:this.shape_25}]},1).to({state:[{t:this.shape_26}]},1).to({state:[{t:this.shape_27}]},1).to({state:[{t:this.shape_28}]},1).to({state:[{t:this.shape_29}]},1).to({state:[{t:this.shape_30}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_31}]},1).to({state:[{t:this.shape_32}]},1).to({state:[{t:this.shape_33}]},1).to({state:[{t:this.shape_34}]},1).to({state:[]},1).to({state:[{t:this.shape_35}]},43).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_35}]},9).to({state:[{t:this.shape_37}]},1).to({state:[{t:this.shape_38}]},1).to({state:[{t:this.shape_39}]},1).to({state:[{t:this.shape_40}]},1).to({state:[{t:this.shape_41}]},1).to({state:[{t:this.shape_36}]},14).to({state:[{t:this.shape_36}]},1).to({state:[{t:this.shape_42}]},1).to({state:[{t:this.shape_43}]},1).to({state:[{t:this.shape_35}]},1).to({state:[{t:this.shape_35}]},15).to({state:[{t:this.shape_44}]},1).to({state:[{t:this.shape_45}]},1).to({state:[{t:this.shape_46}]},1).to({state:[{t:this.shape_47}]},1).to({state:[{t:this.shape_48}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_50}]},1).to({state:[{t:this.shape_51}]},1).to({state:[{t:this.shape_52}]},1).to({state:[{t:this.shape_53}]},1).to({state:[{t:this.shape_54}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_55}]},1).to({state:[{t:this.shape_56}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_58}]},1).to({state:[{t:this.shape_59}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_61}]},1).to({state:[{t:this.shape_62}]},1).to({state:[{t:this.shape_63}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_60}]},1).to({state:[{t:this.shape_64}]},1).to({state:[{t:this.shape_65}]},1).to({state:[{t:this.shape_66}]},1).to({state:[{t:this.shape_57}]},1).to({state:[{t:this.shape_67}]},1).to({state:[{t:this.shape_68}]},1).to({state:[{t:this.shape_69}]},1).to({state:[{t:this.shape_49}]},1).to({state:[{t:this.shape_49}]},1).to({state:[]},1).to({state:[{t:this.shape_49}]},26).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_35).wait(94).to({_off:false},0).wait(1).to({_off:true},1).wait(5).to({_off:false},0).wait(9).to({_off:true},1).wait(22).to({_off:false},0).wait(15).to({_off:true},1).wait(110));
	this.timeline.addTween(cjs.Tween.get(this.shape_36).wait(96).to({_off:false},0).wait(1).to({y:-97.2},0).wait(1).to({y:-92.2},0).wait(1).to({y:-97.2},0).wait(1).to({y:-102.3},0).to({_off:true},1).wait(28).to({_off:false,x:-133.7,y:-107.4},0).wait(1).to({x:-132.4},0).to({_off:true},1).wait(128));
	this.timeline.addTween(cjs.Tween.get(this.shape_49).wait(154).to({_off:false},0).wait(27).to({_off:true},1).wait(5).to({_off:false},0).to({_off:true},1).wait(42).to({_off:false},0).wait(1).to({_off:true},1).wait(26).to({_off:false},0).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.shape_60).wait(193).to({_off:false},0).wait(5).to({_off:true},1).wait(2).to({_off:false,x:-127.5},0).to({_off:true},1).wait(8).to({_off:false,x:-129.3},0).to({_off:true},1).wait(10).to({_off:false},0).wait(1).to({_off:true},1).wait(36));
	this.timeline.addTween(cjs.Tween.get(this.shape_61).wait(199).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false,x:-126.9},0).to({_off:true},1).wait(5).to({_off:false,x:-127.3},0).to({_off:true},1).wait(2).to({_off:false,x:-130.3},0).wait(1).to({x:-131.3},0).wait(5).to({x:-130.9},0).wait(1).to({x:-130.5},0).to({_off:true},1).wait(40));
	this.timeline.addTween(cjs.Tween.get(this.shape_62).wait(200).to({_off:false},0).to({_off:true},1).wait(2).to({_off:false,x:-126.3},0).wait(4).to({_off:true},1).wait(1).to({_off:false,x:-128.3},0).to({_off:true},1).wait(9).to({_off:false,x:-130.1},0).to({_off:true},1).wait(39));

	// hat02
	this.instance_17 = new lib.Symbol25("synched",0);
	this.instance_17.parent = this;
	this.instance_17.setTransform(-146,-170,1,1,0,0,0,30,16);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(51).to({_off:false},0).wait(13).to({mode:"single",startPosition:14},0).to({regX:29.9,regY:16.1,rotation:-11.5,x:-153.3,y:-164.8},4,cjs.Ease.get(0.46)).to({regX:30.1,regY:16,rotation:11.4,x:-135.9,y:-172.6},9).to({regX:30,rotation:0,x:-146,y:-170,mode:"synched",loop:false},6,cjs.Ease.get(-0.39)).wait(1).to({startPosition:14},0).to({_off:true},9).wait(166));

	// mouth02
	this.instance_18 = new lib.mouth();
	this.instance_18.parent = this;
	this.instance_18.setTransform(-129.6,-91.3,0.77,0.269,0,0,0,16.6,15.1);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(51).to({_off:false},0).to({regX:16.5,regY:14.6,scaleX:1.12,scaleY:1.12,x:-130.5,y:-99.8},2).to({regY:14.5,scaleX:1,scaleY:1,y:-97.5},2).wait(9).to({regY:14.6,scaleY:0.84,rotation:-11.5,x:-123.6,y:-96.8},4,cjs.Ease.get(0.46)).to({regX:16.6,scaleY:0.92,rotation:11.4,x:-135.1,y:-98.4},9).to({regX:16.5,regY:14.5,scaleY:1,rotation:0,x:-130.5,y:-97.5},6,cjs.Ease.get(-0.39)).wait(5).to({regX:16.8,scaleX:0.96,scaleY:0.27,x:-129.3,y:-106.6},5).to({_off:true},1).wait(165));

	// mask (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	var mask_1_graphics_47 = new cjs.Graphics().p("AhGk5Qk/hukRCeQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh8lPk7hwg");
	var mask_1_graphics_48 = new cjs.Graphics().p("Ag+kwQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYQnlBLQh7lPk8hwg");
	var mask_1_graphics_49 = new cjs.Graphics().p("Ag2koQk+hukSCeQi0B5hsC+IAA4CIZ3AAQCVYQnlBLQh7lPk8hxg");
	var mask_1_graphics_50 = new cjs.Graphics().p("AgtkfQk/hukRCeQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh8lOk7hxg");
	var mask_1_graphics_51 = new cjs.Graphics().p("AgtkfQk/hukRCeQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh8lOk7hxg");
	var mask_1_graphics_52 = new cjs.Graphics().p("Ag4kHQk/hvkRCfQi0B4hsC/IAA4DIZ3AAQCVYQnlBLQh8lPk7hwg");
	var mask_1_graphics_53 = new cjs.Graphics().p("AhDjwQk/hukRCeQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh8lOk7hxg");
	var mask_1_graphics_54 = new cjs.Graphics().p("AhOjYQk/hvkRCfQi0B4hsC/IAA4DIZ3AAQCVYQnlBLQh8lPk7hwg");
	var mask_1_graphics_55 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_56 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_57 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_58 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_59 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_60 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_61 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_62 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_63 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_64 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_65 = new cjs.Graphics().p("Av40LIZzhvQD9YDnfBrQiRlGlDhbQlFhZkGCxQisCDhfDHg");
	var mask_1_graphics_66 = new cjs.Graphics().p("AwezGIZqjLQFTXynYCGQikk9lHhJQlKhGj7C/QikCMhUDMg");
	var mask_1_graphics_67 = new cjs.Graphics().p("Aw7yOIZfkUQGWXinSCaQixk1lKg6QlNg4jzDKQidCThLDPg");
	var mask_1_graphics_68 = new cjs.Graphics().p("AxQxlIZWlJQHHXUnNCpQi8kvlLgvQlOgtjtDRQiYCZhEDSg");
	var mask_1_graphics_69 = new cjs.Graphics().p("AwyybIZjkBQGEXnnUCVQiuk4lJg+QlMg7j1DHQifCRhNDPg");
	var mask_1_graphics_70 = new cjs.Graphics().p("AwUzQIZti4QFBX2naCAQigk/lGhNQlJhKj+C8QilCKhWDLg");
	var mask_1_graphics_71 = new cjs.Graphics().p("Av00GIZzhuQD9YDnfBrQiSlGlChbQlFhZkHCwQirCDhfDHg");
	var mask_1_graphics_72 = new cjs.Graphics().p("AhKiyQlBhokNClQiyB7hnDCIgj4CIZ3glQC3YMnjBWQiDlMk+hpg");
	var mask_1_graphics_73 = new cjs.Graphics().p("AhMitQk8h1kVCYQi3B0hwC9IAi4DIZ2AlQBzYTnmBAQh0lRk5h4g");
	var mask_1_graphics_74 = new cjs.Graphics().p("Ag/iWQk2iDkcCMQi7Brh5C4IBn4AIZzBuQAtYXnpAqQhklWk0iFg");
	var mask_1_graphics_75 = new cjs.Graphics().p("AgyiBQkxiRkhB/QjABjiBCyICr35IZtC3QgYYYnrAUQhVlbktiSg");
	var mask_1_graphics_76 = new cjs.Graphics().p("AFFGQQhGlfkmifQkqiekmByQjFBbiICsIDu3xIZjEBQhdYTnqAAIgBAAg");
	var mask_1_graphics_77 = new cjs.Graphics().p("AE4GxQg3lhkeisQkjiskrBlQjIBSiQCmIEx3kIZWFIQigX5neAAIgOgBg");
	var mask_1_graphics_78 = new cjs.Graphics().p("AE/GgQg/lgkjimQkmilkpBsQjGBWiMCpIEP3qIZdEkQh+YGnkAAIgHAAg");
	var mask_1_graphics_79 = new cjs.Graphics().p("AgqhyQkqidkmB0QjEBciICsIDn3xIZkD4QhVYVnrABQhIleknieg");
	var mask_1_graphics_80 = new cjs.Graphics().p("AgyiBQkwiTkiB9QjBBhiCCxIC334IZsDFQgmYXnqARQhTlckriVg");
	var mask_1_graphics_81 = new cjs.Graphics().p("Ag9iSQk0iJkeCHQi9Boh8C2ICB3+IZxCLQASYXnpAiQhflYkxiKg");
	var mask_1_graphics_82 = new cjs.Graphics().p("AhKioQk5h8kYCSQi6Bwh0C6IBE4BIZ1BJQBRYVnoA1QhtlTk2h/g");
	var mask_1_graphics_83 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_84 = new cjs.Graphics().p("AhZjBQk+hvkSCfQi0B4hsC/IAA4DIZ3AAQCVYRnlBKQh7lPk8hwg");
	var mask_1_graphics_85 = new cjs.Graphics().p("AFOGKQg/lgkjilQkmilkpBsQjGBXiMCpIEO3rIZdEiQh8YHnlAAIgHAAg");
	var mask_1_graphics_86 = new cjs.Graphics().p("ADDHeQgLlmkHjOQkMjNk1BAQjRA4ijCUIHn20IYiIMQlMWhnEAAQgZAAgZgEg");
	var mask_1_graphics_87 = new cjs.Graphics().p("ABSIKQAelkjujrQjxjsk7AcQjWAgizCBIKN1zIXaK+QneU/mzAAQgpAAgogMg");
	var mask_1_graphics_88 = new cjs.Graphics().p("AgCIeQA8lgjZj/Qjdj/k8AAQjYAOi9BxIMC02IWZM8QpATtmpAAQg1AAgygUg");
	var mask_1_graphics_89 = new cjs.Graphics().p("AgCIfQA8lgjZj/Qjdj/k8ABQjYAOi9BwIMC02IWZM8QpATtmpAAQg1AAgygUg");
	var mask_1_graphics_90 = new cjs.Graphics().p("AgCIgQA8lgjZj/Qjdj/k8ABQjYAOi9BwIMC01IWZM7QpATtmpAAQg1AAgygUg");
	var mask_1_graphics_91 = new cjs.Graphics().p("AgCIiQA8lhjZj+Qjdj/k8AAQjYAOi9BwIMC01IWZM8QpATsmpAAQg1AAgygTg");
	var mask_1_graphics_92 = new cjs.Graphics().p("AgCIjQA8lgjZj/Qjdj/k8AAQjYAOi9BxIMC02IWZM8QpATtmpAAQg1AAgygUg");
	var mask_1_graphics_93 = new cjs.Graphics().p("AgCIkQA8lgjZj/Qjdj/k8ABQjYAOi9BwIMC02IWZM8QpATtmpAAQg1AAgygUg");

	this.timeline.addTween(cjs.Tween.get(mask_1).to({graphics:null,x:0,y:0}).wait(47).to({graphics:mask_1_graphics_47,x:-95.1,y:-149.4}).wait(1).to({graphics:mask_1_graphics_48,x:-94.3,y:-148.5}).wait(1).to({graphics:mask_1_graphics_49,x:-93.5,y:-147.6}).wait(1).to({graphics:mask_1_graphics_50,x:-92.6,y:-146.8}).wait(1).to({graphics:mask_1_graphics_51,x:-92.6,y:-146.8}).wait(1).to({graphics:mask_1_graphics_52,x:-93.7,y:-144.4}).wait(1).to({graphics:mask_1_graphics_53,x:-94.8,y:-142.1}).wait(1).to({graphics:mask_1_graphics_54,x:-95.9,y:-139.7}).wait(1).to({graphics:mask_1_graphics_55,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_56,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_57,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_58,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_59,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_60,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_61,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_62,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_63,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_64,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_65,x:-101.7,y:-140.3}).wait(1).to({graphics:mask_1_graphics_66,x:-105.5,y:-142.6}).wait(1).to({graphics:mask_1_graphics_67,x:-108.4,y:-144.3}).wait(1).to({graphics:mask_1_graphics_68,x:-110.5,y:-145.5}).wait(1).to({graphics:mask_1_graphics_69,x:-107.5,y:-143.7}).wait(1).to({graphics:mask_1_graphics_70,x:-104.5,y:-141.7}).wait(1).to({graphics:mask_1_graphics_71,x:-101.3,y:-139.7}).wait(1).to({graphics:mask_1_graphics_72,x:-98.1,y:-137.6}).wait(1).to({graphics:mask_1_graphics_73,x:-96.5,y:-137.3}).wait(1).to({graphics:mask_1_graphics_74,x:-96.6,y:-138.7}).wait(1).to({graphics:mask_1_graphics_75,x:-96.6,y:-140}).wait(1).to({graphics:mask_1_graphics_76,x:-96.5,y:-141.3}).wait(1).to({graphics:mask_1_graphics_77,x:-96.3,y:-142.4}).wait(1).to({graphics:mask_1_graphics_78,x:-96.5,y:-141.9}).wait(1).to({graphics:mask_1_graphics_79,x:-96.7,y:-141.3}).wait(1).to({graphics:mask_1_graphics_80,x:-96.8,y:-140.6}).wait(1).to({graphics:mask_1_graphics_81,x:-96.9,y:-139.7}).wait(1).to({graphics:mask_1_graphics_82,x:-97,y:-138.6}).wait(1).to({graphics:mask_1_graphics_83,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_84,x:-97,y:-137.4}).wait(1).to({graphics:mask_1_graphics_85,x:-92.8,y:-144}).wait(1).to({graphics:mask_1_graphics_86,x:-77.9,y:-148.4}).wait(1).to({graphics:mask_1_graphics_87,x:-66.4,y:-151.2}).wait(1).to({graphics:mask_1_graphics_88,x:-58,y:-152.8}).wait(1).to({graphics:mask_1_graphics_89,x:-57.5,y:-152.7}).wait(1).to({graphics:mask_1_graphics_90,x:-57,y:-152.5}).wait(1).to({graphics:mask_1_graphics_91,x:-56.5,y:-152.4}).wait(1).to({graphics:mask_1_graphics_92,x:-56,y:-152.3}).wait(1).to({graphics:mask_1_graphics_93,x:-55.5,y:-152.2}).wait(1).to({graphics:null,x:0,y:0}).wait(165));

	// berry
	this.instance_19 = new lib.berry();
	this.instance_19.parent = this;
	this.instance_19.setTransform(-115.3,-167.6,0.753,0.753,-22.7,0,0,34,52);
	this.instance_19._off = true;

	var maskedShapeInstanceList = [this.instance_19];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(47).to({_off:false},0).to({regX:34.1,regY:51.9,scaleX:0.92,scaleY:0.92,rotation:-8.7,x:-100.3,y:-208.5},4).to({regX:34,regY:52,scaleX:1,scaleY:1,rotation:0,x:-93,y:-200},4).wait(9).to({rotation:-11.5,x:-107.2,y:-204.8},4,cjs.Ease.get(0.46)).to({rotation:11.4,x:-78,y:-191.5},9).to({rotation:0,x:-93,y:-200},6,cjs.Ease.get(-0.39)).wait(1).to({rotation:23.7,x:-51,y:-179.9},4,cjs.Ease.get(0.46)).to({regY:51.9,scaleX:0.77,scaleY:0.77,rotation:68.7,x:-84.9,y:-117.8},5).to({_off:true},1).wait(165));

	// hat03
	this.instance_20 = new lib.Candy03("synched",0,false);
	this.instance_20.parent = this;
	this.instance_20.setTransform(-128.5,-178,1,1,0,0,0,38.5,9);
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(95).to({_off:false},0).wait(15).to({mode:"single",startPosition:15},0).to({rotation:5.2,x:-124.4},5).wait(14).to({startPosition:15},0).to({rotation:2.5,x:-126.5,startPosition:16},2).to({rotation:0,x:-128.5,mode:"synched",startPosition:18,loop:false},2).wait(1).to({startPosition:18},0).wait(13).to({startPosition:0},0).to({_off:true},1).wait(111));

	// hat04
	this.instance_21 = new lib.hat04();
	this.instance_21.parent = this;
	this.instance_21.setTransform(-129,-184,1,1,0,0,0,39,16);
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(148).to({_off:false},0).wait(24).to({mode:"synched",startPosition:24,loop:false},0).to({_off:true},15).wait(72));

	// hat05
	this.instance_22 = new lib.Symbol48("synched",0);
	this.instance_22.parent = this;
	this.instance_22.setTransform(-130,-169,1,1,0,0,0,67,24);
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(187).to({_off:false},0).wait(9).to({startPosition:9,loop:false},0).wait(2).to({mode:"single"},0).to({rotation:5.5,x:-125,y:-168.5},5).to({startPosition:9},4).to({regY:24.1,rotation:-5.2,x:-135,y:-167.9},5).to({rotation:-5.2},4).to({regY:24,rotation:0,x:-130,y:-169,mode:"synched",startPosition:10,loop:false},5).to({startPosition:10},1).to({startPosition:19},8).to({_off:true},1).wait(28));

	// brow4
	this.instance_23 = new lib.Symbol65();
	this.instance_23.parent = this;
	this.instance_23.setTransform(-145.1,-152,0.617,0.391,0,14.5,-165.5,11.2,8.8);
	this.instance_23.alpha = 0.211;
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(232).to({_off:false},0).to({regX:11.3,regY:8.6,scaleX:1,scaleY:1.05,skewX:29.5,skewY:-150.5,y:-149,alpha:1},3).to({x:-143.1},3).to({x:-145.1},2).to({x:-143.1},2).to({x:-145.1},2).to({x:-143.1},2).to({x:-145.1},2).to({x:-143.1},2).to({x:-145.1},2).wait(1).to({regX:11.2,regY:8.8,scaleX:0.62,scaleY:0.39,skewX:14.5,skewY:-165.5,y:-152,alpha:0.211},4).to({_off:true},1).wait(1));

	// brow3
	this.instance_24 = new lib.Symbol65();
	this.instance_24.parent = this;
	this.instance_24.setTransform(-110.4,-152.3,0.542,0.187,-9,0,0,11.5,8.8);
	this.instance_24.alpha = 0.211;
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(232).to({_off:false},0).to({regY:8.6,scaleX:1,scaleY:1,rotation:-24,y:-149.4,alpha:1},3).to({x:-108.4},3).to({x:-110.4},2).to({x:-108.4},2).to({x:-110.4},2).to({x:-108.4},2).to({x:-110.4},2).to({x:-108.4},2).to({x:-110.4},2).wait(1).to({regY:8.8,scaleX:0.54,scaleY:0.19,rotation:-9,y:-152.3,alpha:0.211},4).to({_off:true},1).wait(1));

	// mouth06
	this.instance_25 = new lib.mouth06();
	this.instance_25.parent = this;
	this.instance_25.setTransform(-129.3,-106.1,0.533,0.488,0,0,0,26.7,11.2);
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(232).to({_off:false},0).to({regX:26.6,regY:11.1,scaleX:0.91,scaleY:1,y:-103.9},3).to({scaleX:1.11,rotation:-2,x:-126.8},3).to({regX:26.5,regY:11,scaleX:1,rotation:0,x:-129.5,y:-104},2).to({regX:26.6,regY:11.1,scaleX:1.11,rotation:-2,x:-126.8,y:-103.9},2).to({regX:26.5,regY:11,scaleX:1,rotation:0,x:-129.5,y:-104},2).to({regX:26.6,regY:11.1,scaleX:1.11,rotation:-2,x:-126.8,y:-103.9},2).to({regX:26.5,regY:11,scaleX:1,rotation:0,x:-129.5,y:-104},2).to({regX:26.6,regY:11.1,scaleX:1.11,rotation:-2,x:-126.8,y:-103.9},2).to({regX:26.5,regY:11,scaleX:1,rotation:0,x:-129.5,y:-104},2).wait(1).to({regX:26.7,regY:11.2,scaleX:0.53,scaleY:0.49,x:-129.3,y:-106.1},4).to({_off:true},1).wait(1));

	// body06_3
	this.instance_26 = new lib.Symbol66();
	this.instance_26.parent = this;
	this.instance_26.setTransform(-133.5,-117.5,1,1,0,0,0,81.5,83.5);
	this.instance_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(235).to({_off:false},0).to({rotation:-3.2},3).to({rotation:0},2).to({rotation:-3.2},2).to({rotation:0},2).to({rotation:-3.2},2).to({rotation:0},2).to({rotation:-3.2},2).to({rotation:0},2).wait(1).to({_off:true},1).wait(5));

	// body06_2
	this.instance_27 = new lib.Symbol67();
	this.instance_27.parent = this;
	this.instance_27.setTransform(-133.5,-117.5,1,1,0,0,0,81.5,83.5);
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(235).to({_off:false},0).to({rotation:3.2,y:-117.4},3).to({rotation:0,y:-117.5},2).to({rotation:3.2,y:-117.4},2).to({rotation:0,y:-117.5},2).to({rotation:3.2,y:-117.4},2).to({rotation:0,y:-117.5},2).to({rotation:3.2,y:-117.4},2).to({rotation:0,y:-117.5},2).wait(1).to({_off:true},1).wait(5));

	// body06_1
	this.instance_28 = new lib.body06();
	this.instance_28.parent = this;
	this.instance_28.setTransform(-133.5,-117.5,1,1,0,0,0,81.5,83.5);
	this.instance_28._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_28).wait(235).to({_off:false},0).wait(18).to({_off:true},1).wait(5));

	// Body (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	var mask_2_graphics_0 = new cjs.Graphics().p("AjDLMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAyhBghhdIgKgaQgQgpgLgnQgphqAAh5QAAkUDUjEQDUjEEsAAQEtAADUDEIAgAfIAKAKIAcAfIABABQBMBbAhBrQAgBfAABrQAAApgFAmIAAAGQgLDjh0DQIAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");
	var mask_2_graphics_15 = new cjs.Graphics().p("AiuK/Qg1gSgWgmQgVggg1gDQg5gBgeAiQgkAhhPgIQhsgIgVhZQgUhPAxhGQAggrgDg3QgUgZgJgbIgLgZIgCgFIgKgYQgOgngLglQgnhkAAhyQAAkFDJi6QDJi5EcAAQA3AAA2AHQDegwCdCDQCmCLA5DuQA0DCgtCrQgcBfgoBSQABA+gpBWIgBgCIABACQgNAmAXAfQAZAqgdA3QgeA2hAABQgvAEgbgaQgngkgqAnIgCACIgEADQgRAXggAJQghAIgvgMIgRgFIgJAEQgwAUgmgUQgygXADgsQghAGgTAYQgsAsgiAHQgYAGgXAAQgWAAgWgGg");
	var mask_2_graphics_16 = new cjs.Graphics().p("ABgL6QgygaADgxQAEgwgVgfIgGgGIgQgDQgLAIgKALQgpAsghAHQguAMgqgMQgygSgWgmQgUgggzgDQg3gBgdAiQgiAhhNgIQhngIgVhZQgShPAvhGQAagkACgsQgFgJgDgMQglghgNgpIgLgXIgBgEIgKgXQgOgkgKgkQgkhdAAhsQAAj3C9iuQC9ivEMAAQAbAAAZACQAwgdA9gUQDxhBChCRQCgCSAwD6QAsDMg1CzQgqCJhDBvQgJAegRAjIAAgCIAAACQgMAmAXAfQAXAqgcA3QgdA2g9ABQguAEgZgaQgSgSgUABQgjAbgmAXIgCAAQgIAEgKADQgPAEgTgBQABAQgBARQgCAsgzAWQgYAKgVAAQgWAAgTgKg");
	var mask_2_graphics_17 = new cjs.Graphics().p("ABkM9QgygcAEg0QADg1gVghQg3hKgoAqQgpAqgjAHQgjAGgWgdQgTgUACgdQgSgOgLgUQgTghgygCQgzgBgcAhQgiAhhJgIQhjgIgUhZQgShOAthGIAJgOQgNgeAFgzIgaguQgZgbgKgfIgKgVIgCgFIgJgVQgNgigJghQgMgegIgfIgBgEQgjiGAehsIARgwQAqiABuhmQCyikD7AAQAdAAAbADQBIhIBygnQD1hECaCYQCaCZAlEFQAjDVg8C8Qg6CqhZCDIgSAaQgBAXAPAVQAWApgbA3QgbA3g7AAQgsAFgYgaIgJgIQgyAqg3AjIgmgGIAAARQAMAugCAzQgBAvgzAYQgYAMgWAAQgWAAgTgMg");
	var mask_2_graphics_18 = new cjs.Graphics().p("ABpOAQgygeADg4QAEg5gWgjQg2hQgpAtQgpAugiAHQgjAHgWggQgYgaAJgqQADg4gMgaQgGgMgIgFIgCgBQgegTg/AJQgfAEgVgHIgBACQggAhhGgIQhfgIgThZQgOg/AYg4QgFggAHgvIACgXQgng+gghCIgHgQIgOggIgxiBQgniMAbhtIAPgwQAfhPBBhAQAZgeAegcQCniaDrAAQAsAAAqAGQBSh3Ceg2QD4hHCUCfQCTCeAYERQAaDehEDEQhCCxhfCIIgYAhIAIANQAWAqgaA3QgbA2g4ABQgXACgRgGQgUAQgTgBIgHgCQgwAog0AhIgEAAQgGAdADAqQAMAxgCA3QgCAygzAaQgYAMgWAAQgVAAgTgMg");
	var mask_2_graphics_19 = new cjs.Graphics().p("ABrPBQgyggADg8QAEg8gVgmQg3hVgpAxQgpAwgiAHQgjAIgWghQgYgdAJgsQAFhYgegUQgegUg/AKQhRALgQhFQgUADgZgDQhbgIgShZQgEgTgBgSQgJglAIg8QAFgngIgpQg0hUgphdIg1iHQgriPAWhvIAOgxQA2iWCxhfQAvgZA4gOQBZggBmAAQBEAAA9AOQBYiqDEhFQD7hJCMCkQCMClAMEbQAQDnhODLQhKC4hlCNIgQAWIgFATQAJAjgUAsQgHAQgLAMQgUAxgjAkQgdAngdgBQgqgMgbAUQgzAaAGB1QAMA1gCA7QgCA1gzAbQgYAOgVAAQgWAAgTgOg");
	var mask_2_graphics_20 = new cjs.Graphics().p("ABrQCQgygiADhAQAEhAgVgpQg3hbgpA0QgpA0giAIQgjAIgWgkQgYgeAJgvQAFhegegVQgegVg/AKQhTAMgPhMQgPhNgvAOQgwANglg9QgXgpALhcQALhagxhhQgig8gcg/Ig4iLQgviTAThwIAMgxQAwiYCrhcQCqhYEfA6QBejRDfhRQD+hMCFCrQCFCqgBElQAFDvhWDTQgfBGgjBCIgBADIgVAlIgCAEIAAABIgPAaIgCADIgDAFQhFB8gUB4QgTBog6BBQgdApgdgBQgqgNgbAVQgzAcAGB9QAMA4gCA/QgCA5gzAdQgYAOgVAAQgWAAgTgOg");
	var mask_2_graphics_21 = new cjs.Graphics().p("ABqQIQgygjAEhAQADhAgVgpQg2hcgoA1QgpA0gjAIQgiAIgWgkQgYgeAJgwQAFhegegVQgegWg/ALQhSAMgOhNQgPhNgwAOQgvANglg9QgXgqALhcQAKhYgsheQgkg/gehCIg3iMQgviUAThxIAMgxQAwiYCphdQCqhYEdA6IABAAIAAgBQBcjSDfhRQD8hNCFCsQCDCrAAEnQAFDwhWDUQgeBFgiBBIgDAGIgUAlIgWAoQhFB8gUB4QgSBqg6BAQgdAqgdgBQgqgNgbAVQgzAcAGB+QAMA4gCA/QgBA5gzAeQgYAOgVAAQgWAAgTgOg");
	var mask_2_graphics_22 = new cjs.Graphics().p("ABqQNQgxgjADhAQAEhBgVgpQg3hcgoA1QgoA0giAIQgjAIgWgkQgXgeAIgwQAGhfgegVQgegWg+ALQhSAMgPhNQgPhOguAOQgwAOglg+QgWgqALhcQAJhWgphbQglhDgfhFIg3iNQguiUAShyIAMgyQAwiZCphdQCohZEcA7QBdjUDdhSQD7hMCDCsQCECsgBEoQAFDxhWDWQgeBGgiBBIgCAGIgUAlIgWAoQhEB9gUB5QgSBpg6BBQgdAqgcgBQgqgNgbAVQgyAdAGB+QALA5gBA/QgCA5gyAeQgYAOgWAAQgVAAgTgOg");
	var mask_2_graphics_23 = new cjs.Graphics().p("ABpQSQgxgiADhCQAEhBgVgpQg2hcgoA1QgoA0giAIQgjAIgVgkQgXgeAIgxQAFhfgdgVQgegWg+ALQhRAMgPhOQgPhOguAOQgwAOgkg+QgXgqALhdQALhaguhfQgig/gdhCIg3iOQguiVAShyIAMgyQAwiaCnhdQCohaEaA7IAAAAIABAAQBbjVDchSQD6hNCDCtQCCCtAAEqQAFDyhVDXQgjBRgnBKIgEAIIgDAGIgZAtIgDAFQhEB+gTB5QgTBqg5BCQgdAqgcgBQgpgNgbAVQgyAcAGB/QAMA5gCBAQgCA6gyAdQgYAPgVAAQgVAAgTgPg");
	var mask_2_graphics_24 = new cjs.Graphics().p("ABpQYQgxgjADhBQAEhCgVgpQg2hdgnA1QgoA1giAIQgiAIgWgkQgXgfAJgwQAFhggdgVQgegWg+ALQhQAMgPhOQgPhPguAOQgvAOgkg+QgXgrALhdQAKhagsheQgjhBgdhDIg3iOQguiXAThyIALgyQAwibCmheQCnhaEZA7IAAAAIAAAAQBcjWDahSQD5hOCCCuQCBCuAAEsQAFDzhVDYQgeBHghBBIgCAGIgLATIgKATIgWAoQhDB/gUB6QgSBqg4BCQgdArgcgBQgqgOgaAWQgyAcAGCAQAMA5gCBAQgCA6gyAeQgXAOgWAAQgVAAgSgOg");
	var mask_2_graphics_25 = new cjs.Graphics().p("ABoQdQgxgjAEhCQADhBgVgqQg1hdgnA1QgoA1giAIQgiAIgVgkQgXgfAIgxQAGhfgegWQgdgWg9ALQhRAMgOhPQgPhPguAPQgvAOgkg/QgWgrALheQAKhZgshfQgihCgehEIg2iOQgtiXASh0IALgyQAvibCmhfQCmhaEXA7IAAAAIAAAAQBbjXDZhTQD3hOCCCvQCACvAAEtQAFD1hUDYQgeBHggBCIgDAHIgUAmIgWAoQhDB/gUB7QgSBrg4BCQgdArgcgBQgpgNgaAVQgyAdAGCAQAMA6gCBAQgCA6gxAeQgXAPgVAAQgVAAgTgPg");
	var mask_2_graphics_26 = new cjs.Graphics().p("ABoQiQgxgjAEhCQADhCgUgqQg1hdgoA1QgnA1giAJQghAIgWglQgWgfAIgxQAFhggdgWQgdgWg9ALQhQAMgPhPQgOhPguAOQguAOgkg/QgWgrAKheQAKhZgqheQgjhDgehGIg1iPQguiYASh0IAMgyQAuidClhfQClhaEWA7QBajYDYhTQD2hPCACwQCBCwgBEvQAFD2hUDZQgdBIghBDIgCAFIgUAnIgDAEIgMAWIgHAOQhDCAgTB7QgSBsg4BCQgcArgcgBQgpgNgaAVQgxAdAFCBQAMA6gCBBQgBA6gyAeQgXAPgVAAQgVAAgSgPg");
	var mask_2_graphics_27 = new cjs.Graphics().p("ABnQoQgxgjAEhDQADhCgUgqQg1hegnA2QgnA1ghAIQgiAJgVglQgXgfAIgyQAGhggdgWQgdgXg9ALQhPANgPhQQgOhPguAOQguAOgkg/QgWgrALhfQALhggxhlQgfg9gbhAIg2iQQgtiYASh1IALgzQAvidCkhfQCkhcEUA9IAAAAIAAgBQBajZDXhTQD0hPCACxQB/CwAAEwQAED4hTDaQgeBKghBEIgBADIgUAnIgDAFIgCAEIgQAeIgBABQhCCBgTB8QgSBsg4BDQgcArgbgBQgpgNgaAVQgxAdAGCCQALA6gCBBQgBA7gxAeQgXAPgVAAQgVAAgSgPg");
	var mask_2_graphics_28 = new cjs.Graphics().p("ABmQtQgvgjADhDQADhCgUgrQg0hfgnA3QgnA1ghAJQgiAIgVglQgWgfAIgyQAFhhgcgWQgegXg8ALQhPANgOhQQgOhQguAOQguAPgjhAQgWgrALhgQAKhdgthiQghhAgchEIg1iRQgtiZARh1IAMgzQAuieCjhgQCjhcESA9IAAAAIABgBQBZjaDWhUQDzhPB/CyQB+CxAAEyQAFD4hTDcQgeBLghBFIgBAAIAAACIgUAnIgEAHIAAABIgCADIgJARIgEAIIgCAEQhCCBgTB9QgSBtg3BDQgdArgbgBQgpgNgZAWQgxAdAGCCQALA7gCBBQgBA7gxAeQgXAPgVAAQgUAAgTgPg");
	var mask_2_graphics_29 = new cjs.Graphics().p("ABmQzQgwgkAEhDQADhDgUgrQg0hfgnA3QgnA2ggAIQgiAIgVglQgWgfAIgyQAFhigcgWQgdgXg8ALQhOANgPhQQgOhRgtAPQguAOgjhAQgWgrALhgQAKhegthjQghhAgbhEIg1iRQgtiaASh2IALg0QAuieCihhQCihcERA9QBZjcDUhUQDyhQB+CzQB+CygBEzQAFD6hSDdQgeBMghBFIgBABIgDAGIgQAhIgOAaIgHAPQhCCCgTB9QgSBug3BDQgcAsgbgBQgogOgaAWQgwAdAFCDQAMA7gCBCQgCA7gwAfQgXAPgVAAQgUAAgSgPg");
	var mask_2_graphics_30 = new cjs.Graphics().p("ABlQ4QgvgjADhEQAEhDgUgrQg0hggmA3QgnA2ghAJQghAIgVglQgWggAIgyQAFhigcgWQgdgXg7ALQhOANgOhRQgPhRgsAPQguAOgjhAQgVgsAKhgQAKhdgrhiQgihCgchGIg0iSQgtibASh2IALg0QAuigChhgQChhdEPA9QBZjdDThVQDwhQB9C0QB+CzgBE1QAFD7hSDeQgdBKggBFIgCAFIgTAmIgWAqQhBCCgTB+QgRBug3BEQgcAsgbgBQgogOgZAWQgxAdAGCEQALA7gCBCQgBA8gwAeQgXAPgUAAQgVAAgSgPg");
	var mask_2_graphics_31 = new cjs.Graphics().p("ABmQvQgwgjAEhDQADhDgUgqQg0hfgnA2QgnA2ghAIQghAJgVglQgXggAIgxQAFhigcgWQgdgWg8ALQhPAMgOhQQgPhQgtAOQguAPgjhAQgWgrALhgQAKhegthiQghhAgchDIg1iSQgtiZASh2IALgzQAvieCihgQCjhcERA9IABgBQBZjaDVhUQDyhQB/CyQB+CyAAEyQAFD5hTDdQgeBKghBFIgBACIgIAQIgHAPIgEAIIgTAjIgDAGQhBCBgTB9QgSBtg3BDQgdAsgbgBQgogOgaAWQgxAdAGCDQAMA6gCBBQgCA8gwAeQgXAPgVAAQgVAAgSgPg");
	var mask_2_graphics_32 = new cjs.Graphics().p("ABnQmQgwgjAEhCQADhDgUgpQg1hfgnA2QgoA1ghAJQgiAIgVglQgWgfAIgxQAFhhgdgWQgdgWg9ALQhPANgPhQQgOhPguAOQguAOgkg/QgWgrALhfQAJhZgpheQgkhDgdhHIg2iPQgtiZASh0IAMgzQAuidCkhfQClhbEUA8IAAAAQBajZDYhUQD0hOCACwQCACxgBEvQAFD3hTDbQgdBHggBBIgDAIIgUAmIgWApQhCCAgUB8QgRBsg4BDQgdArgbgBQgpgOgaAWQgxAdAGCBQALA6gCBBQgBA7gxAeQgYAPgVAAQgUAAgTgPg");
	var mask_2_graphics_33 = new cjs.Graphics().p("ABoQdQgxgjADhCQAEhBgVgqQg1hdgnA1QgoA1giAIQgiAIgVgkQgXgfAIgxQAGhggegVQgdgWg+ALQhQAMgOhPQgPhPguAPQgvAOgkg/QgWgrALheQAKhZgshfQgihCgehEIg2iOQgtiXASh0IALgyQAvibCmhfQCmhaEXA7IAAAAIAAAAQBbjXDZhTQD3hOCCCvQCACvAAEtQAFD1hUDYQgdBHghBBIgDAIIgUAmIgWAoQhDB/gUB7QgSBrg4BCQgdArgcgBQgpgNgaAVQgyAdAGCAQAMA6gCBAQgCA6gxAeQgXAPgVAAQgVAAgTgPg");
	var mask_2_graphics_34 = new cjs.Graphics().p("ABpQUQgxgjADhBQAEhBgVgpQg2hdgnA1QgpA1giAIQgiAIgWgkQgXgfAJgwQAFhfgdgWQgegWg+ALQhRAMgPhNQgPhPguAOQgvAOglg+QgWgqALhdQAKhagthgQgjg/gdhDIg2iNQgviWAThyIALgyQAwiaCnheQCohZEZA7IAAAAIABgBQBcjVDbhSQD5hNCDCtQCCCugBEqQAFDzhVDWQgdBGghBBIgDAIIgUAlIgXAoQhDB+gUB6QgSBqg5BCQgdAqgcgBQgqgNgaAVQgyAdAGB/QALA5gCA/QgBA6gyAeQgYAOgVAAQgVAAgTgOg");
	var mask_2_graphics_35 = new cjs.Graphics().p("ABqQLQgygiAEhBQADhAgVgpQg2hcgoA1QgpA0giAIQgjAIgVgkQgYgeAJgwQAFhfgegVQgegVg+AKQhSAMgPhNQgPhOgvAPQgvANglg9QgXgqALhdQALhYgtheQgjhAgehCIg3iMQgviVAThxIAMgyQAwiYCphdQCohZEdA6IAAAAIAAAAQBdjTDdhSQD8hMCECsQCDCsgBEnQAFDxhVDVQgfBGghBBIgDAGIgUAlIgFAJIgGAJIgDAHIgIAPQhFB9gUB4QgSBqg5BBQgeAqgcgBQgqgNgbAVQgyAcAGB+QAMA5gCA/QgCA5gyAdQgYAPgWAAQgVAAgTgPg");
	var mask_2_graphics_36 = new cjs.Graphics().p("ABrQCQgygiADhAQAEhAgVgpQg3hbgpA0QgpA0giAIQgjAIgWgkQgYgeAJgvQAFhegegVQgegVg/AKQhTAMgPhMQgPhNgvAOQgwANglg9QgXgpALhcQALhagxhhQgig8gcg/Ig4iLQgviTAThwIAMgxQAwiYCrhcQCqhYEfA6QBejRDfhRQD+hMCFCrQCFCqgBElQAFDvhWDTQgfBGgjBCIgBADIgVAlIgCAEIAAABIgPAaIgCADIgDAFQhFB8gUB4QgTBog6BBQgdApgdgBQgqgNgbAVQgzAcAGB9QAMA4gCA/QgCA5gzAdQgYAOgVAAQgWAAgTgOg");
	var mask_2_graphics_38 = new cjs.Graphics().p("ABrQCQgygiADhAQAEhAgVgpQg3hbgpA0QgpA0giAIQgjAIgWgkQgYgeAJgvQAFhegegVQgegVg/AKQhTAMgPhMQgPhNgvAOQgwANglg9QgXgpALhcQALhagxhhQgig8gcg/Ig4iLQgviTAThwIAMgxQAwiYCrhcQCqhYEfA6QBejRDfhRQD+hMCFCrQCFCqgBElQAFDvhWDTQgfBGgjBCIgBADIgVAlIgCAEIAAABIgPAaIgCADIgDAFQhFB8gUB4QgTBog6BBQgdApgdgBQgqgNgbAVQgzAcAGB9QAMA4gCA/QgCA5gzAdQgYAOgVAAQgWAAgTgOg");
	var mask_2_graphics_39 = new cjs.Graphics().p("ABqPRQgyghADg9QAEg9gVgmQg3hXgpAxQgpAygiAHQgjAIgWgiQgYgdAJgtQAFhZgegUQgegVg/AKQhTAMgPhJIgEgRQgQACgVgDQhTgHgVhOIgCgDQgXgoALhXQAGgxgNgyQgxhRgnhYIg1iIQgtiQAVhvIAOgxQA0iXCwheQA5geBFgOQBMgWBUAAQBLAABCARQBai1DMhJQD7hKCLCmQCLCnAJEdQANDphPDMQhMC6hmCPIgGAIQgMAngHAmQgTBkg6A9QgdAogdgBQgqgMgbAUQgzAaAGB4QAMA1gCA8QgCA2gzAcQgYAOgVAAQgWAAgTgOg");
	var mask_2_graphics_40 = new cjs.Graphics().p("ABoOgQgygfAEg7QADg6gVglQg3hSgoAvQgpAvgjAHQgjAHgWggQgXgbAIgrQAGhVgegUQgfgTg/AJQg4AIgZgeQgbAKgogFQhdgIgThZQgJguAKgsQgDgfAFgtQAEgZgDgaQgzhTgohbIgziEQgpiOAYhuIAPgxQAqhtBshSQCgiTDiAAQA5AAA0AJQBViNCwg+QD5hICQCiQCQCiATEWQAVDihJDHQhFC1hiCLIgYAhQAVApgZA3QgXAygxAEIgQASQgeAmgcgBQgdgIgXAHIgrAfQgYAkAEBRQAMAzgCA4QgBA0gzAbQgYANgWAAQgVAAgUgNg");
	var mask_2_graphics_41 = new cjs.Graphics().p("ABmNuQgygeADg3QAEg3gVgjQg3hPgpAtQgpAtgiAGQgjAHgWgeQgYgaAJgpQACgfgDgWQgSgOgKgUQgIgNgOgJQgbgFgoAFQgTADgPgCQgMAIgJALQggAhhHgIQhggIgUhZQgPhEAfg+QgGghAHgzIABgPQgcgtgYgvQgRgVgHgYIgKgVIgBgEIgJgUQgMgggJgfQgIgUgGgVIgDgKQgmiKAbhtIAQgwQAOghAUgfQAphPBJhEQCpicDvAAQApAAAoAFQBQhpCSgyQD2hGCWCdQCVCdAcEOQAcDchBDBQhACwheCHIgXAhQADAJAGAIQAWAqgbA3QgaA2g6ABQglAEgXgUIgCABQg5AyhAAoIgPgCQgDAZACAfQAMAwgCA2QgCAygzAZQgYAMgWAAQgVAAgTgMg");
	var mask_2_graphics_42 = new cjs.Graphics().p("ABiM8QgygcAEg0QADg1gVghQg3hKgoAqQgpAqgjAHQgjAGgWgdQgTgUACgdQgSgOgLgUQgTghgygCQgzgBgcAhQgiAhhJgIQhjgIgUhZQgShOAthGIAJgOQgNgeAGg1IgXgnQgdgdgLgiIgKgWIgCgEIgJgWQgNgigJghQgXg5gHhBQgQhjAXhUIACgEQAjicCBh3QCyikD7AAQAfAAAeACQBHhGBxgmQD0hECbCYQCaCZAkEFQAkDVg8C8Qg7CqhZCDIgTAbQABAWANATQAWApgbA3QgbA3g7AAQgsAFgYgaIgHgGQgyAqg4AjIgngHIAAAQQAMAugCAzQgBAvgzAYQgYAMgWAAQgVAAgUgMg");
	var mask_2_graphics_43 = new cjs.Graphics().p("ABgMKQgygbAEgxQADgygVgfQgJgMgJgIIgRgDIgJAJQgpAsghAHQgtAMgqgMQgygSgVgmQgUgggzgDQg1gBgdAiQgiAhhMgIQhmgIgVhZQgShPAuhGQAUgbAFghQgLgSgBgdQgmgigNgoIgKgXIgCgFIgJgWQgOgkgKgjQgkhcAAhqQAAjzC6isQC7isEHAAQAbAAAbACQA2glBKgZQDyhBCgCTQCfCTAtD9QArDOg2C2Qg0ChhSB9IgKAWIAAgCIAAACQgMAmAWAfQAYAqgcA3QgdA2g9ABQgsAEgagaQgMgLgMgEQgoAggsAbIgXgEQgOAEgRgBQAEAdgBAeQgBAsgzAXQgYALgWAAQgWAAgTgLg");
	var mask_2_graphics_44 = new cjs.Graphics().p("ABfLXQgygZAEgvQACgbgHgWQggAHgTAYQgrArghAHQgvANgrgNQg0gRgVgmQgVghg0gCQg4gBgdAhQgkAhhOgIQhpgIgVhZQgThOAwhGQAegpgCg1QgggggNgmIgKgYIgCgFIgKgYQgOglgKgkQgmhhAAhvQAAj+DDi0QDDi0EUAAQAnAAAkAEQAXgKAZgIQDwg/CkCOQCkCOA0D1QAxDGgxCvQgjB0g0BgQgIAvgcA6IAAgCIAAADQgMAmAWAfQAZApgdA3QgeA3g+AAQgvAFgZgaQgngkgpAnIgCABIgDAEQgRAXggAIQgVAFgagCQgJAdgoARQgYAKgWAAQgWAAgTgKg");
	var mask_2_graphics_45 = new cjs.Graphics().p("AivLBQg1gSgXgmQgVggg2gDQg5gBgfAiQgkAhhQgIQhtgIgVhZQgUhPAxhGQAigtgFg7QgOgUgHgWIgLgZIgCgFIgKgZQgPgngLglQgnhmAAh0QAAkJDLi8QDMi8EgAAQBSAABNAQQCzgRCGBuQCnCJA8DrQA2C/gsCpQgaBaglBOQAFBBgtBdIgBgCIABACQgNAmAYAfQAYAqgdA3QgeA2hBABQgwAEgagaQgogkgqAnIgCACIgEADQgRAXghAJQghAIgwgMQgdgIgSgOQglAKgfgQQgigPgKgaQgiAHgUAYQgsAsgiAHQgYAGgXAAQgXAAgWgGg");
	var mask_2_graphics_46 = new cjs.Graphics().p("AjDLMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAyhBghhdIgKgaQgQgpgLgnQgphqAAh5QAAkUDUjEQDUjEEsAAQEtAADUDEIAgAfIAKAKIAcAfIABABQBMBbAhBrQAgBfAABrQAAApgFAmIAAAGQgLDjh0DQIAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");
	var mask_2_graphics_47 = new cjs.Graphics().p("AjDLMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAyhBghhdIgKgaQgQgpgLgnQgphqAAh5QAAkUDUjEQDUjEEsAAQELAADECaQAZASAXAVQAnAjAhAoIAAAAIAEAFIABACQBKBYAfBoQAgBfAABrQAAAbgCAaIgCAxQgFBtghBfQggBtg6BpIAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");
	var mask_2_graphics_48 = new cjs.Graphics().p("AjGKwQg4gRgXgjQgNgSgagIQgyAHgegJQgfAHgUATQgnAfhTgIQhygHgXhSQgUhJAzhAQA0g8gihWIACAEIgBgDIgBgDIgCgFIgLgZQgPgngMgnQgqhmAAh1QAAkMDYi+QDYi+EyAAQBwAABlAaQCtAoCJBzQCFBxA0CnQAQAzAIA5QAHAxACA1QAAAngDAlQgGCOg7B3QgcBJgqBHQgNAiAZAdQAaAmgfAzQggAyhEABQgxAEgcgYQgpghgtAkIgCABIgEADQgSAVgiAIQgjAIgygLQgzgMgSgcQgTgfg+AGQgvADgaAbQguAogkAHQgZAFgYAAQgYAAgXgFg");
	var mask_2_graphics_49 = new cjs.Graphics().p("ACSKIQgLgYAFgUIgEgFQgPgXgtgBIgnADQgkAFgVAVQgvAkgkAGQg0ALgvgLQg4gOgYghIgDgDIgXAHQhdAOgjgeIgCgBQgJAFgIAGQgmAchWgHQhzgGgXhMQgVhDA0g7QAwgygYhHIgFgIIgMgfIgFgLQgQgmgMglQgrhkAAhxQAAkEDci4QBbhNBqgsIBAgYIBAgSQBogaB0AAQEyAADYCuQDYCuAIExQAAAmgCAkQgICfhOCAIgHANQgRAjgUAjIgIAPQAJAegMAVQAEAHAGAHQAbAjggAvQggAuhEAAQgzAEgcgWQglgagnAVQgEAQgHAOQgYAlhHANQgPADgOAAQgzAAgfgjg");
	var mask_2_graphics_50 = new cjs.Graphics().p("ACXKGQgSgpAVgcIAEgHQgOgIgHgKQgIgJgOgGQg5AHg8AAQgLAPgTALIgWAHQgcAQgYADQg0AKgwgKQgqgKgYgSQgTALgfAHQhgANglghIgPgLQgnAVhQgGQh1gGgXhFQgVg9A1g2QAqgogOg3QgLgNgJgPIgOgkQgQgqgLgqQgoheAAhrQAAjACBiVQAvg7BCgyQDeiqE4AAQE6AADeCqQDdCqAIEqQABAngDAiQgICbhQB9IgJAPIAAACIgCABQgHALgKALQgOAagQAaIAEAHQASAlgGAZQAOAdgbAjQggArhGAAQgzAEgdgVQghgVgiANIAAAMIABACQADAjgQAhQgaAnhJAMQgNACgMAAQg4AAgigng");
	var mask_2_graphics_51 = new cjs.Graphics().p("ACVKzQgSgvAUgfQAMgYACgVQgYgIgLgPIgGgGQg4AIg8ABIAAACQgIAdgfAWQgsATghgPQgLgFgIgJQglAEgigHQgbgHgTgKQgRAcg1ANQheANglgnIgfgcQgjAJg1gEQhwgGgWhEQgUg8Ayg1QAfgeAAglQgDgMgHgNQgMgPgKgSIgOglQgYhJgNhFQgVhJAAhTQAAhyAphkQA2iWCEhuQDXiyEtAAQEwAADWCyQDWCyAIE5QAAApgCAkQgICihNCEIgJAQIAAABIgCACQgKASgSAQQgHAPgKAQQABAYAMASQApBihWAaIgGABQgZAMgjAAQgyAEgbgUQgQgLgQgCQgaAYAEAnIAAACQADApgPAlQgYArhIAMIgVABQg5AAgjgug");
	var mask_2_graphics_52 = new cjs.Graphics().p("ACULhQgSg0ATgiQAYg7gUgkQgQgHgJgKQg1AKg4ACQgHALgDANQgIAigdAXQgsAUgfgRQgagPgGgiIgBgFQgaABgagFQgUgFgPgHQgGAJgEALQgNAlg7AQQhbAMglgsIgigmIgQgQQgYADgfgDQhDgDgigcQgPgIgIgQQgXguAXgtIAkg+QAMgfgPgnQgKgPgJgQIgNgnQgdhcgLhXQgOhFADhPQAHknDPi7QDPi6EjAAQElAADPC6QDPC7AHFIQAAAqgCAnQgHCqhLCJIgJARIAAABIgBACQgKAUgSARQgRA8AWApQApBuhSAcQgeAKg8glQgqgbgnAdQgaAbADAuIABADQAEAtgOApQgXAwhGAMIgRABQg7AAgkg3g");
	var mask_2_graphics_53 = new cjs.Graphics().p("ACWMSQgTg7ATgmQAhhgg+gsIgTgGQgeAFgfACQgYAWgFAfQgIAlgcAZQgqAVgfgTQgagRgGgnQgBgjgSgKQgOgBgOgDQgkAEgMArQgMAog6ARQhZALgkgyIgigrQgaghgagMIgFAAQgUgCgSgDIgHACQhDALgVguQgXg0AVgyIAihDQAMgjgPgrIgKgUIgNgpQgchggLhbQgNhIAChTQAIk0DHjDQDIjDEYAAQEbAADHDDQDHDDAHFXQAAAsgCApQgHCxhICQIgIARIgBABIAAACIgGAMIgEAJQghBbAcA6QAqB8hRAdQgcALg7grQgpgfgmAgQgZAdAEAzIABADQAEAzgNAtQgWA1hEAMIgOAAQg7AAgkg+g");
	var mask_2_graphics_54 = new cjs.Graphics().p("ACYL3QgTg5ATglQAihdhAgqIgSgGQgfAFggACQgXAVgGAeQgIAjgcAZQgrAVgggUQgZgQgHglQgBghgRgLQgPAAgPgDIgBgBQgjAFgMAoQgNAog6APQhaAMglgwIgigqQgagggbgMIgFAAQgVgBgSgEIgHACQhEALgVgsQgYgyAWgxIAjhBQALghgOgpIgMgUIgMgoQgdhdgLhXQgNhGAChPQAIkqDKi8QDKi9EdAAQEeAADLC9QDKC8AHFLQAAAqgCAoQgHCqhJCLIgJARIAAACIgBABIgFAJIgFALQgiBYAdA4QArB4hSAcQgdAKg7gpQgrgegmAfQgZAcAEAyIABACQAEAxgNAsQgWAzhFALIgOABQg8AAglg8g");
	var mask_2_graphics_55 = new cjs.Graphics().p("ACaLbQgTg2ATgkQAihZhAgpIgUgGQgeAEggACIAAABQgYAUgGAdQgIAigdAYQgrAUgggTQgagPgHgkQgBghgSgKQgPgBgOgCQglAEgNAnQgMAmg7APQhcALglguIgjgoQgbgggbgLIgFAAQgVgBgRgDIgIACQhFAKgWgrQgYgwAXgvIAjg+QAMghgQgpIgKgRIgNgnQgdhZgLhUQgOhDADhNQAHkfDNi2QDOi1EgAAQEjAADNC1QDNC2AHE/QAAApgCAmQgHCkhKCGIgJAQIAAACIgBABIgHALIgDAJQgjBUAeA3QArBzhTAbQgeAKg8goQgrgdgmAeQgaAbAEAwIABACQAEAvgNArQgXAxhGALIgPAAQg7AAgmg6g");
	var mask_2_graphics_84 = new cjs.Graphics().p("ACaLbQgTg2ATgkQAihZhAgpIgUgGQgeAEggACIAAABQgYAUgGAdQgIAigdAYQgrAUgggTQgagPgHgkQgBghgSgKQgPgBgOgCQglAEgNAnQgMAmg7APQhcALglguIgjgoQgbgggbgLIgFAAQgVgBgRgDIgIACQhFAKgWgrQgYgwAXgvIAjg+QAMghgQgpIgKgRIgNgnQgdhZgLhUQgOhDADhNQAHkfDNi2QDOi1EgAAQEjAADNC1QDNC2AHE/QAAApgCAmQgHCkhKCGIgJAQIAAACIgBABIgHALIgDAJQgjBUAeA3QArBzhTAbQgeAKg8goQgrgdgmAeQgaAbAEAwIABACQAEAvgNArQgXAxhGALIgPAAQg7AAgmg6g");
	var mask_2_graphics_85 = new cjs.Graphics().p("ACaLbQgTg2ATgkQAihZhAgpIgUgGQgeAEggACIAAABQgYAUgGAdQgIAigdAYQgrAUgggTQgagPgHgkQgBghgSgKQgPgBgOgCQglAEgNAnQgMAmg7APQhcALglguIgjgoQgbgggbgLIgFAAQgVgBgRgDIgIACQhFAKgWgrQgYgwAXgvIAjg+QAMghgQgpIgKgRIgNgnQgdhZgLhUQgOhDADhNQAHkfDNi2QDOi1EgAAQEjAADNC1QDNC2AHE/QAAApgCAmQgHCkhKCGIgJAQIAAACIgBABIgHALIgDAJQgjBUAeA3QArBzhTAbQgeAKg8goQgrgdgmAeQgaAbAEAwIABACQAEAvgNArQgXAxhGALIgPAAQg7AAgmg6g");
	var mask_2_graphics_86 = new cjs.Graphics().p("ACVLRQgTgzAUgjQAehJgrgmIgHgGQgzAKg3ACQgLAOgDARQgIAggdAXQgrAUgggRQgagPgGgiIgCgOQgaAAgYgFQgQgEgNgFQgLAKgFARQgNAkg7AQQhaALglgsIgiglQgLgNgMgJQgUABgZgCQgygCgfgQQgggFgNgZQgXguAXgtIAjg8QAMghgQgnIgPgZIgNgnQgdhZgLhUQgOhDADhNQAHkfDNi2QDOi1EgAAQEjAADNC1QDNC2AHE/QAAApgCAmQgHCkhKCGIgJAQIAAACIgBABQgJARgPAQQgWBBAZAsQApBthSAbQgdAKg7glQgrgbgmAcQgaAaAEAuIAAACQAEAtgNApQgXAvhGALIgSABQg5AAgkg2g");
	var mask_2_graphics_87 = new cjs.Graphics().p("ACQLHQgRgxATghQARgngFgcQgYgJgLgOIgBgCQg1AJg4ABIgFAOQgIAfgdAWQgqAUgggQQgUgKgIgYQgeACgcgGQgWgFgQgJIgDAHQgOAjg6APQhaANgkgpIgigjIgFgGQgeAFgpgDQhqgGgVhDQgUg6Axg0QASgSAHgVQADgZgMgcQgMgRgKgSIgNgmQgdhagLhUIgGgjIgFhBIgBgcQAAg2AIgyQAhjkCsiYQDOi2EgAAQEjAADNC2QDNC1AHE/QAAApgCAlQgHCmhKCGIgJAQIAAABIgBACQgLAUgTASIgGAMQgIAqASAeQAoBnhSAbQgXAHgogSIgIAAQgvAEgagUIgGgEQgJAEgJAGQgaAaAEArIAAACQADAqgOAoQgXAshFANIgSABQg5AAgjgyg");
	var mask_2_graphics_88 = new cjs.Graphics().p("ACOK9QgRguAUgfQAJgTADgRQgWgJgKgOQgEgGgFgEQg1AIg4ABQgIAdgcAUQgqATgfgOIgLgHQgnAGgmgIQgbgHgTgMQgSAYgtAMQhaAOgjgmIgagbQgjAMg4gEQhrgGgWhHQgTg+Awg3QAigjgFgtIgIgQIgBgCQgKgPgIgPIgNgnQgVhAgLg9QgahUAAheQAAiMA4h2QA2iABthhQDOi1EgAAQEjAADNC1QDNC2AHE/QAAApgCAlQgHClhKCGIgJAQIAAACIgBABQgJASgQAQQgIARgLATQACATAJAPQAeBKgpAiQgeAqhAABQgvADgagUQgVgPgVABQgOAVACAgIABACQADAogPAlQgXAshEAMQgLABgLAAQg1AAgigtg");
	var mask_2_graphics_89 = new cjs.Graphics().p("ACMKzQgQgrATgfIAEgHQgNgIgIgLQgHgLgOgGQgzAHg2AAQgKAQgSANQgNAGgLADQgZAPgWADQgwALgsgLQglgKgXgTQgRAMgdAHQhZAOgigjIgOgNQgkAXhLgGQhsgHgWhKQgThBAxg6QAogsgOg7IgSgdIgNgmQgNgsgKgqQgohnAAh0QAAjRB+iiQAqg7A5gzQDNi2EhAAQEiAADNC2QDNC1AHE/QABApgDAlQgHCmhKCGIgIAQIgBABIgBACQgGANgLAMIgKAUIgQAhIAFAJQARAqgHAcQALAdgYAkQgeAthBABQgvAEgbgWQgdgWgfAMIAAAOIAAACQADAmgOAkQgYAohEAOQgLACgLAAQg1AAgggqg");
	var mask_2_graphics_90 = new cjs.Graphics().p("ACNKoQgMgeAHgXIgHgKQgMgSgegFQghAEgjABQgWAHgRAQQgsAmgiAGQgxALgsgLQg0gOgXggQgLAFgPAEQhYAPghggIgGgFIgMAJQgkAchRgGQhtgHgWhOQgThFAxg9QAsgygVhHIgIgNIgNgnIgDgIQgOgmgLgmQgohnAAh2QAAkPDQjAQAsgpAxghIAZgQIBXgtQCNhACqAAQEiAADNC1QDNC2AHE/QABAogDAmQgHClhKCGIgIAQIgBACIgBABIgGALIgUAqIgNAbQANAmgNAZIAFAGQAZAlgdAwQgfAvhBABQgwAEgbgXQgfgZgiARQgCAYgKAXQgYAnhDAOQgOACgMAAQgxAAgegmg");
	var mask_2_graphics_91 = new cjs.Graphics().p("ACJKeQgFgNgBgMIgBgBQgSgfg9AGQgtADgZAbQgtAngjAHQgwALgtgLQg2gQgXgjQgFgIgJgGIgHACQhQAPgjgZQgQAHgNANQglAehRgHQhugIgWhRQgUhIAyhAQAyg7ghhWIACAFIAAgCIgEgIIgCgFIgKgaQgPgogMgnQgohoAAh3QAAkRDRjBQCgiUDTgiICHgMIBVAEQD1ATC0CfQDNC1AHE/QAAAogCAmQgHCkhJCGQgTAugXAtIgKATQABARgHANQABATAOARQAZAmgeAzQgeAyhCAAQgwAEgbgYQgoghgrAkIgCACIgEADQgJALgPAIQgYAQgnAIQgPADgNAAQgvAAgegig");
	var mask_2_graphics_92 = new cjs.Graphics().p("AjBLCQg2gQgWglQgRgYgkgHQgdADgUgEQgoAFgZAZQglAfhSgHQhugIgXhVQgUhLAyhDQAxg8gdhWIgDgGIgCgFIgKgaQgQgogLgnQgphpAAh4QAAkTDTjCQDTjDEqAAQCMAAB6ArQCJAtBwBjQCAByAzCoIAAACQATA8AIBEQAFArABAuQAAAogCAmQgGCRg6B4QgWA9gfA7IgOAdIAAgCIAAAAIAAACQgNAlAYAdQAZAogeA1QgfA0hCAAQgwAFgcgZQgogjgsAmIgBABIgEADQgSAXghAIQgiAHgxgLQgygMgRgdQgSggg9AGQguADgZAcQgtAqgjAGQgZAGgXAAQgYAAgXgGg");
	var mask_2_graphics_93 = new cjs.Graphics().p("AjDLMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAyhBghhdIgKgaQgQgpgLgnQgphqAAh5QAAkUDUjEQDUjEEsAAQELAADECaQAZASAXAVQAnAjAhAoIAAAAIAEAFIABACQBKBYAfBoQAgBfAABrQAAAbgCAaIgCAxQgFBtghBfQggBtg6BpIAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");
	var mask_2_graphics_94 = new cjs.Graphics().p("AjDLMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAyhBghhdIgKgaQgQgpgLgnQgphqAAh5QAAkUDUjEQDUjEEsAAQELAADECaQAZASAXAVQAnAjAhAoIAAAAIAEAFIABACQBKBYAfBoQAgBfAABrQAAAbgCAaIgCAxQgFBtghBfQggBtg6BpIAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");
	var mask_2_graphics_95 = new cjs.Graphics().p("AjFLMQg3gRgXgmQgWghg3gCQg7gBgfAhQgmAhhSgIQhwgIgWhZQgUhOAyhGQAogzgMhGQgPgZgKgeIgCgEIAAgBIgCgFIgBgFQgIgbgFgeIgGgUQgohoAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBQAEBaQAZDHhNDiQgOAVgOATIgLAPIgKANQgOAegRAdIAAgCIAAADQgNAmAYAfQAaApgfA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXghAIQgiAIgygMQgygMgRgfQgTghg9AGQguADgaAeQgtArgjAHQgZAGgYAAQgXAAgXgGg");
	var mask_2_graphics_96 = new cjs.Graphics().p("AjDK/Qg2gRgXgmQgWggg4gDQg6gBggAiQglAghTgIQhvgIgXhYQgUhOAzhFQAhgqgDg3QgSgdgMgjIgCgEIAAAAIgCgFIgBgFQgIgbgFgeIgGgSQgohoAAh2QAAkSDUjDQDVjCEsAAQEsAADUDCIAgAfIAKAKIAcAfIABABQBNBaAhBqQAaBQAFBYQAZDGhNDgQgOAVgOATIgLAOIgJAMQgPAegRAeIAAgCIAAADIAAABQADAPgCAPQAEAMAJALQAZApgeA3QgfA2hDAAQgxAFgbgaQgpgkgsAnIgBABIgEAEQgSAXgiAIQgiAIgxgMQgygMgSgeQgFgJgHgGQgqABgtgCIgWgCQgRAIgNAPQgtArgkAHQgYAGgYAAQgYAAgXgGg");
	var mask_2_graphics_97 = new cjs.Graphics().p("AjAKyQg3gRgXgmQgWggg3gCQg7gBgfAhQgmAghSgIQhwgHgWhYQgUhNAyhFQAcgjACgrQgTgegNglIgCgEIAAAAIgCgFIgBgFQgIgbgFgdIgGgTQgohnAAh1QAAkQDUjAQDVjBEsAAQEsAADUDBQARAPAPAQIAKAJIAcAeIABACQBNBZAhBpQAaBPAFBYQAZDEhNDeQgOAVgOASIgLAPIgKANIgXAsQAKAagCAcQAaApgfA2QgfA2hCAAQglAEgYgOQg2AIgugQIgCACIgEADQgSAXghAIQgiAIgygMQgggIgTgPIgMAAQg7gCgWgRIgZgBIgogDIgKAJQgtArgjAHQgZAGgYAAQgXAAgXgGg");
	var mask_2_graphics_98 = new cjs.Graphics().p("AFaKfIgggOQgMAGgOAEQgfAHgugKQgZASgrABQg/gCgVgVQgWgUgTgIIgigCIgDABQgqAmgiAHQgxAMgugMIgNgFQg3gHgigqQgGgOgIgLIgDgEQgQgFgUgBQg6gBggAhQglAghTgIQhvgHgXhXQgUhNAyhDQAEgOAGgKIAAgBIACgCQALgSAFgOIABgFQgUgegNglIgCgEIAAgBIgCgEIgBgFQgIgbgFgdIgGgTQgohmAAhzQAAkODUi/QDVi/EsAAQEsAADUC/IAgAfIAKAKIAcAdIABACQBNBYAhBoQAaBOAFBXQAZDDhNDcQgOAVgOASIgLAPIgKAMIgPAdQAUArgGAwQACAbgSAgQgOAYgWANQgbAWgpAQQgrAOgnAAQgmAAgigOg");
	var mask_2_graphics_99 = new cjs.Graphics().p("AFjKyQhBgggfACQgRAAgYAWQgaAVgtABQhBgDgWgXQgtgvgjAPIgwAWQg6AghEgIQhCgEgmg1QgHgQgHgMIgIgLQgOgTgRgGQgOgCgZACIgHABQgUAIgPAPQgmAhhSgIQhwgIgWhXQgPg5AXg0QADghALgVIABgBIABgCQAPgdAGgTQgNgXgJgbIgCgEIgCgFIgBgFQgIgbgFgdIgGgTQgohnAAh1QAAkQDUjAQDVjBEsAAQEsAADUDBQARAPAPAQIAKAJIAcAeIABACQBNBZAhBpQAaBPAFBYQAZDEhNDeQgOAVgOASIgLAPIgKAMIgHAPQAjBFgTBUQgVBLhhAqQgsAQgnAAQgoAAgigQg");
	var mask_2_graphics_100 = new cjs.Graphics().p("AFrLFQhCgjggACQgRAAgZAYQgbAXguACQhCgDgWgaQgvg1gjARIgxAZQg8AjhGgIQhDgFgng7QgHgSgIgOIgHgLQgPgWgRgHQgOgCgaACIgUADIgcAEQglAThCgGQhvgIgXhYQgGgXABgWIgCgHQgDgLAAgNQABg1AOgdIAAgCIACgCQAUgoAEgYIgKgaIgCgDIAAgBIgCgFIgBgFQgIgbgFgeIgGgSQgohoAAh2QAAkSDUjDQDVjCEsAAQEsAADUDCIAgAgIAKAJIAcAfIABACQBNBZAhBqQAaBQAFBYQAZDGhNDgQgOAVgOATIgLAOIgIALQAAAGADAFIABACQAlBOgUBfQgVBVhjAuQgtASgoAAQgpAAgjgSg");
	var mask_2_graphics_101 = new cjs.Graphics().p("AF0LZQhDgnghACQgRAAgZAbQgcAagvACQhDgEgXgdQgwg6gkASIgyAcQg+AnhHgJQhFgFgnhCQgHgUgIgPIgIgNQgPgYgSgHQgOgDgbADIgUACQgXAEgdAHQhTAThGgwQgrgggWhBQgFgOgDgPQgDgNAAgOQABg6AOghIABgBIABgDQAcg8gCgbIAAgBIgCgFIgBgFQgIgbgFgeIgGgUQgohoAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBQAEBaQAZDHhNDiQgOAVgOATQgIASAHASIABACQAmBXgUBpQgWBehlAzQguAUgpAAQgpAAglgUg");
	var mask_2_graphics_134 = new cjs.Graphics().p("AF0LZQhDgnghACQgRAAgZAbQgcAagvACQhDgEgXgdQgwg6gkASIgyAcQg+AnhHgJQhFgFgnhCQgHgUgIgPIgIgNQgPgYgSgHQgOgDgbADIgUACQgXAEgdAHQhTAThGgwQgrgggWhBQgFgOgDgPQgDgNAAgOQABg6AOghIABgBIABgDQAcg8gCgbIAAgBIgCgFIgBgFQgIgbgFgeIgGgUQgohoAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBQAEBaQAZDHhNDiQgOAVgOATQgIASAHASIABACQAmBXgUBpQgWBehlAzQguAUgpAAQgpAAglgUg");
	var mask_2_graphics_139 = new cjs.Graphics().p("AF0LZQhDgnghACQgRAAgZAbQgcAagvACQhDgEgXgdQgwg6gkASIgyAcQg+AnhHgJQhFgFgnhCQgHgUgIgPIgIgNQgPgYgSgHQgOgDgbADIgUACQgXAEgdAHQhTAThGgwQgrgggWhBQgFgOgDgPQgDgNAAgOQABg6AOghIABgBIABgDQAcg8gCgbIAAgBIgCgFIgBgFQgIgbgFgeIgGgUQgohoAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBQAEBaQAZDHhNDiQgOAVgOATQgIASAHASIABACQAmBXgUBpQgWBehlAzQguAUgpAAQgpAAglgUg");
	var mask_2_graphics_140 = new cjs.Graphics().p("AFuLPQhDglggACQgRAAgZAaQgbAYguACQhCgEgXgbQgvg2gkARIgxAaQg8AkhGgIQhEgFgng+QgHgSgIgOIgHgMQgPgXgRgGQgPgDgaADIgTACQgXADgdAHQg2AMgwgQQhZgPgUhPQgDgNgCgMIgGgVQgDgMAAgOQABg2AOgeIAAgCIACgCQAVgrAEgaIgIgVIgCgEIAAAAIgCgFIgBgFQgIgbgFgeIgGgUQgohpAAh2QAAkVDUjEQDVjEEsAAQEsAADUDEIAgAgIAKAKIAcAeIABACQBNBaAhBrQAbBRAEBZQAZDIhNDiQgOAVgOATIgLAPIgEAFQAAAIADAJIABACQAmBRgVBiQgVBXhjAwQguATgoAAQgpAAgjgTg");
	var mask_2_graphics_141 = new cjs.Graphics().p("AFoLFQhBgiggABQgRAAgYAYQgbAWguACQhAgDgXgZQgugzgjAQIgwAYQg8AihEgIQhDgEgmg5QgHgRgIgNIgHgMQgPgVgRgGQgOgCgaACIgTACIgMACIgGAGQglAhhTgIQhvgIgXhZQgLgsALgpQABgxAOgcIAAgCIACgCQARggAFgWQgKgTgHgWIgCgEIAAgBIgCgFIgBgFQgIgbgFgeIgGgTQgohpAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBRAEBZQAZDHhNDiQgOAVgOATIgLAPIgKANIgDAGIACAEIABACQAlBLgUBbQgVBRhiAtQgsARgoAAQgoAAgjgRg");
	var mask_2_graphics_142 = new cjs.Graphics().p("AFhK7QhAgggfACQgRAAgXAWQgaAUguACQg/gDgWgXQgtgvgjAPIgwAWQg6AfhEgHQhBgEgmg1QgGgPgIgMIgHgLQgPgTgRgGIgGAAQgyACgcAeQglAhhTgIQhvgIgXhZQgQhBAfg6QADgaAJgSIABgBIABgCQAPgaAGgTQgQgagKgfIgCgEIAAAAIgCgFIgBgFQgIgbgFgeIgGgUQgohpAAh2QAAkVDUjEQDVjEEsAAQEsAADUDEIAgAgIAKAKIAcAeIABACQBNBaAhBrQAbBRAEBZQAZDIhNDiQgOAVgOATIgLAPIgKANIgJARQAiBDgTBSQgVBLhgApQgsAQgnAAQgnAAgjgQg");
	var mask_2_graphics_143 = new cjs.Graphics().p("AFbKxIgigPQgLAGgOAEQgfAHgrgJQgaASgsACQg/gDgVgVQgXgWgVgHIgcgCIgHACIgEACQgoAjggAHQgxAMgugMIgHgDQg7gGgjgtQgGgOgIgLIgEgGQgPgFgVgBQg6gBggAiQglAhhTgIQhvgIgXhZQgUhPAzhGIACgCQADgLAFgJIAAgBIACgCQAIgPAGgNIACgLQgUgfgNgmIgCgEIAAgBIgCgFIgBgFQgIgbgFgeIgGgUQgohoAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBQAEBaQAZDHhNDiQgOAVgOATIgLAPIgKANIgOAdQAVAwgIA3QgBAOgFAOIgCAFQgDAJgFAJQgIAMgKAMQgeAgg3AWQgrAOgmAAQgnAAgigOg");
	var mask_2_graphics_144 = new cjs.Graphics().p("AjAK8Qg2gRgXgmQgWghg4gCQg6gBggAhQglAhhTgIQhvgIgXhZQgUhOAzhGQAagiAEgqQgUgfgNglIgCgEIAAgBIgCgFIgBgFQgIgbgFgeIgGgUQgohoAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBQAEBaQAZDHhNDiQgOAVgOATIgLAPIgKANIgUAoQAMAfgCAiQASAmgbAyQgfA3hDAAQgVACgRgEQhFAPg4gWIgFgCQgSAVggAIQgiAIgxgMQgVgFgPgJQgMACgOABQg9gCgWgUIgIgHIgRAAIgrgDIgGAGQgtArgkAHQgYAGgYAAQgYAAgXgGg");
	var mask_2_graphics_145 = new cjs.Graphics().p("AjBLCQg3gSgXgmQgWggg3gDQg7gBgfAiQgmAhhSgIQhwgIgWhZQgUhPAyhGQAfgnAAgxQgUgegMglIgCgEIAAAAIgCgFIgBgFQgIgbgFgeIgGgUQgohpAAh2QAAkVDUjEQDVjEEsAAQEsAADUDEIAgAgIAKAKIAcAeIABACQBNBaAhBrQAbBRAEBZQAZDIhNDiQgOAVgOATIgLAPIgKANIgaAzQAHAXgCAZIAFAIQAaAqgfA3QgfA2hCABQgxAEgcgaIgLgJQgaACgYgFQgMAFgLAKIgCACIgEADQgSAXghAJQgiAIgygMQgygNgRgeIgCgDQgVgBgQgDQgeAAgggCIghgCQgJAHgJAJQgtAsgjAHQgZAGgYAAQgXAAgXgGg");
	var mask_2_graphics_146 = new cjs.Graphics().p("AjDLHQg3gSgXgmQgWggg3gDQg7gBgfAiQgmAhhSgIQhwgIgWhZQgUhPAyhGQAjgsgFg6QgRgdgMgiIgCgEIAAgBIgCgFIgBgFQgIgbgFgeIgGgTQgohpAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBRAEBZQAZDHhNDiQgOAVgOATIgLAPIgKANQgOAegRAdIAAgBIAAACIgBACIgBAEQACAMgBAMQAEAPAKANQAaAqgfA3QgfA2hCABQgxAEgcgaQgogkgsAnIgCACIgEADQgSAXghAJQgiAIgygMQgygNgRgeQgGgLgLgHQgnABgrgCIgQgBQgVAIgQARQgtAsgjAHQgZAGgYAAQgXAAgXgGg");
	var mask_2_graphics_147 = new cjs.Graphics().p("AjFLMQg3gRgXgmQgWghg3gCQg7gBgfAhQgmAhhSgIQhwgIgWhZQgUhOAyhGQAogzgMhGQgPgZgKgeIgCgEIAAgBIgCgFIgBgFQgIgbgFgeIgGgUQgohoAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBQAEBaQASCUgmCkIgJAnQgYBOgwBAIAAAAQgOAegRAdIAAgCIAAADQgNAmAYAfQAaApgfA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXghAIQgiAIgygMQgygMgRgfQgTghg9AGQguADgaAeQgtArgjAHQgZAGgYAAQgXAAgXgGg");
	var mask_2_graphics_171 = new cjs.Graphics().p("AjFLMQg3gRgXgmQgWghg3gCQg7gBgfAhQgmAhhSgIQhwgIgWhZQgUhOAyhGQAogzgMhGQgPgZgKgeIgCgEIAAgBIgCgFIgBgFQgIgbgFgeIgGgUQgohoAAh3QAAkUDUjEQDVjEEsAAQEsAADUDEIAgAfIAKAKIAcAfIABABQBNBbAhBrQAbBQAEBaQASCUgmCkIgJAnQgYBOgwBAIAAAAQgOAegRAdIAAgCIAAADQgNAmAYAfQAaApgfA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXghAIQgiAIgygMQgygMgRgfQgTghg9AGQguADgaAeQgtArgjAHQgZAGgYAAQgXAAgXgGg");
	var mask_2_graphics_187 = new cjs.Graphics().p("AjILMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAug7gXhTIgXgpQgPgvgLgtQgGgVgGgVQgXhTAAhbQAAkUDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAXQATAQATASQDVDIAAEaQAAAhgDAeQgECdhICgIgLAPQgWA3geA1IAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");
	var mask_2_graphics_188 = new cjs.Graphics().p("AjKLKQg2gSgXgmQgVghg3gDQg7gBggAhQgmAhhSgJQhwgJgVhZQgUhPAzhFQAqg1gOhJQgPgYgNgaQgPgvgLgtQgGgVgGgVQgXhTAAhbQAAkUDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAYQATAPATASQDVDIAAEaQAAAhgDAeQgECdhICgIgLAQQgQAmgTAmIgJATQgFAPgGAOQgGAfATAaQAaAqggA3QgfA2hDAAQgwAEgcgbQgogkgsAnIgCABIgEAEQgSAWgiAJQghAHgygMQgygNgRgfQgSghg+AGQgLAAgKADIgCAAQgYATgaALQgtAqgiAGQgYAGgXAAQgZAAgYgHg");
	var mask_2_graphics_189 = new cjs.Graphics().p("AjMLIQg2gSgXgnQgVggg3gEQg7gCggAhQgmAghTgJQhvgKgUhaQgUhPA0hFIAMgPQACgjATghQAAgNgBgOIgBgCIgCgEQgPgZgOgbQgPgvgLgtQgGgUgGgVQgXhTAAhbQAAkVDUjEQDUjEEtAAQCDAABzAnIDPBtIAeAXQATAQATARQDVDIAAEbQAAAggDAeQgECdhICgIgLAQIgPAjQACAbgEAhQgMAugUAjIAGAJQAZAqggA3QgfA2hDgBQgxAEgbgbQgoglgsAnIgCABIgEAEQgSAWgiAIQgiAHgxgMQgggJgSgPIgKgEQg6gpg2AaQg3Aog6AGIgPAEQgXAGgWAAQgaAAgYgIg");
	var mask_2_graphics_190 = new cjs.Graphics().p("AjsK2QgogZgMghQgXgagwgDIgBAAQghAIgfADQgPAHgLALQgnAghSgKQhvgKgUhbQgQhHApg/QgEg/Arg5IgBgCQgPgZgOgbQgPgvgLgtQgGgUgGgWQgXhTAAhaQAAkVDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAYQATAPATASQDVDIAAEbQAAAggDAeQgECdhICgIgLAQIgDAHQAOAwgJBBQgOA0gXAmQAIAigYAqQghA2hCgCQgyAEgbgbIgDgDQgVgBgWgEIgYgBIgOAKIgBABIgFADQgSAWgiAIQgWAFgcgEQgkATg3gaQg7gsg4AbQg9ArhAADQhbgEgjgeg");
	var mask_2_graphics_191 = new cjs.Graphics().p("Aj6K+QgrgdgLglQgXgrgvANQgqAKglACQgmAfhSgLQhvgNgUhaQgKg0ATgvIgCgNQgLhSA7hFIgTgjQgPgvgLgtQgGgVgGgVQgXhTAAhbQAAkUDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAXQATAQATASQDVDIAAEaQAAAhgDAeQgECdhICgIgIAMQAfA8gPBdQgWBMgoAvQgEAKgGAKQgiA1hCgCIgRAAQgwAIgygMQgqgEggAGQgNAIgRADIgGABQgOAHgKAKQgqAmhEgjQg9gwg7AbQg/AshCABQhdgGgkggg");
	var mask_2_graphics_192 = new cjs.Graphics().p("AkHLGQgrgfgMgnQgXgugwANQg7ANgxgCQggAHgugGQhvgOgShaQgFgaACgYQgIgagDgfQgLhbBHhKIgKgTQgPgvgLgtQgGgUgGgVQgXhTAAhbQAAkVDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAYQATAQATARQDVDIAAEbQAAAggDAfQgECchICgIgEAGQAyBEgVB3QguCZhyAmQhFAVhGgSQhcgLgvAqQgrAmhGgmQg+g0g7AbQhCAuhEAAQhhgIgkgjg");
	var mask_2_graphics_193 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_196 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_198 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_203 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_207 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_212 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_216 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_221 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_222 = new cjs.Graphics().p("ACBLrQg/g4g+AcQhFAuhGgBQhjgKgkglQgsgigLgpQgXgxgyAOQiWAehVg7QgmgSgSgjQglgzgHhSQgJhjBPhPIACgBIgCgEQgPgvgKgtQgHgVgFgVQgYhSAAhcQAAkUDUjEQDVjEErAAQCEAABzAnIDQBsIAdAYQATAPATASQDVDIAAEaQAAAhgCAfQgFCchICgIgCADIACACQBHBLgcCPQgyCfh2AmQhHAUhHgUQhggNgwArQgWATgcAAQgeAAgkgVg");
	var mask_2_graphics_223 = new cjs.Graphics().p("AkJLJQgsgggLgoQgXgugwANQhTARg+gKQgTAAgVgDQhvgOgShaQgEgTABgSQgKgegEgkQgLhdBJhMIgIgQQgPgvgLgtQgGgUgGgWQgXhTAAhaQAAkVDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAYQATAPATASQDVDIAAEbQAAAggDAfQgECchICgIgDAEQA5BHgYB+QgvCbhzAnQhFAUhGgTQhegLguAqQgsAnhGgoQg+g0g9AcQhDAthEAAQhhgJgkgjg");
	var mask_2_graphics_224 = new cjs.Graphics().p("AkALCQgqgdgMgmQgXgtgvAOQgzALgqABQgmAQg/gJQhvgNgThaQgIgpAKgmIgEgdQgLhWBBhIIgQgdQgPgugLguQgGgUgGgVQgXhTAAhbQAAkVDUjDQDUjFEtABQCDgBBzAnIDPBtIAeAXQATAQATASQDVDHAAEbQAAAggDAeQgECehICfIgGAJQApBBgSBrQgrCWhwAmQhEAVhFgRQhbgKguApQgqAnhFgmQg9gyg8AcQhBAthCAAQhfgHglgig");
	var mask_2_graphics_225 = new cjs.Graphics().p("Aj1K9QgqgcgMglQgXgrgvAPQgrAKgmACIgBABQgnAghSgLQhvgMgUhaQgMg6AZg0IAAgBQgMhQA5hEIgXgqQgPgvgLgtQgGgVgGgVQgXhTAAhbQAAkUDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAXQATAQATASQDVDIAAEaQAAAhgDAeQgECdhICgIgJANQAbA5gOBYQgTBDghAtQgFARgLATQghA1hCgCQgYABgSgFQglACgkgIQgjgDgbAEQgRAOgZAFIgQACIgOALQgpAlhEgjQg8gvg6AcQg/AshCACQhcgFgkggg");
	var mask_2_graphics_226 = new cjs.Graphics().p("AjrK3QgpgagMgiQgVgZgtgEIgCABQgmAJghADQgNAHgKAKQgmAghSgKQhwgLgUhaQgQhIAqg/QgEg/Arg3IgCgDQgPgZgOgcQgPgvgLgtQgGgUgGgVQgXhTAAhbQAAkVDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAYQATAQATARQDVDIAAEbQAAAggDAeQgECdhICgIgLAQIgDAHQAPAwgJBDQgOA0gYAmQAIAigYAoQghA2hCgCQgxAEgbgbIgBgBQgWgBgXgFIgbgBIgMAJIgBABIgEADQgSAXgiAHQgVAEgagDQglAWg5gbQg7gtg4AcQg8ArhBADQhbgEgjgeg");
	var mask_2_graphics_227 = new cjs.Graphics().p("AjNLIQg3gSgWgnQgVghg3gEQg7gCghAhQglAghTgKQhwgKgUhaQgShPA0hEIAHgKQACgpAYgmIgBgTIgBgCIgCgFQgPgZgOgaQgPgvgLgtQgGgVgGgVQgXhTAAhbQAAkUDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAXQATAQATASQDVDIAAEaQAAAggDAfQgECdhICgIgLAPIgLAaQAFAigGArQgMAvgVAkIABACQAZArggA2QggA2hDgBQgxADgagaQgoglgtAmIgCABIgEAEQgSAWgiAIQgiAHgygNQgQgEgMgHQgOgDgPgHQg6gpg2AbQg7Aqg/ADIgXgBQgPACgOAAQgaAAgYgHg");
	var mask_2_graphics_228 = new cjs.Graphics().p("AjLLKQg3gTgWgmQgWghg3gDQg6gCggAhQgmAhhTgJQhvgKgVhZQgThPAzhFQAogzgKhEIgBgCQgPgZgOgaQgPgvgLgtQgGgVgGgVQgXhTAAhbQAAkUDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAXQATAQATASQDVDIAAEaQAAAhgDAeQgECdhICgIgLAPIgXAzIgCAeQgLAsgSAiQADANAJAMQAZAqggA2QgfA2hDAAQgwAEgcgaQgoglgtAmIgBACIgEADQgTAXghAIQgiAIgxgNQgygNgSgfIgDgFQgzgdgwAYQgoAegrALQgZASgWAEQgYAFgXAAQgYAAgYgGg");
	var mask_2_graphics_229 = new cjs.Graphics().p("AjJLLQg2gSgXgmQgWghg3gCQg7gCgfAiQgmAghSgIQhwgJgWhZQgUhOAzhGQAsg3gShNQgOgWgMgZQgPgvgLgtQgGgUgGgVQgXhTAAhbQAAkVDUjEQDUjEEtAAQCDAABzAnIDPBtIAeAXQATAQATARQDVDIAAEbQAAAggDAeQgECdhICgIgLAQQgSAtgYAsIgKAVIAAgBIAAABQgOAmAYAfQAaAqgfA3QgfA2hCAAQgxAFgcgaQgoglgsAnIgCACIgEADQgTAXghAIQgiAIgygMQgxgNgRgeQgTgig9AGQgSABgPAFQgPALgQAIIgIAHQgtAsgkAGQgYAHgYAAQgYAAgXgHg");
	var mask_2_graphics_230 = new cjs.Graphics().p("AjILMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAug7gXhTIgXgpQgPgvgLgtQgGgVgGgVQgXhTAAhbQAAkUDUjEQDUjEEtAAQCDAABzAnIDPBsIAeAXQATAQATASQDVDIAAEaQAAAhgDAeQgECdhICgIgLAPQgWA3geA1IAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");
	var mask_2_graphics_231 = new cjs.Graphics().p("AjDLMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAyhBghhdIgKgaQgQgpgLgnQgphqAAh5QAAkUDUjEQDUjEEsAAQEtAADUDEIAgAfIAKAKIAcAfIABABQBMBbAhBrQAgBfAABrQAAApgFAmIAAAGQgLDjh0DQIAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");
	var mask_2_graphics_232 = new cjs.Graphics().p("AiyLOQg2gRgXgmIAAAAQgWghg4gCIAAAAQg6gBggAhIAAAAQglAhhTgIIAAAAQhvgIgXhZIAAAAQgUhOAzhGIAAAAQAegnAAgxIAAAAQgGgMgFgNIAAAAIgLgaIAAAAIgCgFIAAAAIgLgaIAAAAQgPgpgMgnIAAAAQgRgtgKgwIAAAAQgRgDgHgKIAAAAIATgNIAAAAQgJg1AAg3IAAAAQAAkDC7i9IAAAAIAEgEIAAAAIAVgUIAAAAQDVjEErAAIAAAAQAdAAAcABIAAAAIAMABIAAAAIANABIAAAAQAmAEAkAHIAAAAIAdAGIAAAAQA9AOA5AZIAAAAQAzAVAuAdIAAAAQAVANAUAPIAAAAQAhAXAeAcIAAAAIAEAEIAAAAIAFAEIAAAAIAgAfIAAAAIAKAKIAAAAIADACIAAAAIAZAdIAAAAIABABIAAAAQAVAZARAZIAAAAQAwBGAYBOIAAAAQATA5AHA+IAAAAIABAJIAAAAQAEAkAAAmIAAAAQAAAogEAnIAAAAIgBAGIAAAAQgJC7hQCuIAAAAQgHAUgKAVIAAAAIALAfIAAAAQAAAFgCABIAAAAQgQAIgFAOIAAAAQgCgHAAgHIAAAAQAAAHACAHIAAAAQADAQALAOIAAAAQAZApgeA3IAAAAQgfA3hDAAIAAAAQgxAFgbgaIAAAAQgNgMgNgEIAAAAIgZAAIAAAAQgRAEgRAPIAAAAIgBABIAAAAIgEAEIAAAAQgSAXgiAIIAAAAQgiAIgxgMIAAAAQgugLgTgbIAAAAQgegCgcgHIAAAAIgCADIAAAAQgwAUhAADIAAAAQgiAdgdAFIAAAAQgZAGgXAAIAAAAQgYAAgXgGgABIJ7IAPgBIAAAAIgPABgABXJ6IAAAAIAAAAIAAAAgAJiHXIgBgDgALVh3IAEAAIAAAAIAAAJIAAAAIgDAAIAAAAIgBgJgAJNm0IgBgBIAAAAIgZgdIAAAAIAHAAIAAAAQA8AJgDBHIAAAAQgRgZgVgZgAoUnpIAEAAIAAAAIgEAEIAAAAIAAgEgAH9oFIAEAAIAAAAIAAAEIgEgEgAGVpUIATAKIAAAAIALAGIAAAAQAIADADAJIAAAAQgUgPgVgNgAC+qtQA6gHAZAcIAAAAIAYAFQAJACACALIAAAAQg5gZg9gOgABXq+QAygXAYAiIAAAAQgkgHgmgEgAA+rAQAEgHAAgMIAAAAQAEALAEAJIAAAAIgMgBg");
	var mask_2_graphics_233 = new cjs.Graphics().p("AhVLoQgFAAgDgCIAAAAIgVgMIAAAAQgbABgagHIAAAAQg2gRgXgmIAAAAQgWghg4gCIAAAAQg6gBggAhIAAAAQglAhhTgIIAAAAQhvgIgXhZIAAAAQgUhOAyhFIAAAAIABgBIAAAAQAVgcAGggIAAAAQgGAggVAcIAAAAIgBABIAAAAQABgdANgdIAAAAQAAgFgCgCIAAAAQgvgpAcghIAAAAIgIgTIAAAAQgPgogMgoIAAAAQgLgdgJgfIAAAAIgKABIAAAAIAAgFQgjACgNgRIAAAAQAagPAVgUIAAAAQgFgbgDgdIAAAAIgJACIAAAAQgLgcASgQIAAAAIAAgOIAAAAQAAhvAihiIAAAAQAIgWAJgVIAAAAQALgXAMgXIAAAAIAUgjIAAAAQAfgwAogtIAAAAQAWgYAZgXIAAAAIAKgIIAAAAQBFg+BOgqIAAAAQAjgSAkgOIAAAAIAsgQIAAAAQAmgMAogIIAAAAQAmgIAngEIAAAAQAqgEArAAIAAAAQEtAADUDEIAAAAIAgAgIAAAAIAKAKIAAAAIAcAeIAAAAIABACIAAAAQAxA6AgBBIAAAAQARAkAMAmIAAAAQAJAcAGAcIAAAAQAJAnAEApIAAAAQADAZAAAbIAAAAIAAAOIAAAAIAAgOIAAAAQAVAjgYAyIAAAAIAAAAIgBAIIAAAAIgBAHIAAAAQgICgg8CWIAAAAQgGAcgNAgIAAAAIgGARIAAAAIAFABIAAAAQAHAeARAuIAAAAQAAAEgCABIAAAAQgPAIgGAMIAAAAIgGgMIAAAAQgQgUAAgXIAAAAQAAAXAQAUIAAAAIAGAMIAAAAQAJAXgGAaIAAAAIABACIAAAAIgCAAIAAAAQgEAQgJARIAAAAQgfA3hDAAIAAAAIgRABIAAAAIgOAHIAAAAIgKABIAAAAQhUAKg0gVIAAAAQgQAOgZAGIAAAAQgfAHgsgJIAAAAIgQALIAAAAIgKAAIAAAAQhCgBgzgQIAAAAIgJAOIAAAAQg7AXhSAAIAAAAIgcAAgAFwKnIACgBIAAAAIgCABgAFyKlQAWgTAVAAIAAAAQgVAAgWATgAHDKiIAAAAIAAAAIAAAAgAGdKSIAAAAIAAAAIAAAAgAJtHdIgBgDgALKijQAOAYAVAQIAAAAQADACAFAAIAAAAIAAAKIAAAAQgFAAgCACIAAAAQgKAOgNAMIAAAAQgEgpgJgngAqwj1QABgSATgGIAAAAQgJAVgIAWIAAAAQgDgIAAgLgAJNmgIgBgCIAAAAIgcgeIAAAAIgKgKIAAAAIggggIAAAAQjUjEktAAIAAAAQgrAAgqAEIAAAAQA1gUA4gCIAAAAQAFgBAOgLIAAAAQAKgJAAgSIAAAAQAIAZAMANIAAAAIALgFIAAAAQBDgmAaA0IAAAAQBPgPAdAhIAAAAIAZAFQARAEgGAfIAAAAQA6AGA9AiIAAAAIAMAGIAAAAQATAJgHApIAAAAQAYAPAsgBIAAAAIAAAFQgHA2A2gGIAAAAIAKAAIAAAAQBDAJgJBXIAAAAIgBAJIAAAAQAPAlAEAoIAAAAQgghBgxg6gAqTlVIAAgJIAAAAIAiAAIAAAAIgUAjIAAAAIgOgagAoqn4IAKAAIAAAAQAZAGAWAAIAAAAIgKAIIAAAAQgZAXgWAYIAAAAIAAg9gAkXp6QgkAOgjASIAAAAQAQgxA3ARgAidqeQgoAIgmAMIAAAAQArgkAjAQg");
	var mask_2_graphics_234 = new cjs.Graphics().p("AhhMQQgFAAgDgCIAAAAQgbgRghgLIAAAAQgFAAgDgCIAAAAQgCgDAAgFIAAAAQg6gQgggnIAAAAQgfglghggIAAAAIgDAAIAAAAIAAACIAAAAIgdAEIAAAAQgYAIgRASIAAAAQglAhhTgIIAAAAQhvgIgXhZIAAAAQgKgnAIgkIAAAAQAHgmAagkIAAAAQAOgRAHgVIAAAAQgHAVgOARIAAAAQgaAkgHAmIAAAAQgGgqATgqIAAAAQAAgEgDgCIAAAAQg/g3BGgrIAAAAIgBgCIAAAAIgKgaIAAAAIgNgiIAAAAIgKAAIAEgRIAAAAIgIgdIAAAAIgKgbIAAAAQgQgIgcADIAAAAIAAgFQglACgMgRIAAAAQArgaAcgkIAAAAIgDgXIAAAAQgMgIgGgJIAAAAIgBgBIAAAAIAAAAIAAAAIAAAAQgLgSAIgVIAAAAQgHAAgKADIAAAAQgQgqAxgRIAAAAIAAAXIAAAAIAAgXIAAAAIACgkIAAAAIAFgnIAAAAIAHgpIAAAAQARhMAjhFIAAAAIAGgLIAAAAQASgiAXggIAAAAQAOgUAQgTIAAAAQAfglAmgjIAAAAQDUjEEsAAIAAAAQEsAADUDEIAAAAIAhAfIAAAAIAKAKIAAAAIAbAfIAAAAIABABIAAAAQBNBbAhBrIAAAAQAWBAAHBGIAAAAIAAABIAAAAQA/AtgkBKIAAAAIgBAAIgFAEIAAAAQAZBeg5AbIAAAAIgDAOIAAAAQAUAyghATIAAAAQgKArgPArIAAAAIgCAJIAAAAQgDAagKAeIAAAAQAaAegZA9IAAAAQAFAAAAABIAAAAQAHAfARAuIAAAAQAAAFgBABIAAAAQgnAUAVA1IAAAAQgFAAgDACIAAAAQg+A4hNAoIAAAAIgKAAIAAAAQhlANg3ghIAAAAIgKAAIAAAAQg0gCg4ApIAAAAIgKAAIAAAAQhIgBg3gSIAAAAQgFAAgDACIAAAAIgKAHIAAAAIgIALIAAAAQg8AZhTAAIAAAAIgegBgAEkLAIgBAAIAAAAIABAAgAEkLAQAPAAANgDIAAAAQgNADgPAAgADyK6IgBAAIAAAAIABAAgADwK5IgBAAIAAAAIABAAgAINKsIAAAAIAAAAIAAAAgAINKsIANgBIAAAAIgNABgAF6KYIABAAIAAAAIgBAAgAF7KXIABAAIAAAAIgBAAgAHQKXIAAgBIAAAAIAAABgAF9KWQAVgSAVAAIAAAAQgVAAgVASgAHPKVIgBAAIAAAAIABAAgAGnKEIAAAAIAAAAIAAAAgAlIJsIAFAAIAAAAIgFAAgAlCJrIAGAAIAAAAIgGAAgAk4JrIgCAAIAAAAIACAAgAk7JrIAAAAIAAAAIAAAAgAKCITQALATAAAWIAAAAQAAgWgLgTIAAAAQgQgUAAgXIAAAAQAAAXAQAUgAJ3HOIgBgCgALWhUQADAhAAAiIAAAAQAAgigDghIAAAAIAAAAgArJh0IgFAnIAAAAQgFgUAKgTgAK5jbQghhrhNhbIAAAAIgBgBIAAAAIgbgfIAAAAIgKgKIAAAAIghgfIAAAAQjUjEksAAIAAAAQksAAjUDEIAAAAQgmAjgfAlIAAAAIAAghIAAAAIAAgKIAAAAIAAhOIAAAAIAKAAIAAAAQBuAZAjhXIAAAAIAYgQQAKgHAQgDIAAAAQAPhHBKAeIAAAAQAEACAFAAIAAAAQBEhLA0ArIAAAAQADACAFAAIAAAAQBOgsBWgEIAAAAQAGgBAOgLIAAAAQAKgJAAgTIAAAAQALAiAQAMIAAAAIABABIAAAAQALAAAOgIIAAAAQBEgnAbA2IAAAAQBQgRAfAiIAAAAIAZAGQASAEgHAfIAAAAQA7AGA/AjIAAAAIALAGIAAAAQAUAKgGApIAAAAQAYAPAsAAIAAAAIAAAFQgHA4A4gHIAAAAIAKAAIAAAAQBEAKgJBYIAAAAIAAAJIAAAAQAXA9gEBDIAAAAIAAAKIAAAAQAUATAOAUIAAAAIgEAAQAPAwAjAcIAAAAQADACAFAAIAAAAIAAAKIAAAAQgFAAgCACIAAAAQgUAdgeARIAAAAQgHhGgWhAgAq5jlQACgCgKgNIAAAAQgJgMAAgTIAAAAQABgeA7ADIAAAAQgjBFgRBMIAAAAQgegjAnglgAqtl1IAAgJIAAAAQAnAAAnADIAAAAQgXAggSAiIAAAAQgVgcgQggg");
	var mask_2_graphics_235 = new cjs.Graphics().p("AhtM5QgFAAgEgCIAAAAQgbgRgigLIAAAAQgFAAgCgDIAAAAQgDgCAAgFIAAAAQg7gRghgoIAAAAQghgngjgiIAAAAIgBAFIAAAAQgpADgcAMIAAAAQAAAFgBAAIAAAAQhDAMgqgRIAAAAQgFAAgEgCIAAAAQg1gag6gWIAAAAQAAgFgDgEIAAAAQg2hSAlhPIAAAAQAAgFgCgCIAAAAQhEg7BQgsIAAAAQAcg5gwgCIAAAAIgUgBQAbhZhXAJIAAAAIAAgFQglACgNgRIAAAAQA1ggAggwIAAAAIgFAAQg0gdAWgsIAAAAQAHgNAMgOIAAAAIgLgJQgHgGgVAGIAAAAQgSgzBDgOIAAAAQABAAAAgFIAAAAQglgyA0gyIAAAAIgFAAQgxgoAvgrIAAAAQACgCgLgOIAAAAQgJgMAAgTIAAAAQACgjBLAIIAAAAIgIgKQgWgegRghIAAAAIAAgKIAAAAQAyAAAyAFIAAAAIAAAFIAAAAIAKAAIAAAAIAAgKIAAAAIAAhGIAAAAIAAgKIAAAAIAAhQIAAAAIAKAAIAAAAQBxAZAjhYIAAAAIAZgRQAKgHARgDIAAAAQAOhIBMAeIAAAAQAFACAFAAIAAAAQBFhNA1AsIAAAAQADADAFAAIAAAAQBQgtBYgFIAAAAQAGAAAOgMIAAAAQAKgJABgTIAAAAQALAiAQANIAAAAQATANAbgPIAAAAQBGgoAbA3IAAAAQBSgRAfAjIAAAAIAaAGQASAEgHAgIAAAAQA9AGA/AkIAAAAIANAGIAAAAQAUAJgHArIAAAAQAYAPAuAAIAAAAIAAAFQgHA5A5gHIAAAAIAKAAIAAAAQBGAKgKBaIAAAAIAAAKIAAAAQAXA9gDBFIAAAAIAAAKIAAAAQAUAUAPAUIAAAAIgFAAQAPAxAlAcIAAAAQADADAFAAIAAAAIAAAKIAAAAQgFAAgCACIAAAAQgVAdggATIAAAAQBBAtgkBNIAAAAIAAAAIgDAFIAAAAQgBACgFAAIAAAAQAAAFABAEIAAAAQAqB6hJAdIAAAAQAAAFACAEIAAAAQAcBCg8APIAAAAQAAAFgDACIAAAAQgHAIgKAFIAAAAQAAAFACAEIAAAAQAlBBhFAkIAAAAQAFAAAEACIAAAAQA2AbghBRIAAAAQAFAAABABIAAAAQAGAfASAwIAAAAQAAAFgCABIAAAAQgoAUAWA2IAAAAQgFAAgDACIAAAAQg/A5hPApIAAAAIgKAAIAAAAQhoANg4ghIAAAAIgKAAIAAAAQg1gCg5AqIAAAAIgKAAIAAAAQhKgBg4gTIAAAAQgFAAgDACIAAAAQgVAOgYAKIAAAAQg8AYhWAAIAAAAIgeAAgAEtKyIAAAAIAAAAIAAAAgAEuKyQAQAAANgDIAAAAQgNADgQAAgAD8KsIgBgBIAAAAIABABgAIXKdIgBAAIAAAAIABAAgAIXKdIAOAAIAAAAIgOAAgAniKYIAAAAIAAAAIAAAAgAnhKYQA5AAAegZIAAAAQgeAZg5AAgAGEKKIABgBIAAAAIgBABgAGFKJIABgBIAAAAIgBABgAHaKIIgBAAIAAAAIABAAgAGHKIQAVgSAUAAIAAAAQgUAAgVASgAHYKHIgBgBIAAAAIABABgAmJJ/IABgBIAAAAIgBABgAmIJ9IABgBIAAAAIgBABgAmGJ8QAegfA2AAIAAAAQg2AAgeAfgAGxJ2IAAAAIAAAAIAAAAgAkuJdIgCAAIAAAAIACAAgAkxJdIAAAAIAAAAIAAAAgAKMIFQALATAAAWIAAAAQAAgWgLgTIAAAAQgQgUAAgXIAAAAQAAAXAQAUgApoGhQgkAyAAA3IAAAAQAAg3AkgyIAAAAQAIgKAGgKIAAAAQgGAKgIAKgAKBHAIgBgCgAKqErIAAAFIAAAAIAAgFIAAAAIAAAAgAIDnrIAgAgIAAAAIAKAKIAAAAIAcAeIAAAAIABACIAAAAQBNBaAhBrIAAAAQAgBfgBBrIAAAAQABhrgghfIAAAAQghhrhNhaIAAAAIgBgCIAAAAIgcgeIAAAAIgKgKIAAAAIggggIAAAAQjUjEksAAIAAAAQksAAjUDEIAAAAQjVDEAAEVIAAAAQAAkVDVjEIAAAAQDUjEEsAAIAAAAQEsAADUDEg");
	var mask_2_graphics_254 = new cjs.Graphics().p("AhtM5QgFAAgEgCIAAAAQgbgRgigLIAAAAQgFAAgCgDIAAAAQgDgCAAgFIAAAAQg7gRghgoIAAAAQghgngjgiIAAAAIgBAFIAAAAQgpADgcAMIAAAAQAAAFgBAAIAAAAQhDAMgqgRIAAAAQgFAAgEgCIAAAAQg1gag6gWIAAAAQAAgFgDgEIAAAAQg2hSAlhPIAAAAQAAgFgCgCIAAAAQhEg7BQgsIAAAAQAcg5gwgCIAAAAIgUgBQAbhZhXAJIAAAAIAAgFQglACgNgRIAAAAQA1ggAggwIAAAAIgFAAQg0gdAWgsIAAAAQAHgNAMgOIAAAAIgLgJQgHgGgVAGIAAAAQgSgzBDgOIAAAAQABAAAAgFIAAAAQglgyA0gyIAAAAIgFAAQgxgoAvgrIAAAAQACgCgLgOIAAAAQgJgMAAgTIAAAAQACgjBLAIIAAAAIgIgKQgWgegRghIAAAAIAAgKIAAAAQAyAAAyAFIAAAAIAAAFIAAAAIAKAAIAAAAIAAgKIAAAAIAAhGIAAAAIAAgKIAAAAIAAhQIAAAAIAKAAIAAAAQBxAZAjhYIAAAAIAZgRQAKgHARgDIAAAAQAOhIBMAeIAAAAQAFACAFAAIAAAAQBFhNA1AsIAAAAQADADAFAAIAAAAQBQgtBYgFIAAAAQAGAAAOgMIAAAAQAKgJABgTIAAAAQALAiAQANIAAAAQATANAbgPIAAAAQBGgoAbA3IAAAAQBSgRAfAjIAAAAIAaAGQASAEgHAgIAAAAQA9AGA/AkIAAAAIANAGIAAAAQAUAJgHArIAAAAQAYAPAuAAIAAAAIAAAFQgHA5A5gHIAAAAIAKAAIAAAAQBGAKgKBaIAAAAIAAAKIAAAAQAXA9gDBFIAAAAIAAAKIAAAAQAUAUAPAUIAAAAIgFAAQAPAxAlAcIAAAAQADADAFAAIAAAAIAAAKIAAAAQgFAAgCACIAAAAQgVAdggATIAAAAQBBAtgkBNIAAAAIAAAAIgDAFIAAAAQgBACgFAAIAAAAQAAAFABAEIAAAAQAqB6hJAdIAAAAQAAAFACAEIAAAAQAcBCg8APIAAAAQAAAFgDACIAAAAQgHAIgKAFIAAAAQAAAFACAEIAAAAQAlBBhFAkIAAAAQAFAAAEACIAAAAQA2AbghBRIAAAAQAFAAABABIAAAAQAGAfASAwIAAAAQAAAFgCABIAAAAQgoAUAWA2IAAAAQgFAAgDACIAAAAQg/A5hPApIAAAAIgKAAIAAAAQhoANg4ghIAAAAIgKAAIAAAAQg1gCg5AqIAAAAIgKAAIAAAAQhKgBg4gTIAAAAQgFAAgDACIAAAAQgVAOgYAKIAAAAQg8AYhWAAIAAAAIgeAAgAEtKyIAAAAIAAAAIAAAAgAEuKyQAQAAANgDIAAAAQgNADgQAAgAD8KsIgBgBIAAAAIABABgAIXKdIgBAAIAAAAIABAAgAIXKdIAOAAIAAAAIgOAAgAniKYIAAAAIAAAAIAAAAgAnhKYQA5AAAegZIAAAAQgeAZg5AAgAGEKKIABgBIAAAAIgBABgAGFKJIABgBIAAAAIgBABgAHaKIIgBAAIAAAAIABAAgAGHKIQAVgSAUAAIAAAAQgUAAgVASgAHYKHIgBgBIAAAAIABABgAmJJ/IABgBIAAAAIgBABgAmIJ9IABgBIAAAAIgBABgAmGJ8QAegfA2AAIAAAAQg2AAgeAfgAGxJ2IAAAAIAAAAIAAAAgAkuJdIgCAAIAAAAIACAAgAkxJdIAAAAIAAAAIAAAAgAKMIFQALATAAAWIAAAAQAAgWgLgTIAAAAQgQgUAAgXIAAAAQAAAXAQAUgApoGhQgkAyAAA3IAAAAQAAg3AkgyIAAAAQAIgKAGgKIAAAAQgGAKgIAKgAKBHAIgBgCgAKqErIAAAFIAAAAIAAgFIAAAAIAAAAgAIDnrIAgAgIAAAAIAKAKIAAAAIAcAeIAAAAIABACIAAAAQBNBaAhBrIAAAAQAgBfgBBrIAAAAQABhrgghfIAAAAQghhrhNhaIAAAAIgBgCIAAAAIgcgeIAAAAIgKgKIAAAAIggggIAAAAQjUjEksAAIAAAAQksAAjUDEIAAAAQjVDEAAEVIAAAAQAAkVDVjEIAAAAQDUjEEsAAIAAAAQEsAADUDEg");
	var mask_2_graphics_255 = new cjs.Graphics().p("AhhMQQgFAAgDgCIAAAAQgbgQghgLIAAAAQgFAAgDgDIAAAAQgCgCAAgFIAAAAQg5gQghgnIAAAAQgfglgigiIAAAAIgCAAIAAAAIAAAEIAAAAQgUABgQAEIAAAAQgUAIgPAPIAAAAQglAhhTgIIAAAAQhvgIgXhZIAAAAQgKgoAIgmIAAAAQAIgkAZgiIAAAAQAOgSAHgUIAAAAQgHAUgOASIAAAAQgZAigIAkIAAAAQgEgoASgnIAAAAQAAgFgDgCIAAAAQg/g3BGgqIAAAAIgBgEIAAAAIgLgaIAAAAIgMggIAAAAIgKgBIAFgQIAAAAIgKgfIAAAAIgJgbIAAAAQgQgHgcADIAAAAIAAgFQgkACgNgQIAAAAQAqgaAdgkIAAAAIgEgYIAAAAQgLgIgGgKIAAAAQgMgRAIgWIAAAAQgHAAgJADIAAAAQgQgqAvgQIAAAAIAAAWIAAAAIAAgWIAAAAIADgqIAAAAQgCgOAFgPIAAAAQADgYAGgYIAAAAQARhLAihFIAAAAIAHgMIAAAAQASghAWgfIAAAAQAPgVAQgUIAAAAQAfglAlgiIAAAAQDVjEErAAIAAAAQEsAADVDEIAAAAIAgAgIAAAAIAKAKIAAAAIAbAeIAAAAIABACIAAAAQBNBaAiBrIAAAAQAVBBAHBGIAAAAIAAAAQADAhAAAiIAAAAQAAgigDghIAAAAQA/AtgkBLIAAAAIAAAAIgFADIAAAAQAZBfg6AbIAAAAIgCANIAAAAQAUAzgiATIAAAAQgKAqgOAoIAAAAQAAAIgDAHIAAAAQgDAZgKAdIAAAAQAbAegZA9IAAAAQAFAAAAABIAAAAQAHAfARAuIAAAAQAAAFgBABIAAAAQgoAUAWA1IAAAAQgFAAgDACIAAAAQg+A4hNAoIAAAAIgKAAIAAAAQhmANg3ghIAAAAIgJAAIAAAAQg1gCg3ApIAAAAIgKAAIAAAAQhJgBg2gSIAAAAQgFAAgDACIAAAAIgJAGIAAAAIgJANIAAAAQg8AYhSAAIAAAAIgfgBgAEjK/IgBAAIAAAAIABAAgAEjK/QAPAAANgDIAAAAQgNADgPAAgADxK5IAAAAIAAAAIAAAAgADvK5IgBgBIAAAAIABABgAINKrIgBAAIAAAAIABAAgAINKrIAMgBIAAAAIgMABgAF5KYIABgBIAAAAIgBABgAF7KXIAAgBIAAAAIAAABgAHPKWIAAAAIAAAAIAAAAgAF8KVQAVgSAVAAIAAAAQgVAAgVASgAHOKVIgBgBIAAAAIABABgAGnKDIgBAAIAAAAIABAAgAlJJrIAFAAIAAAAIgFAAgAlDJrIAHAAIAAAAIgHAAgAk5JrIgBAAIAAAAIABAAgAk8JrIAAAAIAAAAIAAAAgAKBITQAMASAAAWIAAAAQAAgWgMgSIAAAAQgQgUAAgYIAAAAQAAAYAQAUgAJ2HOIgBgDgAK5jcQgihrhNhaIAAAAIgBgCIAAAAIgbgeIAAAAIgKgKIAAAAIggggIAAAAQjVjEksAAIAAAAQkrAAjVDEIAAAAQglAigfAlIAAAAIAAgfIAAAAIAAgKIAAAAIAAhOIAAAAIAKAAIAAAAQBvAZAihXIAAAAIAZgQQAJgHARgDIAAAAQAPhHBKAeIAAAAIAJACIAAAAQBDhLA0ArIAAAAQADACAFAAIAAAAQBPgsBWgEIAAAAQAGgBAOgLIAAAAQAJgJABgTIAAAAQALAiAQAMIAAAAIAAAAQALAAANgIIAAAAQBEgnAbA2IAAAAQBRgQAeAiIAAAAIAZAGQASADgHAgIAAAAQA8AGA+AjIAAAAIAMAGIAAAAQAUAJgGAqIAAAAQAXAPAtgBIAAAAIAAAFQgHA4A4gGIAAAAIAKgBIAAAAQBEAKgJBYIAAAAIgBAKIAAAAQAXA8gDBDIAAAAIAAAKIAAAAQATAUAPATIAAAAIgEAAQAOAxAkAbIAAAAQADADAFAAIAAAAIAAAJIAAAAQgFAAgCADIAAAAQgVAcgeASIAAAAQgHhGgVhBgAq4jlQACgCgLgNIAAAAQgIgMgBgTIAAAAQACgeA5ACIAAAAQgiBFgRBLIAAAAQgdgiAngkgAqsl1IAAgJIAAAAQAmAAAmADIAAAAQgWAfgSAhIAAAAQgVgbgPgfg");
	var mask_2_graphics_256 = new cjs.Graphics().p("AhVLoQgEAAgEgCIAAAAIgXgNIAAAAQgaABgZgHIAAAAQg3gRgXgmIAAAAQgWghg3gCIAAAAQg7gBgfAhIAAAAQgmAhhSgIIAAAAQhwgIgWhZIAAAAQgUhOAyhGIAAAAIABgBIAAAAQAVgbAGggIAAAAQgGAggVAbIAAAAQABgbANgbIAAAAQAAgFgDgCIAAAAQgvgpAcgiIAAAAIgIgUIAAAAQgQgogLgoIAAAAQgLgdgIgeIAAAAIgKABIAAAAIAAgFQgkACgMgQIAAAAQAZgQAVgTIAAAAQgFgcgDgdIAAAAIgJACIAAAAQgKgcARgPIAAAAIAAgQIAAAAQAAhwAihiIAAAAQAHgVAJgUIAAAAQALgYANgYIAAAAIAUghIAAAAQAegxAqguIAAAAQAVgXAZgXIAAAAIAIgIIAAAAQBHg/BPgqIAAAAQAigRAjgOIAAAAQAXgJAYgIIAAAAQAlgLAmgIIAAAAQAngIAqgEIAAAAQAogEAqAAIAAAAQEsAADUDEIAAAAIAhAgIAAAAIAKAKIAAAAIAbAeIAAAAIABACIAAAAQAyA6AfBBIAAAAQASAkALAmIAAAAQAJAcAHAcIAAAAQAJAnADApIAAAAQADAZAAAbIAAAAIABAOIAAAAIgBgOIAAAAQAVAjgXAyIAAAAIgBAAIgBAIIAAAAIAAAHIAAAAQgICgg8CXIAAAAQgGAZgLAcIAAAAQgDALgFANIAAAAQAFAAAAABIAAAAQAHAeARAtIAAAAQAAAFgCABIAAAAQgPAIgGAMIAAAAIgHgNIAAAAQgPgUAAgXIAAAAQAAAXAPAUIAAAAIAHANIAAAAQAIAWgGAZIAAAAIACAEIAAAAIgCAAIAAAAQgEAPgKARIAAAAQgfA3hCAAIAAAAIgRABIAAAAIgOAIIAAAAIgKAAIAAAAQhUALg1gWIAAAAQgQAOgYAGIAAAAQgfAHgrgJIAAAAIgRAMIAAAAIgKAAIAAAAQhBgBgzgQIAAAAIgKAOIAAAAQg7AYhSAAIAAAAIgcgBgAFwKmIABgBIAAAAIgBABgAFyKkQAVgTAVAAIAAAAQgVAAgVATgAHCKhIAAAAIAAAAIAAAAgAGdKRIAAAAIAAAAIAAAAgAJsHcIAAgDgALKikQAOAYAVAQIAAAAQADACAEAAIAAAAIAAAKIAAAAQgEAAgCACIAAAAQgLAOgNAMIAAAAQgDgpgJgngAqwj2QABgQASgHIAAAAQgJAUgHAVIAAAAQgCgIgBgKgAJMmhIgBgCIAAAAIgbgeIAAAAIgKgKIAAAAIghggIAAAAQjUjEksAAIAAAAQgqAAgoAEIAAAAQA0gTA2gDIAAAAQAGAAANgLIAAAAQAKgJABgTIAAAAQAIAZALANIAAAAIALgFIAAAAQBCgmAbA0IAAAAQBOgPAeAhIAAAAIAZAFQARAEgHAfIAAAAQA7AGA9AiIAAAAIALAGIAAAAQAUAJgHApIAAAAQAXAPAsgBIAAAAIAAAFQgHA2A3gGIAAAAIAKAAIAAAAQBDAJgKBXIAAAAIAAAJIAAAAQAOAlAEAoIAAAAQgfhBgyg6gAqTlVIAAgKIAAAAIAiABIAAAAIgUAhIAAAAIgOgYgAopn4IAJAAIAAAAQAZAFAUAAIAAAAIgIAIIAAAAQgZAXgVAXIAAAAIAAg7gAkYp7QgjAOgiARIAAAAQAQguA1APgAieqfQgmAIglALIAAAAQApgiAiAPg");
	var mask_2_graphics_257 = new cjs.Graphics().p("AiyLOQg3gSgXgmIAAAAQgWggg3gDIAAAAQg7gBgfAiIAAAAQgmAhhSgIIAAAAQhwgIgWhZIAAAAQgUhPAyhGIAAAAQAegmABgxIAAAAQgHgNgEgNIAAAAIgMgaIAAAAIgCgFIAAAAIgKgaIAAAAQgQgogLgoIAAAAQgSgtgKgvIAAAAQgPgDgIgKIAAAAIATgMIAAAAQgJg2AAg3IAAAAQAAkEC7i9IAAAAIACgCIAAAAIAXgWIAAAAQDUjEEsAAIAAAAQAdAAAdACIAAAAIALABIAAAAIANABIAAAAQAmAEAkAHIAAAAIAcAGIAAAAQA+AOA5AYIAAAAQAyAVAuAdIAAAAQAWAOAVAPIAAAAQAfAXAeAbIAAAAIAFAEIAAAAIAEAEIAAAAIAhAgIAAAAIAKAKIAAAAIACACIAAAAIAZAcIAAAAIABACIAAAAQAVAYASAbIAAAAQAvBFAYBNIAAAAQATA5AIA+IAAAAIABAKIAAAAQAEAkAAAlIAAAAQAAAogFAnIAAAAIAAAHIAAAAQgKC7hQCtIAAAAQgGAUgKAVIAAAAIALAfIAAAAQAAAFgCABIAAAAQgRAJgFAPIAAAAQgBgIAAgIIAAAAQAAAIABAIIAAAAQAEAPAKANIAAAAQAaAqgfA3IAAAAQgfA2hCABIAAAAQgxAEgcgaIAAAAQgMgLgOgEIAAAAIgZgBIAAAAQgQAEgRAPIAAAAIgCACIAAAAIgEADIAAAAQgSAXghAJIAAAAQgiAIgygMIAAAAQgtgMgTgaIAAAAQgegDgbgGIAAAAIgCAEIAAAAQgxAThCADIAAAAQghAcgcAGIAAAAQgZAGgYAAIAAAAQgYAAgWgGgABIJ6IAPAAIAAAAIgPAAgABXJ6IAAAAIAAAAIAAAAgAJhHWIAAgCgALVh4IAEABIAAAAIAAAJIAAAAIgDAAIAAAAIgBgKgAJMm0IgBgCIAAAAIgZgcIAAAAIAIAAIAAAAQA8AIgDBJIAAAAQgSgbgVgYgAoUnoIACAAIAAAAIgCACIAAAAIAAgCgAH8oGIAFAAIAAAAIAAAEIgFgEgAGUpVIAUALIAAAAIALAFIAAAAQAIAEAEAJIAAAAQgVgPgWgOgAC9qtQA7gHAZAbIAAAAIAYAGQAKACABAKIAAAAQg5gYg+gOgABXq+QAygYAYAjIAAAAQgkgHgmgEgAA/rAQAEgHAAgMIAAAAIAHAUIAAAAIgLgBg");
	var mask_2_graphics_258 = new cjs.Graphics().p("AjDLMQg2gRgXgmQgWghg4gCQg6gBggAhQglAhhSgIQhwgIgWhZQgVhOAzhGQAyhBghhdIgKgaQgQgpgLgnQgphqAAh5QAAkUDUjEQDUjEEsAAQEtAADUDEIAgAfIAKAKIAcAfIABABQBMBbAhBrQAgBfAABrQAAApgFAmIAAAGQgLDjh0DQIAAgCIAAADQgNAmAYAfQAZApgeA3QgfA3hCAAQgxAFgcgaQgogkgsAnIgCABIgEAEQgSAXgiAIQgiAIgxgMQgygMgRgfQgTghg9AGQgvADgZAeQgtArgjAHQgZAGgYAAQgYAAgXgGg");

	this.timeline.addTween(cjs.Tween.get(mask_2).to({graphics:mask_2_graphics_0,x:-126.9,y:-112.8}).wait(15).to({graphics:mask_2_graphics_15,x:-129,y:-111.5}).wait(1).to({graphics:mask_2_graphics_16,x:-129.2,y:-108.1}).wait(1).to({graphics:mask_2_graphics_17,x:-129.7,y:-104.1}).wait(1).to({graphics:mask_2_graphics_18,x:-130.1,y:-100.2}).wait(1).to({graphics:mask_2_graphics_19,x:-130.3,y:-96.1}).wait(1).to({graphics:mask_2_graphics_20,x:-130.3,y:-91.8}).wait(1).to({graphics:mask_2_graphics_21,x:-130.3,y:-92}).wait(1).to({graphics:mask_2_graphics_22,x:-130.3,y:-92.1}).wait(1).to({graphics:mask_2_graphics_23,x:-130.3,y:-92.3}).wait(1).to({graphics:mask_2_graphics_24,x:-130.3,y:-92.4}).wait(1).to({graphics:mask_2_graphics_25,x:-130.3,y:-92.6}).wait(1).to({graphics:mask_2_graphics_26,x:-130.3,y:-92.8}).wait(1).to({graphics:mask_2_graphics_27,x:-130.3,y:-92.9}).wait(1).to({graphics:mask_2_graphics_28,x:-130.2,y:-93.1}).wait(1).to({graphics:mask_2_graphics_29,x:-130.2,y:-93.2}).wait(1).to({graphics:mask_2_graphics_30,x:-130.3,y:-93.3}).wait(1).to({graphics:mask_2_graphics_31,x:-130.3,y:-93.1}).wait(1).to({graphics:mask_2_graphics_32,x:-130.4,y:-92.9}).wait(1).to({graphics:mask_2_graphics_33,x:-130.3,y:-92.6}).wait(1).to({graphics:mask_2_graphics_34,x:-130.3,y:-92.3}).wait(1).to({graphics:mask_2_graphics_35,x:-130.4,y:-92.1}).wait(1).to({graphics:mask_2_graphics_36,x:-130.3,y:-91.8}).wait(2).to({graphics:mask_2_graphics_38,x:-130.3,y:-91.8}).wait(1).to({graphics:mask_2_graphics_39,x:-130.2,y:-94.9}).wait(1).to({graphics:mask_2_graphics_40,x:-130.1,y:-98}).wait(1).to({graphics:mask_2_graphics_41,x:-129.8,y:-101.1}).wait(1).to({graphics:mask_2_graphics_42,x:-129.5,y:-104}).wait(1).to({graphics:mask_2_graphics_43,x:-129.3,y:-107}).wait(1).to({graphics:mask_2_graphics_44,x:-129.2,y:-109.9}).wait(1).to({graphics:mask_2_graphics_45,x:-128.9,y:-111.7}).wait(1).to({graphics:mask_2_graphics_46,x:-126.9,y:-112.8}).wait(1).to({graphics:mask_2_graphics_47,x:-126.9,y:-112.8}).wait(1).to({graphics:mask_2_graphics_48,x:-127.2,y:-114.1}).wait(1).to({graphics:mask_2_graphics_49,x:-127.4,y:-114}).wait(1).to({graphics:mask_2_graphics_50,x:-127.6,y:-113}).wait(1).to({graphics:mask_2_graphics_51,x:-128,y:-112.3}).wait(1).to({graphics:mask_2_graphics_52,x:-128.6,y:-111.4}).wait(1).to({graphics:mask_2_graphics_53,x:-129.3,y:-110.3}).wait(1).to({graphics:mask_2_graphics_54,x:-129.2,y:-108.3}).wait(1).to({graphics:mask_2_graphics_55,x:-129.3,y:-106.3}).wait(29).to({graphics:mask_2_graphics_84,x:-129.3,y:-106.3}).wait(1).to({graphics:mask_2_graphics_85,x:-129.3,y:-106.3}).wait(1).to({graphics:mask_2_graphics_86,x:-128.8,y:-107.5}).wait(1).to({graphics:mask_2_graphics_87,x:-128.3,y:-108.8}).wait(1).to({graphics:mask_2_graphics_88,x:-128.1,y:-110.1}).wait(1).to({graphics:mask_2_graphics_89,x:-127.9,y:-111.3}).wait(1).to({graphics:mask_2_graphics_90,x:-127.7,y:-112.6}).wait(1).to({graphics:mask_2_graphics_91,x:-127.5,y:-113.8}).wait(1).to({graphics:mask_2_graphics_92,x:-127.3,y:-113.4}).wait(1).to({graphics:mask_2_graphics_93,x:-126.9,y:-112.8}).wait(1).to({graphics:mask_2_graphics_94,x:-126.9,y:-112.8}).wait(1).to({graphics:mask_2_graphics_95,x:-126.7,y:-112.8}).wait(1).to({graphics:mask_2_graphics_96,x:-126.7,y:-113.3}).wait(1).to({graphics:mask_2_graphics_97,x:-126.7,y:-113.8}).wait(1).to({graphics:mask_2_graphics_98,x:-126.7,y:-114.1}).wait(1).to({graphics:mask_2_graphics_99,x:-126.7,y:-112.8}).wait(1).to({graphics:mask_2_graphics_100,x:-126.7,y:-111.5}).wait(1).to({graphics:mask_2_graphics_101,x:-126.7,y:-110.1}).wait(33).to({graphics:mask_2_graphics_134,x:-126.7,y:-110.1}).wait(5).to({graphics:mask_2_graphics_139,x:-126.7,y:-110.1}).wait(1).to({graphics:mask_2_graphics_140,x:-126.7,y:-111.3}).wait(1).to({graphics:mask_2_graphics_141,x:-126.7,y:-112.4}).wait(1).to({graphics:mask_2_graphics_142,x:-126.7,y:-113.6}).wait(1).to({graphics:mask_2_graphics_143,x:-126.7,y:-114.7}).wait(1).to({graphics:mask_2_graphics_144,x:-126.7,y:-114.4}).wait(1).to({graphics:mask_2_graphics_145,x:-126.7,y:-113.9}).wait(1).to({graphics:mask_2_graphics_146,x:-126.7,y:-113.3}).wait(1).to({graphics:mask_2_graphics_147,x:-126.7,y:-112.8}).wait(24).to({graphics:mask_2_graphics_171,x:-126.7,y:-112.8}).wait(16).to({graphics:mask_2_graphics_187,x:-126.4,y:-112.8}).wait(1).to({graphics:mask_2_graphics_188,x:-126.4,y:-112.9}).wait(1).to({graphics:mask_2_graphics_189,x:-126.4,y:-113.1}).wait(1).to({graphics:mask_2_graphics_190,x:-126.4,y:-112.2}).wait(1).to({graphics:mask_2_graphics_191,x:-126.4,y:-111}).wait(1).to({graphics:mask_2_graphics_192,x:-126.4,y:-109.8}).wait(1).to({graphics:mask_2_graphics_193,x:-127,y:-108.2}).wait(3).to({graphics:mask_2_graphics_196,x:-127,y:-108.2}).wait(2).to({graphics:mask_2_graphics_198,x:-127,y:-108.2}).wait(5).to({graphics:mask_2_graphics_203,x:-127,y:-108.2}).wait(4).to({graphics:mask_2_graphics_207,x:-127,y:-108.2}).wait(5).to({graphics:mask_2_graphics_212,x:-127,y:-108.2}).wait(4).to({graphics:mask_2_graphics_216,x:-127,y:-108.2}).wait(5).to({graphics:mask_2_graphics_221,x:-127,y:-108.2}).wait(1).to({graphics:mask_2_graphics_222,x:-127,y:-108.2}).wait(1).to({graphics:mask_2_graphics_223,x:-126.4,y:-109.3}).wait(1).to({graphics:mask_2_graphics_224,x:-126.4,y:-110.3}).wait(1).to({graphics:mask_2_graphics_225,x:-126.4,y:-111.2}).wait(1).to({graphics:mask_2_graphics_226,x:-126.4,y:-112.2}).wait(1).to({graphics:mask_2_graphics_227,x:-126.4,y:-113.1}).wait(1).to({graphics:mask_2_graphics_228,x:-126.4,y:-113}).wait(1).to({graphics:mask_2_graphics_229,x:-126.4,y:-112.9}).wait(1).to({graphics:mask_2_graphics_230,x:-126.4,y:-112.8}).wait(1).to({graphics:mask_2_graphics_231,x:-126.9,y:-112.8}).wait(1).to({graphics:mask_2_graphics_232,x:-129.2,y:-114.8}).wait(1).to({graphics:mask_2_graphics_233,x:-131,y:-117.1}).wait(1).to({graphics:mask_2_graphics_234,x:-132.6,y:-117.4}).wait(1).to({graphics:mask_2_graphics_235,x:-134.2,y:-117.8}).wait(1).to({graphics:null,x:0,y:0}).wait(18).to({graphics:mask_2_graphics_254,x:-134.2,y:-117.8}).wait(1).to({graphics:mask_2_graphics_255,x:-132.5,y:-117.4}).wait(1).to({graphics:mask_2_graphics_256,x:-130.9,y:-117}).wait(1).to({graphics:mask_2_graphics_257,x:-129.2,y:-114.7}).wait(1).to({graphics:mask_2_graphics_258,x:-126.9,y:-112.8}).wait(1));

	// body04
	this.instance_29 = new lib.body_04();
	this.instance_29.parent = this;
	this.instance_29.setTransform(-126.5,-112,1,1,0,0,0,72.5,72);

	var maskedShapeInstanceList = [this.instance_29];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_29).wait(14).to({alpha:0},6).wait(20).to({alpha:1},6).wait(1).to({alpha:0},2).wait(35).to({alpha:1},5).wait(6).to({alpha:0},4).wait(43).to({alpha:1},4).wait(43).to({alpha:0},3).wait(32).to({alpha:1},3).wait(4).to({alpha:0},2).wait(22).to({alpha:1},2).wait(2));

	// body06
	this.instance_30 = new lib.body06();
	this.instance_30.parent = this;
	this.instance_30.setTransform(-133.5,-117.5,1,1,0,0,0,81.5,83.5);
	this.instance_30.alpha = 0;
	this.instance_30._off = true;

	var maskedShapeInstanceList = [this.instance_30];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_30).wait(231).to({_off:false},0).to({alpha:1},4).to({_off:true},1).wait(18).to({_off:false},0).to({alpha:0},4).wait(1));

	// body05
	this.instance_31 = new lib.body05();
	this.instance_31.parent = this;
	this.instance_31.setTransform(-127.5,-108,1,1,0,0,0,73.5,76);
	this.instance_31.alpha = 0;
	this.instance_31._off = true;

	var maskedShapeInstanceList = [this.instance_31];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_31).wait(189).to({_off:false},0).to({alpha:1},4).wait(5).to({scaleY:1,skewX:-4.1,x:-123.5},5).wait(4).to({scaleY:1,skewX:2.6,x:-130},5).to({skewX:2.6},4).to({scaleY:1,skewX:0,x:-127.5},5).wait(1).to({alpha:0},5).to({_off:true},1).wait(31));

	// body03
	this.instance_32 = new lib.Symbol68();
	this.instance_32.parent = this;
	this.instance_32.setTransform(-126,-103.5,1,1,0,0,0,72,79.5);
	this.instance_32.alpha = 0;
	this.instance_32._off = true;

	var maskedShapeInstanceList = [this.instance_32];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_32).wait(97).to({_off:false},0).to({alpha:1},3).wait(42).to({alpha:0},6).to({_off:true},1).wait(110));

	// body02
	this.instance_33 = new lib.body02();
	this.instance_33.parent = this;
	this.instance_33.setTransform(-129.5,-106,1,1,0,0,0,70.5,79);
	this.instance_33.alpha = 0;
	this.instance_33._off = true;

	var maskedShapeInstanceList = [this.instance_33];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_33).wait(47).to({_off:false},0).to({alpha:1},4).wait(33).to({alpha:0},5).to({_off:true},1).wait(169));

	// body01
	this.instance_34 = new lib.body01();
	this.instance_34.parent = this;
	this.instance_34.setTransform(-131,-91.5,1,1,0,0,0,77,103.5);
	this.instance_34.alpha = 0;
	this.instance_34._off = true;

	var maskedShapeInstanceList = [this.instance_34];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_34).wait(16).to({_off:false},0).to({alpha:1},4).to({regX:77.2,scaleX:0.96,scaleY:1.05,x:-130.7,y:-94},10).to({regX:77,scaleX:1,scaleY:1,x:-131,y:-91.5},6).wait(1).to({alpha:0},5).to({_off:true},5).wait(212));

	// body_Fill_Shadow
	this.shape_70 = new cjs.Shape();
	this.shape_70.graphics.rf(["rgba(255,255,255,0)","#D6D6D6"],[0.188,1],-12.5,-77,0,-12.5,-77,87.2).s().p("A1ebWMAAAg2rMAq9AAAMAAAA2rg");
	this.shape_70.setTransform(-128.4,-54.9);

	var maskedShapeInstanceList = [this.shape_70];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_70).to({_off:true},236).wait(18).to({_off:false},0).wait(5));

	// body_Fill_Grey
	this.shape_71 = new cjs.Shape();
	this.shape_71.graphics.f("#FFFFFF").s().p("A1ebWMAAAg2rMAq9AAAMAAAA2rg");
	this.shape_71.setTransform(-128.4,-54.9);

	var maskedShapeInstanceList = [this.shape_71];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get(this.shape_71).to({_off:true},236).wait(18).to({_off:false},0).wait(5));

	// Icecream_Shadow
	this.instance_35 = new lib.Tween1("synched",0);
	this.instance_35.parent = this;
	this.instance_35.setTransform(-127,108.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_35).wait(259));

	// Icecream_cone
	this.instance_36 = new lib.Tween2("synched",0);
	this.instance_36.parent = this;
	this.instance_36.setTransform(-130,12);

	this.timeline.addTween(cjs.Tween.get(this.instance_36).wait(259));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220,-185,186,307.1);


(lib.heart = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_14 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(14).call(this.frame_14).wait(1));

	// Слой 1
	this.instance = new lib.heart2();
	this.instance.parent = this;
	this.instance.setTransform(111.1,108.6,0.413,0.413,0,0,0,111.2,108.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:111,regY:108.5,scaleX:1.17,scaleY:1.17,x:111,y:108.5},4,cjs.Ease.get(-1)).to({scaleX:0.91,scaleY:0.91},3).to({regX:110.9,scaleX:1.1,scaleY:1.1,x:110.9},7,cjs.Ease.get(1)).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(65.2,63.8,91.6,89.6);


(lib.angryclouds = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 13
	this.instance = new lib.mol("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(219.2,184.7,0.564,0.668,0,0,0,48.1,58.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(13).to({_off:false},0).wait(1).to({regX:48.2,regY:87.4,scaleX:0.57,scaleY:0.67,x:220.5,y:204.1,startPosition:1},0).wait(1).to({scaleX:0.57,scaleY:0.67,x:224.3,y:203.8,startPosition:2},0).wait(1).to({scaleX:0.59,scaleY:0.68,x:231.7,y:203.2,startPosition:3},0).wait(1).to({scaleX:0.61,scaleY:0.69,x:243.7,y:202.2,startPosition:4},0).wait(1).to({scaleX:0.64,scaleY:0.71,x:261.9,y:200.7,startPosition:5},0).wait(1).to({scaleX:0.69,scaleY:0.73,x:286.3,y:198.7,startPosition:6},0).wait(1).to({scaleX:0.73,scaleY:0.75,x:311.9,y:196.6,startPosition:7},0).wait(1).to({scaleX:0.76,scaleY:0.77,x:328.4,y:195.2,startPosition:8},0).wait(1).to({regY:58,scaleX:0.77,scaleY:0.77,x:333.2,y:172.2,startPosition:0},0).to({x:345.2,y:160.2},77).wait(1));

	// cloud
	this.instance_1 = new lib.cloud();
	this.instance_1.parent = this;
	this.instance_1.setTransform(189.2,142.1,0.099,0.112,0,0,180,338.4,242.1);
	this.instance_1.alpha = 0.32;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({_off:false},0).wait(1).to({regX:338.7,regY:240.6,scaleX:0.1,scaleY:0.11,x:191.1,y:140.5},0).wait(1).to({scaleX:0.1,scaleY:0.12,x:197.5,y:135.9},0).wait(1).to({scaleX:0.11,scaleY:0.12,x:209.5,y:127.4},0).wait(1).to({scaleX:0.12,scaleY:0.13,x:227.6,y:114.5},0).wait(1).to({scaleX:0.14,scaleY:0.14,x:250.9,y:97.9},0).wait(1).to({scaleX:0.15,scaleY:0.15,x:274.4,y:81},0).wait(1).to({scaleX:0.16,scaleY:0.16,x:290.8,y:69.4},0).wait(1).to({regY:242.1,scaleX:0.17,scaleY:0.16,x:296.2,y:65.8},0).to({x:309.2,y:65.5},90).wait(1));

	// cloud
	this.instance_2 = new lib.cloud();
	this.instance_2.parent = this;
	this.instance_2.setTransform(189.2,222.2,0.073,0.075,0,0,0,338.7,243.9);
	this.instance_2.alpha = 0.32;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(12).to({_off:false},0).wait(1).to({regY:240.6,scaleY:0.08,x:187.9,y:221.1},0).wait(1).to({scaleX:0.08,scaleY:0.08,x:183.6,y:218.3},0).wait(1).to({scaleX:0.08,scaleY:0.08,x:175.6,y:213.1},0).wait(1).to({scaleX:0.08,scaleY:0.09,x:163.6,y:205.1},0).wait(1).to({scaleX:0.09,scaleY:0.09,x:148.1,y:194.9},0).wait(1).to({scaleX:0.1,scaleY:0.1,x:132.3,y:184.6},0).wait(1).to({scaleX:0.1,scaleY:0.11,x:121.4,y:177.5},0).wait(1).to({regX:338.4,regY:243.2,scaleX:0.1,scaleY:0.11,x:117.9,y:175.5},0).to({x:112.9,y:167.5},79).wait(1));

	// cloud
	this.instance_3 = new lib.cloud();
	this.instance_3.parent = this;
	this.instance_3.setTransform(189.2,172.2,0.058,0.087,0,0,0,338.6,243.3);
	this.instance_3.alpha = 0.32;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(5).to({_off:false},0).wait(1).to({regX:338.7,regY:240.6,scaleX:0.06,x:187.6,y:171.3},0).wait(1).to({scaleX:0.06,scaleY:0.09,x:182.5,y:169.1},0).wait(1).to({scaleX:0.07,scaleY:0.09,x:173.1,y:165.1},0).wait(1).to({scaleX:0.08,scaleY:0.1,x:158.7,y:159.1},0).wait(1).to({scaleX:0.09,scaleY:0.11,x:140.3,y:151.3},0).wait(1).to({scaleX:0.11,scaleY:0.12,x:121.6,y:143.3},0).wait(1).to({scaleX:0.12,scaleY:0.12,x:108.6,y:137.9},0).wait(1).to({regX:338.1,regY:243.1,scaleX:0.12,scaleY:0.12,x:104.4,y:136.4},0).to({x:101.4,y:131.4},86).wait(1));

	// cloud
	this.instance_4 = new lib.cloud();
	this.instance_4.parent = this;
	this.instance_4.setTransform(189.2,202.2,0.092,0.135,0,0,180,338.2,242.5);
	this.instance_4.alpha = 0.32;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({_off:false},0).wait(1).to({regX:338.7,regY:240.6,scaleX:0.09,scaleY:0.14,x:188.5,y:200.3},0).wait(1).to({scaleX:0.1,scaleY:0.14,x:186.5,y:194.9},0).wait(1).to({scaleX:0.1,scaleY:0.14,x:182.7,y:184.9},0).wait(1).to({scaleX:0.12,scaleY:0.14,x:177,y:169.8},0).wait(1).to({scaleX:0.13,scaleY:0.14,x:169.6,y:150.3},0).wait(1).to({scaleX:0.14,scaleY:0.15,x:162.1,y:130.7},0).wait(1).to({scaleX:0.15,scaleY:0.15,x:157,y:117},0).wait(1).to({regX:338.4,regY:242.3,scaleX:0.16,scaleY:0.15,x:155.4,y:112.8},0).to({x:147.4,y:104.8},90).wait(1));

	// cloud
	this.instance_5 = new lib.cloud();
	this.instance_5.parent = this;
	this.instance_5.setTransform(189.2,202.1,0.068,0.106,0,0,180,337.9,242.6);
	this.instance_5.alpha = 0.32;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(6).to({_off:false},0).wait(1).to({regX:338.7,regY:240.6,scaleX:0.07,x:190.4,y:201.3},0).wait(1).to({scaleX:0.07,scaleY:0.11,x:194.6,y:199.4},0).wait(1).to({scaleX:0.08,scaleY:0.11,x:202.4,y:195.9},0).wait(1).to({scaleX:0.09,scaleY:0.11,x:214.2,y:190.5},0).wait(1).to({scaleX:0.1,scaleY:0.12,x:229.3,y:183.5},0).wait(1).to({scaleX:0.12,scaleY:0.12,x:244.6,y:176.6},0).wait(1).to({scaleX:0.12,scaleY:0.12,x:255.2,y:171.7},0).wait(1).to({regX:338.4,regY:242,scaleX:0.13,scaleY:0.12,x:258.9,y:170.4},0).to({x:260.9,y:144.4},85).wait(1));

	// cloud
	this.instance_6 = new lib.cloud();
	this.instance_6.parent = this;
	this.instance_6.setTransform(189.2,142.1,0.075,0.075,0,0,0,338.8,242.3);
	this.instance_6.alpha = 0.32;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(3).to({_off:false},0).wait(1).to({regX:338.7,regY:240.6,x:190.4,y:141},0).wait(1).to({scaleX:0.08,scaleY:0.08,x:194.7,y:138.1},0).wait(1).to({scaleX:0.08,scaleY:0.08,x:202.6,y:132.5},0).wait(1).to({scaleX:0.09,scaleY:0.09,x:214.6,y:124},0).wait(1).to({scaleX:0.09,scaleY:0.09,x:230.1,y:113.1},0).wait(1).to({scaleX:0.1,scaleY:0.1,x:245.7,y:102.1},0).wait(1).to({scaleX:0.11,scaleY:0.11,x:256.5,y:94.4},0).wait(1).to({regX:338.4,regY:242,scaleX:0.11,scaleY:0.11,x:260.1,y:92.1},0).to({x:278.1,y:91.8},88).wait(1));

	// Слой 12
	this.instance_7 = new lib.mol("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(189.3,144.8,0.543,0.871,0,0,0,48.2,58.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1).to({regY:87.4,scaleX:0.55,x:188.7,y:169.8,startPosition:1},0).wait(1).to({scaleX:0.55,scaleY:0.87,x:186.9,y:168.8,startPosition:2},0).wait(1).to({scaleX:0.56,scaleY:0.88,x:183.6,y:167,startPosition:3},0).wait(1).to({scaleX:0.57,scaleY:0.88,x:178.7,y:164.2,startPosition:4},0).wait(1).to({scaleX:0.59,scaleY:0.89,x:171.7,y:160.3,startPosition:5},0).wait(1).to({scaleX:0.62,scaleY:0.89,x:162.4,y:155,startPosition:6},0).wait(1).to({scaleX:0.65,scaleY:0.9,x:150.3,y:148.2,startPosition:7},0).wait(1).to({scaleX:0.7,scaleY:0.91,x:134.9,y:139.5,startPosition:8},0).wait(1).to({scaleX:0.75,scaleY:0.93,x:116.1,y:128.9,startPosition:9},0).wait(1).to({scaleX:0.81,scaleY:0.95,x:94.5,y:116.7,startPosition:10},0).wait(1).to({scaleX:0.88,scaleY:0.97,x:72,y:104,startPosition:11},0).wait(1).to({scaleX:0.93,scaleY:0.98,x:52.3,y:92.9,startPosition:12},0).wait(1).to({scaleX:0.97,scaleY:0.99,x:38.2,y:85,startPosition:13},0).wait(1).to({scaleX:0.99,scaleY:1,x:30.4,y:80.6,startPosition:14},0).wait(1).to({regX:48.1,regY:58,scaleX:1,scaleY:1,x:28,y:50.1,startPosition:15},0).to({x:23,y:46.1,mode:"independent"},84).wait(1));

	// cloud
	this.instance_8 = new lib.cloud();
	this.instance_8.parent = this;
	this.instance_8.setTransform(189.2,142.1,0.075,0.075,0,0,0,338.8,242.3);
	this.instance_8.alpha = 0.23;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(5).to({_off:false},0).wait(1).to({regX:338.7,regY:240.6,x:188.9,y:139.8},0).wait(1).to({scaleX:0.08,scaleY:0.08,x:188.1,y:132.9},0).wait(1).to({scaleX:0.08,scaleY:0.08,x:186.5,y:119.9},0).wait(1).to({scaleX:0.09,scaleY:0.09,x:184.3,y:100.3},0).wait(1).to({scaleX:0.09,scaleY:0.09,x:181.3,y:75},0).wait(1).to({scaleX:0.1,scaleY:0.1,x:178.3,y:49.4},0).wait(1).to({scaleX:0.11,scaleY:0.11,x:176.2,y:31.7},0).wait(1).to({regX:338.4,regY:242,scaleX:0.11,scaleY:0.11,x:175.6,y:26.1},0).to({y:20.1},86).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(163.1,114.5,52.3,60);


(lib.smile6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.instance = new lib.r4();
	this.instance.parent = this;
	this.instance.setTransform(16.6,14.1,1,1,0,0,0,7.9,2.1);

	this.instance_1 = new lib.brov();
	this.instance_1.parent = this;
	this.instance_1.setTransform(12.2,9.7,1,0.912,0,24,180,1.8,1.4);

	this.instance_2 = new lib.brov();
	this.instance_2.parent = this;
	this.instance_2.setTransform(20.7,9.7,1,0.912,0,-24,0,1.8,1.4);

	this.instance_3 = new lib.mouth07();
	this.instance_3.parent = this;
	this.instance_3.setTransform(16.5,19.8,0.218,0.196,-3,0,0,26.9,11.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AAkANQgEgEAAgLQAEAJAKgBQAKgBACgGQADgIgDgCIAUgJQAGARgJAMQgJAMgLAAQgLAAgIgIgAhHANQgIgIAAgKQAAgFACgFQAAAIAEAFQAEAEAGAAQAFAAAEgEQAEgEABgFIAXALQgCAHgGAGQgIAIgLAAQgLAAgHgIg");
	this.shape.setTransform(16.5,14.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_3,p:{regX:26.9,regY:11.5,scaleX:0.218,scaleY:0.196,x:16.5,y:19.8}},{t:this.instance_2,p:{y:9.7}},{t:this.instance_1,p:{y:9.7}},{t:this.instance}]}).to({state:[{t:this.shape},{t:this.instance_3,p:{regX:27.1,regY:11.6,scaleX:0.24,scaleY:0.215,x:16.9,y:20.4}},{t:this.instance_2,p:{y:10.3}},{t:this.instance_1,p:{y:10.3}}]},1).wait(3));

	// Слой 1
	this.instance_4 = new lib.share1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_5 = new lib.smile1anim();
	this.instance_5.parent = this;
	this.instance_5.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_6 = new lib.share2();
	this.instance_6.parent = this;
	this.instance_6.setTransform(16,16,1,1,0,0,0,16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACfAAQAABCgvAuQguAvhCAAQhBAAgvgvQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAvAvAABBg");
	this.shape_1.setTransform(16,16);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999C28").s().p("AhwBwQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAwAvgBBBQABBCgwAuQguAwhCgBQhBABgvgwg");
	this.shape_2.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4}]}).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.shape_2},{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-1.3,34.5,34.5);


(lib.smile5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.instance = new lib.r3();
	this.instance.parent = this;
	this.instance.setTransform(15.5,23.2,1,1,0,0,0,6,1.9);

	this.instance_1 = new lib.brov2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(20.7,8.8,1,1,0,180,0,1.9,1.1);

	this.instance_2 = new lib.brov2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(10.4,8.8,1,1,165,0,0,1.9,1.1);

	this.instance_3 = new lib.Symbol5();
	this.instance_3.parent = this;
	this.instance_3.setTransform(20,13.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_4 = new lib.Symbol6();
	this.instance_4.parent = this;
	this.instance_4.setTransform(21.2,14.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_5 = new lib.Symbol5();
	this.instance_5.parent = this;
	this.instance_5.setTransform(9.6,13.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_6 = new lib.Symbol6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(10.7,14.8,0.433,0.433,0,0,0,6.4,6.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AAdAFQgkgNgfAOQgfAQAQgQQAQgQAbgDQAZgCASAJQASAHAHAKQABABAAAAQAAABAAAAQAAABAAAAQgBAAgBAAQgFAAgXgJg");
	this.shape.setTransform(16,22.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6,p:{x:10.7,y:14.8}},{t:this.instance_5,p:{x:9.6,y:13.8}},{t:this.instance_4,p:{x:21.2,y:14.8}},{t:this.instance_3,p:{x:20,y:13.8}},{t:this.instance_2,p:{regY:1.1,rotation:165,x:10.4,y:8.8}},{t:this.instance_1,p:{x:20.7}},{t:this.instance}]}).to({state:[{t:this.shape},{t:this.instance_6,p:{x:10.5,y:15.4}},{t:this.instance_5,p:{x:9.4,y:14.4}},{t:this.instance_4,p:{x:21.5,y:15.2}},{t:this.instance_3,p:{x:20.3,y:14.2}},{t:this.instance_2,p:{regY:1,rotation:180,x:10,y:8.9}},{t:this.instance_1,p:{x:21.1}}]},1).wait(3));

	// Слой 1
	this.instance_7 = new lib.share1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_8 = new lib.smile1anim();
	this.instance_8.parent = this;
	this.instance_8.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_9 = new lib.share2();
	this.instance_9.parent = this;
	this.instance_9.setTransform(16,16,1,1,0,0,0,16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACfAAQAABCgvAuQguAvhCAAQhBAAgvgvQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAvAvAABBg");
	this.shape_1.setTransform(16,16);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999C28").s().p("AhwBwQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAwAvgBBBQABBCgwAuQguAwhCgBQhBABgvgwg");
	this.shape_2.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7}]}).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.shape_2},{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-1.3,34.5,34.5);


(lib.smile4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.instance = new lib.r2();
	this.instance.parent = this;
	this.instance.setTransform(15.8,22.7,1,1,0,0,0,4,0.8);

	this.instance_1 = new lib.brov2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(20.7,8.8,1,1,0,0,180,1.9,1.1);

	this.instance_2 = new lib.brov2();
	this.instance_2.parent = this;
	this.instance_2.setTransform(10.4,8.8,1,1,0,0,0,1.9,1.1);

	this.instance_3 = new lib.Symbol5();
	this.instance_3.parent = this;
	this.instance_3.setTransform(20,13.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_4 = new lib.Symbol6();
	this.instance_4.parent = this;
	this.instance_4.setTransform(21.2,14.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_5 = new lib.Symbol5();
	this.instance_5.parent = this;
	this.instance_5.setTransform(9.6,13.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_6 = new lib.Symbol6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(10.7,14.8,0.433,0.433,0,0,0,6.4,6.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgcABQAAgGAKgBIAagBQARAAADAHQAEAIgOAAIgdAAIgCAAQgQAAABgHg");
	this.shape.setTransform(15.7,23.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6,p:{x:10.7,y:14.8}},{t:this.instance_5,p:{x:9.6,y:13.8}},{t:this.instance_4,p:{x:21.2,y:14.8}},{t:this.instance_3,p:{x:20,y:13.8}},{t:this.instance_2,p:{regY:1.1,rotation:0,x:10.4,y:8.8}},{t:this.instance_1,p:{regY:1.1,skewX:0,skewY:180,x:20.7,y:8.8}},{t:this.instance}]}).to({state:[{t:this.shape},{t:this.instance_6,p:{x:10.5,y:15.2}},{t:this.instance_5,p:{x:9.4,y:14.2}},{t:this.instance_4,p:{x:21.4,y:15.2}},{t:this.instance_3,p:{x:20.2,y:14.2}},{t:this.instance_2,p:{regY:1.2,rotation:15,x:9.9,y:9.2}},{t:this.instance_1,p:{regY:1.2,skewX:-15,skewY:165,x:21.1,y:9.3}}]},1).wait(3));

	// Слой 1
	this.instance_7 = new lib.share1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_8 = new lib.smile1anim();
	this.instance_8.parent = this;
	this.instance_8.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_9 = new lib.share2();
	this.instance_9.parent = this;
	this.instance_9.setTransform(16,16,1,1,0,0,0,16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACfAAQAABCgvAuQguAvhCAAQhBAAgvgvQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAvAvAABBg");
	this.shape_1.setTransform(16,16);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999C28").s().p("AhwBwQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAwAvgBBBQABBCgwAuQguAwhCgBQhBABgvgwg");
	this.shape_2.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7}]}).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.shape_2},{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-1.3,34.5,34.5);


(lib.smile3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.instance = new lib.brov2();
	this.instance.parent = this;
	this.instance.setTransform(20.7,8.8,1,1,0,0,180,1.9,1.1);

	this.instance_1 = new lib.brov2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(10.4,8.8,1,1,0,0,0,1.9,1.1);

	this.instance_2 = new lib.Symbol5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(20,13.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_3 = new lib.Symbol6();
	this.instance_3.parent = this;
	this.instance_3.setTransform(21.2,14.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_4 = new lib.Symbol5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(9.6,13.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_5 = new lib.Symbol6();
	this.instance_5.parent = this;
	this.instance_5.setTransform(10.7,14.8,0.433,0.433,0,0,0,6.4,6.4);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#3C070E").s().p("AgXAJQgZgEgGgKQgGgKAZALQAYAJAlgIQAlgJgIAIQgIAHgXAFQgNADgMAAQgLAAgLgCg");
	this.shape.setTransform(16.3,22.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape,p:{x:16.3,y:22.8}},{t:this.instance_5,p:{x:10.7,y:14.8}},{t:this.instance_4,p:{x:9.6,y:13.8}},{t:this.instance_3,p:{x:21.2,y:14.8}},{t:this.instance_2,p:{x:20,y:13.8}},{t:this.instance_1,p:{x:10.4,y:8.8}},{t:this.instance,p:{x:20.7}}]}).to({state:[{t:this.shape,p:{x:16.5,y:23.6}},{t:this.instance_5,p:{x:10.5,y:15.4}},{t:this.instance_4,p:{x:9.4,y:14.4}},{t:this.instance_3,p:{x:22.4,y:15.2}},{t:this.instance_2,p:{x:21.2,y:14.2}},{t:this.instance_1,p:{x:9.8,y:9}},{t:this.instance,p:{x:22.3}}]},1).wait(3));

	// Слой 1
	this.instance_6 = new lib.share1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_7 = new lib.smile1anim();
	this.instance_7.parent = this;
	this.instance_7.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_8 = new lib.share2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(16,16,1,1,0,0,0,16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACfAAQAABCgvAuQguAvhCAAQhBAAgvgvQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAvAvAABBg");
	this.shape_1.setTransform(16,16);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#999C28").s().p("AhwBwQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAwAvgBBBQABBCgwAuQguAwhCgBQhBABgvgwg");
	this.shape_2.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6}]}).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.shape_2},{t:this.shape_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-1.3,34.5,34.5);


(lib.smile2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Слой 2
	this.instance = new lib.brov2();
	this.instance.parent = this;
	this.instance.setTransform(20.7,7.2,1,1,0,0,180,1.9,1.1);

	this.instance_1 = new lib.brov2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(10.4,7.2,1,1,0,0,0,1.9,1.1);

	this.instance_2 = new lib.Symbol5();
	this.instance_2.parent = this;
	this.instance_2.setTransform(20,12.2,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_3 = new lib.Symbol6();
	this.instance_3.parent = this;
	this.instance_3.setTransform(21.2,13.2,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_4 = new lib.Symbol5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(9.6,12.2,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_5 = new lib.Symbol6();
	this.instance_5.parent = this;
	this.instance_5.setTransform(10.7,13.2,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_6 = new lib.mouth();
	this.instance_6.parent = this;
	this.instance_6.setTransform(15.8,23.2,0.306,0.306,0,0,0,16.7,14.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_6,p:{y:23.2}},{t:this.instance_5,p:{x:10.7}},{t:this.instance_4,p:{x:9.6}},{t:this.instance_3,p:{x:21.2}},{t:this.instance_2,p:{x:20}},{t:this.instance_1,p:{x:10.4,y:7.2}},{t:this.instance,p:{x:20.7,y:7.2}}]}).to({state:[{t:this.instance_6,p:{y:24.2}},{t:this.instance_5,p:{x:10.1}},{t:this.instance_4,p:{x:9}},{t:this.instance_3,p:{x:21.7}},{t:this.instance_2,p:{x:20.5}},{t:this.instance_1,p:{x:8.9,y:7}},{t:this.instance,p:{x:21.9,y:7}}]},1).wait(3));

	// Слой 1
	this.instance_7 = new lib.share1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_8 = new lib.smile1anim();
	this.instance_8.parent = this;
	this.instance_8.setTransform(16,16,1,1,0,0,0,16,16);

	this.instance_9 = new lib.share2();
	this.instance_9.parent = this;
	this.instance_9.setTransform(16,16,1,1,0,0,0,16,16);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("ACfAAQAABCgvAuQguAvhCAAQhBAAgvgvQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAvAvAABBg");
	this.shape.setTransform(16,16);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#999C28").s().p("AhwBwQgvguAAhCQAAhBAvgvQAvgvBBAAQBCAAAuAvQAwAvgBBBQABBCgwAuQguAwhCgBQhBABgvgwg");
	this.shape_1.setTransform(16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_7}]}).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.shape_1},{t:this.shape}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1.3,-1.3,34.5,34.5);


(lib.heart1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// heart
	this.instance = new lib.heart();
	this.instance.parent = this;
	this.instance.setTransform(254.2,152.6,0.125,0.125,15,0,0,111.8,108.5);
	this.instance.alpha = 0.328;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:111,rotation:16.9,x:255.7,y:150.6},0).wait(1).to({rotation:28.3,x:265.3,y:139.1},0).wait(1).to({rotation:41.9,x:276.9,y:125.4},0).wait(1).to({rotation:49.1,x:283,y:118.1},0).wait(1).to({rotation:52.5,x:285.8,y:114.7},0).wait(1).to({rotation:55.6,x:288.5,y:111.5},0).wait(1).to({rotation:60.2,x:292.4,y:106.9},0).wait(1).to({rotation:67.4,x:298.5,y:99.6},0).wait(1).to({rotation:73.1,x:303.3,y:93.9},0).wait(1).to({regX:112,regY:108.2,rotation:75,x:305,y:92.1},0).to({regX:111.8,regY:108.5,rotation:15,x:354.9,y:78,alpha:0.68},89).wait(1));

	// heart
	this.instance_1 = new lib.heart();
	this.instance_1.parent = this;
	this.instance_1.setTransform(121.8,184.6,0.125,0.125,-60,0,0,111.2,109.2);
	this.instance_1.alpha = 0.328;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).wait(1).to({regX:111,regY:108.5,rotation:-59.1,x:120.2,y:183.9},0).wait(1).to({rotation:-53.4,x:111.6,y:180.2},0).wait(1).to({rotation:-46.6,x:101.3,y:175.8},0).wait(1).to({rotation:-42.9,x:95.8,y:173.5},0).wait(1).to({rotation:-41.3,x:93.3,y:172.4},0).wait(1).to({rotation:-39.7,x:90.9,y:171.3},0).wait(1).to({rotation:-37.4,x:87.4,y:169.9},0).wait(1).to({rotation:-33.8,x:82,y:167.6},0).wait(1).to({rotation:-31,x:77.7,y:165.7},0).wait(1).to({regX:111.8,regY:109,rotation:-30,x:76.3,y:165.2},0).to({regX:111,regY:109.3,rotation:-45,x:-1.3,y:99.7,alpha:0.73},87).wait(1));

	// heart
	this.instance_2 = new lib.heart();
	this.instance_2.parent = this;
	this.instance_2.setTransform(226.7,144.6,0.111,0.111,0,0,0,111.8,109.1);
	this.instance_2.alpha = 0.172;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(2).to({_off:false},0).wait(1).to({regX:111,regY:108.5,x:226.6,y:140.6},0).wait(1).to({x:226.9,y:116.9},0).wait(1).to({x:227.2,y:88.6},0).wait(1).to({x:227.4,y:73.6},0).wait(1).to({y:66.5},0).wait(1).to({x:227.5,y:60},0).wait(1).to({x:227.6,y:50.5},0).wait(1).to({x:227.8,y:35.5},0).wait(1).to({x:227.9,y:23.7},0).wait(1).to({regX:111.8,regY:109.1,x:228.1,y:19.8},0).to({y:0.8},87,cjs.Ease.get(1)).wait(1));

	// heart
	this.instance_3 = new lib.heart();
	this.instance_3.parent = this;
	this.instance_3.setTransform(246.6,110.9,0.348,0.348,15,0,0,111.8,109.4);
	this.instance_3.alpha = 0.172;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(1).to({regX:111,regY:108.5,y:109.4},0).wait(1).to({x:247.8,y:102.9},0).wait(1).to({x:249.1,y:95.1},0).wait(1).to({x:249.9,y:91},0).wait(1).to({x:250.2,y:89.1},0).wait(1).to({x:250.5,y:87.3},0).wait(1).to({x:251,y:84.7},0).wait(1).to({x:251.7,y:80.6},0).wait(1).to({x:252.3,y:77.4},0).wait(1).to({regX:111.8,regY:109.4,x:252.6,y:76.6},0).to({y:62.6},89).wait(1));

	// heart
	this.instance_4 = new lib.heart();
	this.instance_4.parent = this;
	this.instance_4.setTransform(260.5,189.7,0.187,0.187,60,0,0,111.6,109);
	this.instance_4.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({regX:111,regY:108.5,rotation:56.4,x:261.5,y:185.5},0).wait(1).to({rotation:36.1,x:267.6,y:163},0).wait(1).to({rotation:19.7,x:272.4,y:145},0).wait(1).to({rotation:16.1,x:273.4,y:141},0).wait(1).to({rotation:17.6,x:273,y:142.7},0).wait(1).to({rotation:19.1,x:272.5,y:144.3},0).wait(1).to({rotation:17.4,x:273,y:142.4},0).wait(1).to({rotation:8.4,x:275.7,y:132.5},0).wait(1).to({rotation:2.1,x:277.6,y:125.5},0).wait(1).to({regX:111.5,regY:109,rotation:0,x:278.3,y:123.3},0).wait(1).to({regX:111,regY:108.5,x:278.2,y:123.2},0).wait(3).to({y:123.1},0).wait(1).to({rotation:0.1},0).wait(2).to({rotation:0.2,y:123},0).wait(1).to({y:122.9},0).wait(1).to({rotation:0.3},0).wait(1).to({y:122.7},0).wait(1).to({rotation:0.4},0).wait(1).to({rotation:0.5,x:278.3,y:122.5},0).wait(1).to({rotation:0.6,y:122.4},0).wait(1).to({rotation:0.7,x:278.2,y:122.3},0).wait(1).to({rotation:0.8,x:278.3,y:122.2},0).wait(1).to({rotation:0.9,y:122},0).wait(1).to({rotation:1,y:121.9},0).wait(1).to({rotation:1.1,y:121.7},0).wait(1).to({rotation:1.3,y:121.5},0).wait(1).to({rotation:1.4,y:121.3},0).wait(1).to({rotation:1.6,x:278.4,y:121.1},0).wait(1).to({rotation:1.7,y:120.9},0).wait(1).to({rotation:1.9,y:120.7},0).wait(1).to({rotation:2.1,y:120.4},0).wait(1).to({rotation:2.3,x:278.5,y:120.1},0).wait(1).to({rotation:2.5,y:119.8},0).wait(1).to({rotation:2.7,y:119.5},0).wait(1).to({rotation:3,y:119.2},0).wait(1).to({rotation:3.2,y:118.8},0).wait(1).to({rotation:3.5,x:278.6,y:118.5},0).wait(1).to({rotation:3.7,y:118.2},0).wait(1).to({rotation:4,y:117.8},0).wait(1).to({rotation:4.3,x:278.7,y:117.4},0).wait(1).to({rotation:4.6,y:116.9},0).wait(1).to({rotation:4.9,y:116.5},0).wait(1).to({rotation:5.3,x:278.8,y:116},0).wait(1).to({rotation:5.6,y:115.6},0).wait(1).to({rotation:6,x:278.9,y:115.1},0).wait(1).to({rotation:6.4,y:114.5},0).wait(1).to({rotation:6.8,y:114},0).wait(1).to({rotation:7.2,x:279,y:113.4},0).wait(1).to({rotation:7.7,y:112.8},0).wait(1).to({rotation:8.1,x:279.1,y:112.2},0).wait(1).to({rotation:8.6,x:279.2,y:111.6},0).wait(1).to({rotation:9.1,y:110.9},0).wait(1).to({rotation:9.6,y:110.2},0).wait(1).to({rotation:10.1,x:279.3,y:109.6},0).wait(1).to({rotation:10.6,y:108.8},0).wait(1).to({rotation:11.2,x:279.4,y:108},0).wait(1).to({rotation:11.8,y:107.3},0).wait(1).to({rotation:12.4,x:279.5,y:106.4},0).wait(1).to({rotation:13,x:279.6,y:105.6},0).wait(1).to({rotation:13.6,x:279.7,y:104.7},0).wait(1).to({rotation:14.3,y:103.9},0).wait(1).to({rotation:14.9,x:279.8,y:103},0).wait(1).to({rotation:15.6,y:102.1},0).wait(1).to({rotation:16.3,x:280,y:101.2},0).wait(1).to({rotation:16.9,y:100.3},0).wait(1).to({rotation:17.6,x:280.1,y:99.4},0).wait(1).to({rotation:18.3,x:280.2,y:98.3},0).wait(1).to({rotation:19,y:97.4},0).wait(1).to({rotation:19.7,x:280.3,y:96.5},0).wait(1).to({rotation:20.4,x:280.4,y:95.5},0).wait(1).to({rotation:21.1,y:94.6},0).wait(1).to({rotation:21.8,x:280.5,y:93.7},0).wait(1).to({rotation:22.5,x:280.6,y:92.8},0).wait(1).to({rotation:23.1,y:91.9},0).wait(1).to({rotation:23.7,x:280.7,y:91.1},0).wait(1).to({rotation:24.3,y:90.3},0).wait(1).to({rotation:24.9,x:280.9,y:89.5},0).wait(1).to({rotation:25.5,y:88.8},0).wait(1).to({rotation:26,y:88},0).wait(1).to({rotation:26.5,x:281,y:87.4},0).wait(1).to({rotation:26.9,y:86.8},0).wait(1).to({rotation:27.3,x:281.1,y:86.2},0).wait(1).to({rotation:27.7,x:281.2,y:85.7},0).wait(1).to({rotation:28.1,x:281.1,y:85.2},0).wait(1).to({rotation:28.4,x:281.2,y:84.8},0).wait(1).to({rotation:28.7,y:84.4},0).wait(1).to({rotation:29,y:84},0).wait(1).to({rotation:29.2,x:281.3,y:83.7},0).wait(1).to({rotation:29.4,y:83.4},0).wait(1).to({rotation:29.6,y:83.2},0).wait(1).to({rotation:29.7,y:83},0).wait(1).to({rotation:29.8,y:82.9},0).wait(1).to({rotation:29.9,y:82.7},0).wait(1).to({rotation:30,x:281.4},0).wait(2).to({regX:111.6,regY:109.2,y:82.8},0).wait(1));

	// heart
	this.instance_5 = new lib.heart();
	this.instance_5.parent = this;
	this.instance_5.setTransform(159.3,123.1,0.369,0.369,0,0,0,111.5,108.7);
	this.instance_5.alpha = 0.301;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(3).to({_off:false},0).wait(1).to({regX:111,regY:108.5,x:159,y:121.4},0).wait(1).to({y:112.2},0).wait(1).to({x:158.9,y:104.9},0).wait(1).to({y:103.3},0).wait(1).to({y:104},0).wait(1).to({y:104.6},0).wait(1).to({y:103.9},0).wait(1).to({x:158.8,y:99.8},0).wait(1).to({y:97},0).wait(1).to({regX:111.5,regY:108.7,x:159,y:96.1},0).to({regY:108.8,rotation:-30,x:159.1,y:65.1},86).wait(1));

	// heart
	this.instance_6 = new lib.heart();
	this.instance_6.parent = this;
	this.instance_6.setTransform(119.6,189.5,0.161,0.161,-60,0,0,111.5,109.2);
	this.instance_6.alpha = 0.121;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1).to({regX:111,regY:108.5,rotation:-57.3,x:118.7,y:179.7},0).wait(1).to({rotation:-42,x:114.8,y:124.8},0).wait(1).to({rotation:-29.8,x:111.6,y:80.6},0).wait(1).to({rotation:-27,x:111,y:70.8},0).wait(1).to({rotation:-28.2,x:111.2,y:75},0).wait(1).to({rotation:-29.3,x:111.5,y:79},0).wait(1).to({rotation:-28,x:111.2,y:74.3},0).wait(1).to({rotation:-21.3,x:109.5,y:50.1},0).wait(1).to({rotation:-16.6,x:108.2,y:32.9},0).wait(1).to({regX:111.3,regY:108.8,rotation:-15,x:107.9,y:27.4},0).to({y:8.4},89).wait(1));

	// heart
	this.instance_7 = new lib.heart();
	this.instance_7.parent = this;
	this.instance_7.setTransform(236,199.5,0.262,0.262,75,0,0,112,108.9);
	this.instance_7.alpha = 0.18;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(10).to({_off:false},0).wait(1).to({regX:111,regY:108.5,rotation:72.3,x:237.3,y:198.1},0).wait(1).to({rotation:57,x:244.7,y:191.9},0).wait(1).to({rotation:44.8,x:250.7,y:187},0).wait(1).to({rotation:42,x:252,y:185.8},0).wait(1).to({rotation:43.2,x:251.4,y:186.3},0).wait(1).to({rotation:44.3,x:250.9,y:186.7},0).wait(1).to({rotation:43,x:251.5,y:186.2},0).wait(1).to({rotation:36.3,x:254.8,y:183.5},0).wait(1).to({rotation:31.6,x:257.1,y:181.5},0).wait(1).to({regX:111.7,regY:109,rotation:30,x:258,y:181.2},0).to({regY:109.4,rotation:15,x:276.1,y:136.8},79).wait(1));

	// heart
	this.instance_8 = new lib.heart();
	this.instance_8.parent = this;
	this.instance_8.setTransform(136.2,194.5,0.161,0.161,-75,0,0,111.2,108.7);
	this.instance_8.alpha = 0.301;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(7).to({_off:false},0).wait(1).to({regX:111,regY:108.5,rotation:-70.5,x:135.3,y:193.8},0).wait(1).to({rotation:-45.1,x:130.1,y:189.1},0).wait(1).to({rotation:-24.6,x:125.9,y:185.5},0).wait(1).to({rotation:-20.1,x:125,y:184.7},0).wait(1).to({rotation:-22,x:125.4,y:185},0).wait(1).to({rotation:-23.9,x:125.8,y:185.4},0).wait(1).to({rotation:-21.7,x:125.3,y:185},0).wait(1).to({rotation:-10.5,x:123.1,y:183},0).wait(1).to({rotation:-2.6,x:121.4,y:181.5},0).wait(1).to({regX:111.4,regY:108.6,rotation:0,x:121,y:181.1},0).to({regX:111.3,regY:108.7,rotation:-45,x:106.7,y:138.8},82).wait(1));

	// heart
	this.instance_9 = new lib.heart();
	this.instance_9.parent = this;
	this.instance_9.setTransform(183.4,189.5,0.161,0.161,0,0,0,111.4,108.6);
	this.instance_9.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({y:56.1},10,cjs.Ease.get(1)).to({y:42.1},89).wait(1));

	// heart
	this.instance_10 = new lib.heart();
	this.instance_10.parent = this;
	this.instance_10.setTransform(212.1,189.5,0.206,0.206,0,0,0,111.2,108.5);
	this.instance_10.alpha = 0.398;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({y:115.1},10,cjs.Ease.get(1)).to({regX:111.3,regY:108.7,rotation:3.3,y:98.7},10).to({regX:111.5,rotation:30,x:212.2,y:84.2},79,cjs.Ease.get(-1)).wait(1));

	// heart
	this.instance_11 = new lib.heart();
	this.instance_11.parent = this;
	this.instance_11.setTransform(127.4,189.5,0.261,0.261,-60,0,0,111.2,108.8);
	this.instance_11.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(1).to({regX:111,regY:108.5,rotation:-57.3,x:126.8,y:184.9},0).wait(1).to({rotation:-42,x:124.1,y:159.8},0).wait(1).to({rotation:-29.8,x:121.7,y:139.5},0).wait(1).to({rotation:-27,x:121.3,y:135},0).wait(1).to({rotation:-28.2,x:121.5,y:136.9},0).wait(1).to({rotation:-29.3,x:121.7,y:138.8},0).wait(1).to({rotation:-28,x:121.4,y:136.6},0).wait(1).to({rotation:-21.3,x:120.2,y:125.5},0).wait(1).to({rotation:-16.6,x:119.4,y:117.7},0).wait(1).to({regX:111.4,regY:108.7,rotation:-15,x:119.2,y:115.2},0).to({regX:111,regY:109,rotation:-75,x:34,y:51},89).wait(1));

	// heart
	this.instance_12 = new lib.heart();
	this.instance_12.parent = this;
	this.instance_12.setTransform(263.9,189.6,0.261,0.261,45,0,0,111.2,108.5);
	this.instance_12.alpha = 0.301;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(1).to({regX:111,rotation:42.3,x:265.7,y:181},0).wait(1).to({rotation:27,x:275.9,y:133.9},0).wait(1).to({rotation:14.8,x:284.1,y:95.8},0).wait(1).to({rotation:12,x:285.9,y:87.3},0).wait(1).to({rotation:13.2,x:285.2,y:91.1},0).wait(1).to({rotation:14.3,x:284.4,y:94.4},0).wait(1).to({rotation:13,x:285.3,y:90.4},0).wait(1).to({rotation:6.3,x:289.8,y:69.6},0).wait(1).to({rotation:1.6,x:293,y:54.9},0).wait(1).to({regX:111.2,rotation:0,x:294.1,y:50.1},0).to({regY:108.3,rotation:75,x:328,y:21.8},89).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(109.5,91.3,171,114.9);


(lib.angy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Слой 1
	this.instance = new lib.angryclouds("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(13.2,-9.5,1,1,0,0,0,176.3,105);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(80).to({startPosition:80},0).to({alpha:0,mode:"single",startPosition:98},18).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,52.3,60);


(lib.heartt = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_99 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(99).call(this.frame_99).wait(1));

	// Слой 1
	this.instance = new lib.heart1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(13.2,-9.5,1,1,0,0,0,176.3,105);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(80).to({startPosition:80},0).to({alpha:0,mode:"single",startPosition:98},18).to({_off:true},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-52,-21,166.9,105.2);


// stage content:
(lib.Icecream = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{smile1:14,smile11:20,smile2:47,smile22:64,smile3:95,smile33:110,smile4:148,smile44:157,smile5:187,smile55:196,smile6:231,smile66:235});

	// timeline functions:
	this.frame_0 = function() {
		timeline = this;
		
		
		this.smile1.on("click", function () {
			timeline.gotoAndPlay("smile1");
		})
		
		this.smile2.on("click", function () {
			timeline.gotoAndPlay("smile2");
		})
		
		this.smile3.on("click", function () {
			timeline.gotoAndPlay("smile3");
		})
		
		this.smile4.on("click", function () {
			timeline.gotoAndPlay("smile4");
		})
		
		this.smile5.on("click", function () {
			timeline.gotoAndPlay("smile5");
		})
		
		this.smile6.on("click", function () {
			timeline.gotoAndPlay("smile6");
		})
	}
	this.frame_13 = function() {
		var timeline=this; 
		timeline.stop();
	}
	this.frame_21 = function() {
		heart.gotoAndPlay(1);
	}
	this.frame_36 = function() {
		var timeline=this; 
		timeline.stop(); 
		var delay = 5000; 
		var startTime=createjs.Ticker.getTime(); 
		createjs.Ticker.addEventListener('tick', f); 
		function f(e){ 
		    if(createjs.Ticker.getTime()-startTime>delay){ 
		       timeline.play(); 
		        createjs.Ticker.removeEventListener('tick' ,f); 
		    } 
		}
	}
	this.frame_37 = function() {
		timeline.gotoAndPlay("smile11");
	}
	this.frame_83 = function() {
		var timeline=this; 
		timeline.stop(); 
		var delay = 6000; 
		var startTime=createjs.Ticker.getTime(); 
		createjs.Ticker.addEventListener('tick', f); 
		function f(e){ 
		    if(createjs.Ticker.getTime()-startTime>delay){ 
		       timeline.play(); 
		        createjs.Ticker.removeEventListener('tick' ,f); 
		    } 
		}
	}
	this.frame_84 = function() {
		timeline.gotoAndPlay("smile22");
	}
	this.frame_133 = function() {
		var timeline=this; 
		timeline.stop(); 
		var delay = 5000; 
		var startTime=createjs.Ticker.getTime(); 
		createjs.Ticker.addEventListener('tick', f); 
		function f(e){ 
		    if(createjs.Ticker.getTime()-startTime>delay){ 
		       timeline.play(); 
		        createjs.Ticker.removeEventListener('tick' ,f); 
		    } 
		}
	}
	this.frame_134 = function() {
		timeline.gotoAndPlay("smile33");
	}
	this.frame_170 = function() {
		var timeline=this; 
		timeline.stop(); 
		var delay = 5000; 
		var startTime=createjs.Ticker.getTime(); 
		createjs.Ticker.addEventListener('tick', f); 
		function f(e){ 
		    if(createjs.Ticker.getTime()-startTime>delay){ 
		       timeline.play(); 
		        createjs.Ticker.removeEventListener('tick' ,f); 
		    } 
		}
	}
	this.frame_171 = function() {
		timeline.gotoAndPlay("smile44");
	}
	this.frame_221 = function() {
		var timeline=this; 
		timeline.stop(); 
		var delay = 5000; 
		var startTime=createjs.Ticker.getTime(); 
		createjs.Ticker.addEventListener('tick', f); 
		function f(e){ 
		    if(createjs.Ticker.getTime()-startTime>delay){ 
		       timeline.play(); 
		        createjs.Ticker.removeEventListener('tick' ,f); 
		    } 
		}
	}
	this.frame_222 = function() {
		timeline.gotoAndPlay("smile55");
	}
	this.frame_232 = function() {
		angry.gotoAndPlay(1);
	}
	this.frame_252 = function() {
		var timeline=this; 
		timeline.stop(); 
		var delay = 5000; 
		var startTime=createjs.Ticker.getTime(); 
		createjs.Ticker.addEventListener('tick', f); 
		function f(e){ 
		    if(createjs.Ticker.getTime()-startTime>delay){ 
		       timeline.play(); 
		        createjs.Ticker.removeEventListener('tick' ,f); 
		    } 
		}
	}
	this.frame_253 = function() {
		timeline.gotoAndPlay("smile66");
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(13).call(this.frame_13).wait(8).call(this.frame_21).wait(15).call(this.frame_36).wait(1).call(this.frame_37).wait(46).call(this.frame_83).wait(1).call(this.frame_84).wait(49).call(this.frame_133).wait(1).call(this.frame_134).wait(36).call(this.frame_170).wait(1).call(this.frame_171).wait(50).call(this.frame_221).wait(1).call(this.frame_222).wait(10).call(this.frame_232).wait(20).call(this.frame_252).wait(1).call(this.frame_253).wait(6));

	// Layer 1
	this.instance = new lib.heart();
	this.instance.parent = this;
	this.instance.setTransform(448.6,-511.6,1,1,0,0,0,111,108.5);
	this.instance.alpha = 0.602;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(259));

	// smile6
	this.smile6 = new lib.smile6();
	this.smile6.parent = this;
	this.smile6.setTransform(317.1,238,1,1,0,0,0,16,16);
	new cjs.ButtonHelper(this.smile6, 0, 1, 2, false, new lib.smile6(), 3);

	this.instance_1 = new lib.r4();
	this.instance_1.parent = this;
	this.instance_1.setTransform(317.7,236.1,1,1,0,0,0,7.9,2.1);

	this.instance_2 = new lib.brov();
	this.instance_2.parent = this;
	this.instance_2.setTransform(313.2,231.7,1,0.912,0,24,180,1.8,1.4);

	this.instance_3 = new lib.brov();
	this.instance_3.parent = this;
	this.instance_3.setTransform(321.7,231.7,1,0.912,0,-24,0,1.8,1.4);

	this.instance_4 = new lib.mouth07();
	this.instance_4.parent = this;
	this.instance_4.setTransform(317.6,241.8,0.218,0.196,-3,0,0,26.9,11.5);

	this.instance_5 = new lib.share3();
	this.instance_5.parent = this;
	this.instance_5.setTransform(317.1,238,1,1,0,0,0,16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.smile6}]}).to({state:[{t:this.instance_5},{t:this.instance_4},{t:this.instance_3},{t:this.instance_2},{t:this.instance_1}]},235).wait(24));

	// smile5
	this.smile5 = new lib.smile5();
	this.smile5.parent = this;
	this.smile5.setTransform(332.6,140,1,1,0,0,0,16,16);
	new cjs.ButtonHelper(this.smile5, 0, 1, 2, false, new lib.smile5(), 3);

	this.instance_6 = new lib.r3();
	this.instance_6.parent = this;
	this.instance_6.setTransform(332.1,147.2,1,1,0,0,0,6,1.9);

	this.instance_7 = new lib.brov2();
	this.instance_7.parent = this;
	this.instance_7.setTransform(337.3,132.8,1,1,0,180,0,1.9,1.1);

	this.instance_8 = new lib.brov2();
	this.instance_8.parent = this;
	this.instance_8.setTransform(327,132.8,1,1,165,0,0,1.9,1.1);

	this.instance_9 = new lib.Symbol5();
	this.instance_9.parent = this;
	this.instance_9.setTransform(336.6,137.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_10 = new lib.Symbol6();
	this.instance_10.parent = this;
	this.instance_10.setTransform(337.8,138.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_11 = new lib.Symbol5();
	this.instance_11.parent = this;
	this.instance_11.setTransform(326.2,137.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_12 = new lib.Symbol6();
	this.instance_12.parent = this;
	this.instance_12.setTransform(327.3,138.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_13 = new lib.share3();
	this.instance_13.parent = this;
	this.instance_13.setTransform(332.6,140,1,1,0,0,0,16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.smile5}]}).to({state:[{t:this.instance_13},{t:this.instance_12},{t:this.instance_11},{t:this.instance_10},{t:this.instance_9},{t:this.instance_8},{t:this.instance_7},{t:this.instance_6}]},187).to({state:[{t:this.smile5}]},48).wait(24));

	// smile4
	this.smile4 = new lib.smile4();
	this.smile4.parent = this;
	this.smile4.setTransform(271,67,1,1,0,0,0,16,16);
	new cjs.ButtonHelper(this.smile4, 0, 1, 2, false, new lib.smile4(), 3);

	this.instance_14 = new lib.r2();
	this.instance_14.parent = this;
	this.instance_14.setTransform(270.8,73.7,1,1,0,0,0,4,0.8);

	this.instance_15 = new lib.brov2();
	this.instance_15.parent = this;
	this.instance_15.setTransform(275.7,59.8,1,1,0,0,180,1.9,1.1);

	this.instance_16 = new lib.brov2();
	this.instance_16.parent = this;
	this.instance_16.setTransform(265.4,59.8,1,1,0,0,0,1.9,1.1);

	this.instance_17 = new lib.Symbol5();
	this.instance_17.parent = this;
	this.instance_17.setTransform(275,64.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_18 = new lib.Symbol6();
	this.instance_18.parent = this;
	this.instance_18.setTransform(276.2,65.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_19 = new lib.Symbol5();
	this.instance_19.parent = this;
	this.instance_19.setTransform(264.6,64.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_20 = new lib.Symbol6();
	this.instance_20.parent = this;
	this.instance_20.setTransform(265.7,65.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_21 = new lib.share3();
	this.instance_21.parent = this;
	this.instance_21.setTransform(271,67,1,1,0,0,0,16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.smile4}]}).to({state:[{t:this.instance_21},{t:this.instance_20},{t:this.instance_19},{t:this.instance_18},{t:this.instance_17},{t:this.instance_16},{t:this.instance_15},{t:this.instance_14}]},148).to({state:[{t:this.smile4}]},39).wait(72));

	// smile3
	this.smile3 = new lib.smile3();
	this.smile3.parent = this;
	this.smile3.setTransform(175,67,1,1,0,0,0,16,16);
	new cjs.ButtonHelper(this.smile3, 0, 1, 2, false, new lib.smile3(), 3);

	this.instance_22 = new lib.f();
	this.instance_22.parent = this;
	this.instance_22.setTransform(175.4,73.9,1,1,0,0,0,5.7,1.1);

	this.instance_23 = new lib.brov2();
	this.instance_23.parent = this;
	this.instance_23.setTransform(179.7,59.8,1,1,0,0,180,1.9,1.1);

	this.instance_24 = new lib.brov2();
	this.instance_24.parent = this;
	this.instance_24.setTransform(169.4,59.8,1,1,0,0,0,1.9,1.1);

	this.instance_25 = new lib.Symbol5();
	this.instance_25.parent = this;
	this.instance_25.setTransform(179,64.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_26 = new lib.Symbol6();
	this.instance_26.parent = this;
	this.instance_26.setTransform(180.2,65.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_27 = new lib.Symbol5();
	this.instance_27.parent = this;
	this.instance_27.setTransform(168.6,64.8,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_28 = new lib.Symbol6();
	this.instance_28.parent = this;
	this.instance_28.setTransform(169.7,65.8,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_29 = new lib.share3();
	this.instance_29.parent = this;
	this.instance_29.setTransform(175,67,1,1,0,0,0,16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.smile3}]}).to({state:[{t:this.instance_29},{t:this.instance_28},{t:this.instance_27},{t:this.instance_26},{t:this.instance_25},{t:this.instance_24},{t:this.instance_23},{t:this.instance_22}]},95).to({state:[{t:this.smile3}]},53).wait(111));

	// smile2
	this.smile2 = new lib.smile2();
	this.smile2.parent = this;
	this.smile2.setTransform(109,140,1,1,0,0,0,16,16);
	new cjs.ButtonHelper(this.smile2, 0, 1, 2, false, new lib.smile2(), 3);

	this.instance_30 = new lib.brov2();
	this.instance_30.parent = this;
	this.instance_30.setTransform(113.7,131.2,1,1,0,0,180,1.9,1.1);

	this.instance_31 = new lib.brov2();
	this.instance_31.parent = this;
	this.instance_31.setTransform(103.4,131.2,1,1,0,0,0,1.9,1.1);

	this.instance_32 = new lib.Symbol5();
	this.instance_32.parent = this;
	this.instance_32.setTransform(113,136.2,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_33 = new lib.Symbol6();
	this.instance_33.parent = this;
	this.instance_33.setTransform(114.2,137.2,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_34 = new lib.Symbol5();
	this.instance_34.parent = this;
	this.instance_34.setTransform(102.6,136.2,0.271,0.271,0,0,0,4.6,4.6);

	this.instance_35 = new lib.Symbol6();
	this.instance_35.parent = this;
	this.instance_35.setTransform(103.7,137.2,0.433,0.433,0,0,0,6.4,6.4);

	this.instance_36 = new lib.mouth();
	this.instance_36.parent = this;
	this.instance_36.setTransform(108.8,147.2,0.306,0.306,0,0,0,16.7,14.6);

	this.instance_37 = new lib.share3();
	this.instance_37.parent = this;
	this.instance_37.setTransform(109,140,1,1,0,0,0,16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.smile2}]}).to({state:[{t:this.instance_37},{t:this.instance_36},{t:this.instance_35},{t:this.instance_34},{t:this.instance_33},{t:this.instance_32},{t:this.instance_31},{t:this.instance_30}]},47).to({state:[{t:this.smile2}]},48).wait(164));

	// smile1
	this.smile1 = new lib.smile1();
	this.smile1.parent = this;
	this.smile1.setTransform(122,238,1,1,0,0,0,16,16);
	new cjs.ButtonHelper(this.smile1, 0, 1, 2, false, new lib.smile1(), 3);

	this.instance_38 = new lib.rotik();
	this.instance_38.parent = this;
	this.instance_38.setTransform(122,243,1,1,0,0,0,9.1,1.9);

	this.instance_39 = new lib.glaz();
	this.instance_39.parent = this;
	this.instance_39.setTransform(128,234.1,1,1,0,-7.5,172.5,3.5,1.7);

	this.instance_40 = new lib.brov();
	this.instance_40.parent = this;
	this.instance_40.setTransform(127.6,228.6,1,1,0,0,180,1.8,1.3);

	this.instance_41 = new lib.glaz();
	this.instance_41.parent = this;
	this.instance_41.setTransform(116.6,234.4,1,1,0,0,0,3.6,1.6);

	this.instance_42 = new lib.brov();
	this.instance_42.parent = this;
	this.instance_42.setTransform(114.5,229,1,1,0,0,0,1.8,1.3);

	this.instance_43 = new lib.share3();
	this.instance_43.parent = this;
	this.instance_43.setTransform(122,238,1,1,0,0,0,16,16);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.smile1}]}).to({state:[{t:this.instance_43},{t:this.instance_42},{t:this.instance_41},{t:this.instance_40},{t:this.instance_39},{t:this.instance_38}]},14).to({state:[{t:this.smile1}]},33).wait(212));

	// line
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#CCCCCC").ss(2,2,0,3).p("AwONRQh+jyAAkmQAAniFVlVQFVlWHiAAQHjAAFVFWQFWFVAAHiQAAEmh/Dy");
	this.shape.setTransform(221,143.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(259));

	// smilebase
	this.instance_44 = new lib.All("synched",0);
	this.instance_44.parent = this;
	this.instance_44.setTransform(366.4,296.4,0.8,0.8,0,0,0,53,31.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_44).wait(259));

	// Layer 1
	this.heart = new lib.heartt();
	this.heart.parent = this;
	this.heart.setTransform(220.5,158.2,1,1,0,0,0,26.2,25.2);

	this.angry = new lib.angy();
	this.angry.parent = this;
	this.angry.setTransform(220.5,158.2,1,1,0,0,0,26.2,25.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.heart}]},20).to({state:[]},27).to({state:[{t:this.angry}]},187).wait(25));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(311.7,-358.9,402.6,925);
// library properties:
lib.properties = {
	width: 440,
	height: 395,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"./Icecream_Berry.png?1484554177470", id:"Icecream_Berry"},
		{src:"./Icecream_body_01.png?1484554177470", id:"Icecream_body_01"},
		{src:"./Icecream_body_02.png?1484554177470", id:"Icecream_body_02"},
		{src:"./Icecream_body_03.png?1484554177470", id:"Icecream_body_03"},
		{src:"./Icecream_body_04.png?1484554177470", id:"Icecream_body_04"},
		{src:"./Icecream_body_04_1.png?1484554177470", id:"Icecream_body_04_1"},
		{src:"./Icecream_body_06.png?1484554177470", id:"Icecream_body_06"},
		{src:"./Icecream_cone.png?1484554177470", id:"Icecream_cone"},
		{src:"./Icecream_hat_02.png?1484554177470", id:"Icecream_hat_02"},
		{src:"./Icecream_hat_03.png?1484554177470", id:"Icecream_hat_03"},
		{src:"./Icecream_hat_04.png?1484554177470", id:"Icecream_hat_04"},
		{src:"./Icecream_hat_04_1.png?1484554177470", id:"Icecream_hat_04_1"},
		{src:"./Icecream_red.png?1484554177470", id:"Icecream_red"},
		{src:"./Icecream_Shadow.png?1484554177470", id:"Icecream_Shadow"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;