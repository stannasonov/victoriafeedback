window.VC = {
    init: function () {
      $('body').delegate('.cmd', 'click', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();

        var $t = $(this);
        if (!$t.is('.inack')) {
          var m = $t.data('method').split('.');
          window[m[0]][m[1]]($t);
        }

        return false;
      });

      $('select.autoSB').sb();
    },

    search: function ($d) {
      $d.parents('form')[0].submit();
    },

    submenu: function ($d) {
      $ul = $d.closest('li').eq(0).find('>ul').stop();
      if ($d.hasClass('active')) {
        $d.removeClass('active');
        $ul.hide('slow');
      }
      else {
        $d.addClass('active');
        $ul.show('slow');
      }
    },

    nop: false

  };

$(document).ready(VC.init);

$(document).ready(function () {
  $('menu.feedback a.sub0').click(function () {
    $(this).children('.icon').css({'top': '0', 'transform': 'translateY(0%)'});
  });
  $('menu.feedback a.sub1').click(function () {
    if (!$(this).hasClass('active')) {
      $(this).css('border', '0');
      $(this).siblings('span').fadeOut();
    } else {
      $(this).css('border-bottom', '1px dotted');
      $(this).siblings('span').fadeIn();
    }
  });
  $(".info.card").hover(function () {
    $("img.card").fadeIn();
  }, function () {
    $("img.card").hide();
  });
  $(".info.check").hover(function () {
    $("img.check").fadeIn();
  }, function () {
    $("img.check").hide();
  });
  $("#number").mask("26 99999999999", {placeholder: "26 00000000000"});
});