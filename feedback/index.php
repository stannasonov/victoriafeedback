<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Моя Виктория");
?>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!--meta name="viewport" content="width=1024" /-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Обратная связь</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/media.css">
    <link rel="stylesheet" type="text/css" href="css/ui.css">
    <link rel="stylesheet" type="text/css" href="css/fa.css">
    
    <!--script type="text/javascript" src="js/jquery.js"></script-->
    <script type="text/javascript" src="js/jq.sb.js"></script>
    <script type="text/javascript" src="js/init.js"></script>
    <? require($_SERVER["DOCUMENT_ROOT"]."/adaptive-menu/index.php");?>
<div class="crumbs main">
    <div class="right">
        <a class="active" href="#">Выберите раздел</a> <span class="sep fa fa-caret-right"></span>
        <a href="#">Заполните форму</a> <span class="sep fa fa-caret-right"></span>
        <a href="#">Укажите контактные данные</a>
    </div>
</div>

<menu class="feedback main">
<?
    function layer($menu, $n, $fid)
    {
        echo '<ul class="sub'.$n.'">'."\n";
        foreach($menu->item as $sub)
        {
            echo '<li>';

            if($sub->icon)
                $icon = '<div class="icon" style="background-image:url(\'css/icon/'.$sub->icon.'.png\');"></div>';
            else
                $icon = '';

            $text = '<span>'.$sub->text.'</span>';

           if($sub->form){
                echo '<a class="sub'.$n.' form" href="/obratnaya-svyaz-1/feedback/form/?id='.(++$fid).'">'.$icon.$text.'</a>';
                if($n > 0){
                    echo ' <span class="fa fa-caret-right"></span>';
                }
            }else
            {
                echo '<a href="#" class="cmd sub'.$n.'" data-method="VC.submenu">'.$icon.$text.'</a>';
                if($n > 0){
                    echo ' <span class="fa fa-caret-right"></span>';
                }
            }

            if($sub->menu)
            {
                echo "\n";
                $fid = layer($sub->menu, $n + 1, $fid);
            }
            echo "</li>\n";
        }
        echo "\n</ul>\n";
        return $fid;
    }

    $tree = simplexml_load_file( __DIR__.'/menu.xml' );
    layer($tree, 0, 0);
?>

<div class="back">
    <a href="/obratnaya-svyaz-1/"><span class="fa fa-caret-left"></span>Назад</a>
</div>
</menu>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>